package br.com.gtac.edl.core.exception;

public class GuicheMessageException extends RuntimeException {
	
	private final String messageCode;
	private final boolean isAjax;
	private final String[] args;

	public GuicheMessageException(String messageCode) {
		this(messageCode, false);

	}

	public GuicheMessageException(String messageCode, Throwable e) {
		this(messageCode, false);
	}

	public GuicheMessageException(String messageCode, boolean isAjax) {
		super(messageCode);
		this.messageCode=messageCode;
		this.isAjax = isAjax;
		this.args=null;
		
	}

	public GuicheMessageException(String messageCode, boolean isAjax,
			Throwable e,String ... args) {
		super(messageCode,e);
		this.messageCode=messageCode;
		this.isAjax = isAjax;
		this.args = args;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public boolean getIsAjax() {
		return isAjax;
	}

	public String[] getArgs() {
	    return args;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	

}
