package br.com.gtac.edl.core.service;

import org.springframework.security.core.Authentication;
import org.springframework.transaction.annotation.Transactional;

import br.com.gtac.edl.core.exception.PasswordChangeException;
import br.com.gtac.edl.core.service.security.UserEmailUnavailableException;
import br.com.gtac.edl.domain.register.ChangePasswordDTO;
import br.com.gtac.edl.domain.register.RegisterUserDTO;
import br.com.gtac.edl.domain.register.User;

@Transactional
public interface UserService {
	
	void updateLastAccess(Authentication authentication);
	User getCurrentUser();
	Long getCurrentUserId();

	User findOneByEmail(String email);
	User findOne(Long id);
	

	String createLostPassToken(User user);

	User createNewUser(String email) throws UserEmailUnavailableException;
	void resetPassword(String receivedToken, String newPassword);
	void setPassword(String receivedToken, String newPassword, String username);
	void authenticate(String username, String password);
	User findOneNotDeleted(Long id);
	User changePassword(Long userId, String old, String newPass)
			throws PasswordChangeException;

	void authenticate(User user);
	
	void registerUser(RegisterUserDTO dto);

	void editUser(ChangePasswordDTO dto);

}
