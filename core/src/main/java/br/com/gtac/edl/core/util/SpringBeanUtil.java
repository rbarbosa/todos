package br.com.gtac.edl.core.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringBeanUtil implements ApplicationContextAware{
	
	private static ApplicationContext APPLICATION_CONTEXT;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		APPLICATION_CONTEXT=applicationContext;
	}
	
	public static Object getBean(String name){
		return APPLICATION_CONTEXT.getBean(name);
	}
	public static <T> T getBean(Class<T> type){
		return APPLICATION_CONTEXT.getBean(type);
	}
}
