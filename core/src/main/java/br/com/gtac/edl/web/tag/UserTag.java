package br.com.gtac.edl.web.tag;

import lombok.Getter;
import lombok.Setter;
import br.com.gtac.edl.core.service.UserService;
import br.com.gtac.edl.core.util.SpringBeanUtil;

public class UserTag extends BaseVarTag{
    
    /**
     * 
     */
    private static final long serialVersionUID = 8840747381406967617L;
    @Getter
    @Setter
    private Long userId;

    @Override
    protected Object variable() {
	return SpringBeanUtil.getBean(UserService.class).findOne(userId);
    }

}
