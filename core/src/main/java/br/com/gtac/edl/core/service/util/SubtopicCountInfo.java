package br.com.gtac.edl.core.service.util;


public class SubtopicCountInfo {
	
	private final long questions;
	private final long contents;
	private final double average;
	
	public SubtopicCountInfo(long questions, long contents, double average) {
		super();
		this.questions = questions;
		this.contents = contents;
		this.average = average;
	}

	public long getTotal(){
		return questions+contents;
	}

	public long getQuestions() {
		return questions;
	}

	public long getContents() {
		return contents;
	}

	public double getAverage() {
		return average;
	}
	

}
