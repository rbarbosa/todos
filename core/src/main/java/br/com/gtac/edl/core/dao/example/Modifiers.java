package br.com.gtac.edl.core.dao.example;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.google.common.collect.ImmutableMap;

public class Modifiers {
	
	
	
	public static class EqualModifier extends ModifierBase {

		public EqualModifier(String field) {
			super(field);
		}

		@Override
		public void modifyCriteria(DetachedCriteria criteria,String field, Object value) {
			criteria.add(Restrictions.eq(field,value));
		}
	}
	public static class LessThanModifier extends ModifierBase {
		
		public LessThanModifier(String field) {
			super(field);
		}
		
		@Override
		public void modifyCriteria(DetachedCriteria criteria,String field, Object value) {
			criteria.add(Restrictions.lt(field,value));
		}
	}
	public static class GreaterThanModifier extends ModifierBase {
		
		public GreaterThanModifier(String field) {
			super(field);
		}
		
		@Override
		public void modifyCriteria(DetachedCriteria criteria,String field, Object value) {
			criteria.add(Restrictions.gt(field,value));
		}
	}
	public static class BetweenDateModifier extends ModifierBase {
		
		public BetweenDateModifier(String field) {
			super(field);
		}
		
		@Override
		public void modifyCriteria(DetachedCriteria criteria,String field, Object value) {
			Date date = null;
			if(value instanceof Calendar){
				date = ((Calendar) value).getTime();
			}else if(value instanceof Date){
				date = (Date) value;
			}
			if(date!=null){
				Date before = DateUtils.truncate(date, Calendar.DAY_OF_MONTH);
				Date after = DateUtils.addDays(DateUtils.truncate(date, Calendar.DAY_OF_MONTH),1);
				criteria.add(Restrictions.ge(field, before)).add(Restrictions.lt(field, after));
			}
		}
	}
	
	public static class BeginWithModifier extends ModifierBase {
		
		public BeginWithModifier(String field) {
			super(field);
		}
		@Override
		public void modifyCriteria(DetachedCriteria criteria,String field, Object value) {
			criteria.add(Restrictions.like(field,value+"%"));
		}
	}
	public static class EndWithModifier extends ModifierBase {
		
		public EndWithModifier(String field) {
			super(field);
		}
		@Override
		public void modifyCriteria(DetachedCriteria criteria,String field, Object value) {
			criteria.add(Restrictions.like(field,"%"+value));
		}
	}
	public static class MiddleWithModifier extends ModifierBase {
		public MiddleWithModifier(String field) {
			super(field);
		}
		@Override
		public void modifyCriteria(DetachedCriteria criteria,String field, Object value) {
			criteria.add(Restrictions.like(field,"%"+value+"%"));
		}
	}
	public static class IgnoreModifier extends ModifierBase {
		public IgnoreModifier(String field) {
			super(field);
		}
		@Override
		public void modifyCriteria(DetachedCriteria criteria,String field, Object value) {
		}
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static final ImmutableMap<String, Class<? extends Modifier>> modifiersMap = (ImmutableMap)ImmutableMap.builder().put("m", MiddleWithModifier.class).
			put("e",EndWithModifier.class).put("b", BeginWithModifier.class).put(">", GreaterThanModifier.class).
			put("<", LessThanModifier.class).put("=", EqualModifier.class).put("^", IgnoreModifier.class).
			put("bt", BetweenDateModifier.class).build();
	
	public static Collection<Modifier> parseModifiers(String ... modifiers) throws IllegalArgumentException{
		List<Modifier> res = new ArrayList<Modifier>();
		for(String m :modifiers){
			res.add(parseModifier(m));
		}
		return res;
	}
	
	public static Modifier parseModifier(String str) throws IllegalArgumentException{
		String[] exps = StringUtils.split(str,":");
		try {
		if(exps.length==2) {
				return modifiersMap.get(exps[0]).getConstructor(String.class).newInstance(parse(exps));
		}
		}catch(Exception e){
			throw new IllegalArgumentException("Invalid modifier string: "+str,e);
		}
		throw new IllegalArgumentException("Invalid modifier string: "+str);
	}

	private static final Pattern BEAN_PATTERN = Pattern.compile("bean\\['(.*)'\\]");
	private static String parse(String[] exps) {
		String s = exps[1];
		Matcher m = BEAN_PATTERN.matcher(s);
		if(m.find()){
			return m.group(1);
		}else{
			return s;
		}
	}
	

}
