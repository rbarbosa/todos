package br.com.gtac.edl.core.dao.todo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gtac.edl.domain.todo.TodoEntry;

public interface TodoEntryDAO extends JpaRepository<TodoEntry, Long> {
	
	public Page<TodoEntry> findAllByParentId(Pageable pageRequest, Long parentId);
	

}
