package br.com.gtac.edl.core.dao.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

public class RequestMapExampleParser {
	
	public List<Modifier> createModifiersList(HttpServletRequest request){
		List<Modifier> result = new ArrayList<Modifier>();
		Map<String,String[]> parameterMap = request.getParameterMap();
		for(Map.Entry<String, String[]> entry: parameterMap.entrySet()){
			if(entry.getKey().startsWith("m_")){
				for(String str: entry.getValue()){
					result.add(Modifiers.parseModifier(str));
				}
			}
		}
		return result;
	}
	public Map<String,Object> createParametersMap(Map<String,String> parameterMap){
		Map<String,Object> result = new HashMap<String, Object>();
		for(Map.Entry<String, String> entry: parameterMap.entrySet()){
			String str =  entry.getValue();
			if(!StringUtils.isEmpty(str)){
				result.put(entry.getKey(), str);
			}
		}
		return result;
	}

}
