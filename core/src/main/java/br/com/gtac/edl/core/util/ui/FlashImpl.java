package br.com.gtac.edl.core.util.ui;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component("flash")
@Scope(value = "session", proxyMode = ScopedProxyMode.INTERFACES)
public class FlashImpl implements Flash, Serializable {
	private static final long serialVersionUID = 5955261962353566748L;
	private final Queue<FlashMessage> messages = new LinkedList<FlashMessage>();

	private void addMessage(String messageType, String code, String... arguments) {
		messages.add(FlashMessage.create(messageType, new String[] { code },
				arguments));
	}

	@Override
	public void info(String code, String... arguments) {
		addMessage(INFO, code, arguments);
	}

	@Override
	public void error(String code, String... arguments) {
		addMessage(ERROR, code, arguments);
	}

	@Override
	public void success(String code, String... arguments) {
		addMessage(SUCCESS, code, arguments);
	}

	@Override
	public Collection<FlashMessage> getMessages() {
		return messages;
	}

	@Override
	public void reset() {
		messages.clear();
	}

}
