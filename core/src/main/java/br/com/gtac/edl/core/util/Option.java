package br.com.gtac.edl.core.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;

public class Option implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String label;
	private String value;

	public static <T> List<Option> build(List<T> arr, String labelField,
			String valueField) {
		try {
			ArrayList<Option> res = new ArrayList<Option>();
			for (Object o : arr) {
				String label;
				label = PropertyUtils.getProperty(o, labelField).toString();
				String value = PropertyUtils.getProperty(o, valueField)
						.toString();
				res.add(new Option(label, value));
			}
			return res;
		} catch (Exception e) {
			throw new RuntimeException("Erro tentando converter objetos", e);
		}
	}

	public Option(String label, String value) {
		super();
		this.label = label;
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
