package br.com.gtac.edl.core.service.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import br.com.gtac.edl.core.service.UserService;

@Component
public class AuthenticationListener implements ApplicationListener<AuthenticationSuccessEvent>{
	
	@Autowired
	private UserService userService;
	
	private static final Logger logger = LoggerFactory.getLogger(AuthenticationListener.class); 
	

	@Override
	public void onApplicationEvent(AuthenticationSuccessEvent event) {
		Object principal = event.getAuthentication().getPrincipal();
		if(principal instanceof EdlUser){
			userService.updateLastAccess(event.getAuthentication());
			logger.debug("Access date updated: "+principal);
		}else{
			logger.warn("Principal is not a EdlUser: "+principal);
		}
	}
	

}
