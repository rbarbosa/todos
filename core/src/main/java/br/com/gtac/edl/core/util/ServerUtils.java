package br.com.gtac.edl.core.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

public class ServerUtils {
    
    	public static final String URL_PROPERTY_NAME="${applicationUrl}";
	private static String getHostnameBehindApache(HttpServletRequest request) {
		String header = request.getHeader("x-forwarded-host");
		return header;
	}

	public static String getHostname(HttpServletRequest request) {
		String hostname = getHostnameBehindApache(request);
		if (!StringUtils.isBlank(hostname))
			return hostname;

		hostname = request.getLocalAddr();
		InetAddress address;
		try {
			address = InetAddress.getByName(hostname);
			if (address.isLoopbackAddress())
				return "localhost:" + request.getLocalPort();
			return address.getHostName();
		} catch (UnknownHostException e) {
			return null;
		}
	}
}
