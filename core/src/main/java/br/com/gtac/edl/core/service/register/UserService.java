package br.com.gtac.edl.core.service.register;

import java.util.List;

import javax.inject.Inject;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import br.com.gtac.edl.core.dao.UserDAO;
import br.com.gtac.edl.domain.register.User;

@Component
public class UserService {

	@Inject
	private UserDAO dao;

	public List<User> findUserByName(String name, Pageable pageable) {
		return dao.findByEmailStartingWith(name, pageable);
	}
}
