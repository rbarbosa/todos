package br.com.gtac.edl.core.exception;

public class SessionExpiredRelatedException extends GuicheMessageException {

	public SessionExpiredRelatedException() {
		super("session.expired");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}

