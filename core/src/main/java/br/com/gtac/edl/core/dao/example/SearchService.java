package br.com.gtac.edl.core.dao.example;

import java.util.Collection;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SearchService<T> {
	
	public Page<T> findByMapParameters(Map<String, Object> map,
			Pageable pageable, Collection<? extends Modifier> modifiers);

}
