package br.com.gtac.edl.core.util.ui;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component("flashInterceptor")
public class FlashInterceptor implements HandlerInterceptor {
	@Autowired
	private Flash flash;

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		return true;
	}

	private void showMessages(ModelAndView model) {
		final Collection<FlashMessage> messages = flash.getMessages();
		model.addObject("flash", new ArrayList<FlashMessage>(messages));
		flash.reset();
	}

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		String view =modelAndView!=null ?  modelAndView.getViewName(): null;
		if(!flash.getMessages().isEmpty() && view!=null && !view.contains("redirect:")){
			showMessages(modelAndView);
		}
		
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

}
