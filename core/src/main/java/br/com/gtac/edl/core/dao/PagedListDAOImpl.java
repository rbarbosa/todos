package br.com.gtac.edl.core.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Component;

@Component
public class PagedListDAOImpl<T> implements PagedListDAO {
	
	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings({ "hiding", "unchecked" })
	@Override
	public <T> List<T> list(Class<T> clazz, int firstResult, int maxResults, String orderField, boolean asc) {
		DetachedCriteria listCriteria = listCriteria(clazz, orderField, asc);
		Criteria criteria = listCriteria.getExecutableCriteria(getSession());
		criteria.setFirstResult(firstResult);
		criteria.setMaxResults(maxResults);
		return criteria.list();
	}
	

	private Session getSession() {
		return (Session) em.getDelegate();
	}

	public DetachedCriteria listCriteria(Class<?> clazz, String orderField, boolean asc) {
		DetachedCriteria criteria = DetachedCriteria.forClass(clazz).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		if(!StringUtils.isEmpty(orderField)){
			criteria.addOrder(asc?Order.asc(orderField):Order.desc(orderField));
		}
		return criteria;
	}
}
