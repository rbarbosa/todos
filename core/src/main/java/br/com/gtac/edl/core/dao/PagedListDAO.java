package br.com.gtac.edl.core.dao;

import java.util.List;

public interface PagedListDAO {

	<T> List<T> list(Class<T> clazz, int firstResult, int maxResults,
			String orderField, boolean asc);
}
