package br.com.gtac.edl.core.service.security;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.web.session.HttpSessionCreatedEvent;
import org.springframework.security.web.session.HttpSessionDestroyedEvent;
import org.springframework.stereotype.Component;

@Component
public class SessionAccessHolder implements ApplicationListener {

	private Map<String, HttpSession> sessions = new HashMap<String, HttpSession>();

	public HttpSession getSession(String sessionid) {
		return sessions.get(sessionid);

	}

	
	@Override
	public void onApplicationEvent(ApplicationEvent event) {
		// TODO Auto-generated method stub

		if (event instanceof HttpSessionDestroyedEvent) {
			HttpSessionDestroyedEvent e = (HttpSessionDestroyedEvent) event;
			if (sessions.containsKey(e.getSession().getId())) {
				sessions.remove(e.getSession().getId());
			}

		} else if (event instanceof HttpSessionCreatedEvent) {
			HttpSessionCreatedEvent e = (HttpSessionCreatedEvent) event;
			if (!sessions.containsKey(e.getSession().getId()))
				sessions.put(e.getSession().getId(), e.getSession());
		}

	}

}
