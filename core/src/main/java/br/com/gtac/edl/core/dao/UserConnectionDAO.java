package br.com.gtac.edl.core.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.gtac.edl.domain.register.UserConnection;

public interface UserConnectionDAO extends JpaRepository<UserConnection, Long>,
		JpaSpecificationExecutor<UserConnection> {

}
