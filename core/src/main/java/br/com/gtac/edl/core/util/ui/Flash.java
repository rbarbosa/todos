package br.com.gtac.edl.core.util.ui;

import java.util.Collection;

public interface Flash {
	public static final String INFO = "alert";
	public static final String ERROR = "error";
	public static final String SUCCESS = "success";

	void info(String code, String... arguments);

	void error(String code, String... arguments);

	void success(String code, String... arguments);

	Collection<FlashMessage> getMessages();

	void reset();
}
