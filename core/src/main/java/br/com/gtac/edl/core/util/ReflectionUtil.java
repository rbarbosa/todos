package br.com.gtac.edl.core.util;

import org.apache.commons.beanutils.PropertyUtils;

public class ReflectionUtil {
	public static Long getProperty(Object obj, String field) {
		try {
			return (Long) PropertyUtils.getProperty(obj, field);
		} catch (Exception e) {
			throw new IllegalStateException("Entity has no property id", e);
		}
	}

}
