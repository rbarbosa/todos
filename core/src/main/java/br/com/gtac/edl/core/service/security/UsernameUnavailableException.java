package br.com.gtac.edl.core.service.security;

import br.com.gtac.edl.core.exception.GuicheMessageException;

public class UsernameUnavailableException extends GuicheMessageException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1732624264110235057L;
	private static final String DEFAULT_MESSAGE="application.username.unavailable"; 

	public UsernameUnavailableException() {
		super(DEFAULT_MESSAGE);
	}

	public UsernameUnavailableException(Throwable cause) {
		super(DEFAULT_MESSAGE,cause);
	}

	public UsernameUnavailableException(String string) {
		super(string);
	}
}
