package br.com.gtac.edl.core.dao.example;

import java.util.Collection;
import java.util.Map;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ExampleFinderService<T> {

	public DetachedCriteria findByMapParametersCriteria(Class<?> clazz,
			Map<String, Object> map, Collection<? extends Modifier> modifiers);
	public Page<T> findByMapParameters(Class<?> clazz, Map<String, Object> map,
			Pageable pageable, Collection<? extends Modifier> modifiers);

	public Page<T> executePagedCriteria(Pageable pageable,
			DetachedCriteria executableDetachedCriteria);
}
