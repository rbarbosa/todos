package br.com.gtac.edl.core.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;

public class GtacCustomConversionService extends
		FormattingConversionServiceFactoryBean {


	public class DateToStringConverter implements Converter<Date, String> {
		@Override
		public String convert(Date source) {
			return new SimpleDateFormat("dd/MM/yyyy").format(source);
		}
	}

	public class StringToDateConverter implements Converter<String, Date> {
		@Override
		public Date convert(String source) {
			if (!StringUtils.isEmpty(source)) {
				try {
					SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
					df.setLenient(false);
					return df.parse(source);
				} catch (ParseException e) {
					throw new TypeMismatchException("could not parse: "
							+ source, e);
				}
			}
			return null;
		}
	}




	@Autowired
	private MessageSource messageSource;

	@Autowired
	private HttpServletRequest request;

	@Override
	protected void installFormatters(FormatterRegistry registry) {
		super.installFormatters(registry);
		registry.addConverter(new DateToStringConverter());
		registry.addConverter(new StringToDateConverter());
	}

}