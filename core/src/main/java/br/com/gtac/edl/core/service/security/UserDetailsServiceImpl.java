package br.com.gtac.edl.core.service.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.com.gtac.edl.core.dao.UserDAO;
import br.com.gtac.edl.domain.register.User;

@Component("userDetailsService")
public class UserDetailsServiceImpl implements
		org.springframework.security.core.userdetails.UserDetailsService {

	@Autowired
	private UserDAO userDAO;
	
	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String email)
			throws UsernameNotFoundException, DataAccessException {
		User user = userDAO.findOneByEmail(email);
		if (user == null)
			throw new UsernameNotFoundException("user not found");
		return EdlUser.createFromUser(user);
		
	}
}
