package br.com.gtac.edl.core.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.gtac.edl.domain.register.User;
import br.com.gtac.edl.domain.register.UserLoginEntity;

public interface UserLoginEntityDAO extends
		JpaRepository<UserLoginEntity, Long>,
		JpaSpecificationExecutor<User> {
}
