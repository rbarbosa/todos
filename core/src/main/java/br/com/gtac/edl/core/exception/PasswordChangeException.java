package br.com.gtac.edl.core.exception;

public class PasswordChangeException extends GuicheMessageException {

	public PasswordChangeException() {
		super("invalid.password");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	

}
