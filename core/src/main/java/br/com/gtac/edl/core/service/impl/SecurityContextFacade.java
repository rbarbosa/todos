package br.com.gtac.edl.core.service.impl;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

@Component
public class SecurityContextFacade {

	public WebAuthenticationDetails getAuthenticationDetails() {
		
		UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) SecurityContextHolder
				.getContext().getAuthentication();
		WebAuthenticationDetails details = (WebAuthenticationDetails) token
				.getDetails();

		return details;
	}
	
	public Object getPrincipal() {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		Object principal = auth != null ? auth.getPrincipal() : null;
		return principal;
	}
	
	public boolean isLogged() {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		return auth != null && auth.isAuthenticated();
	}

}
