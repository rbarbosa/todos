package br.com.gtac.edl.core.dao.example;

import java.beans.PropertyDescriptor;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;

import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.commons.beanutils.converters.DateTimeConverter;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import br.com.gtac.edl.domain.annotation.ConcreteSearchClass;

@Component("exampleFinderService")
public class ExampleFinderServiceImpl<T> implements ExampleFinderService<T>{

	@PersistenceContext
	private EntityManager em;

	private ConvertUtilsBean convertUtils = new ConvertUtilsBean();

	public ExampleFinderServiceImpl() {
		DateTimeConverter dtConverter = new DateConverter();
		dtConverter.setPattern("dd/MM/yyyy");
		convertUtils.register(dtConverter, Date.class);
	}

	public Session getSession() {
		return (Session) em.getDelegate();
	}
	

	private HashMap<String, Modifier> createModifierMap(
			Collection<? extends Modifier> modifiers) {
		HashMap<String, Modifier> modifierMap = new HashMap<String, Modifier>();
		for (Modifier m : modifiers) {
			modifierMap.put(m.getField(), m);
		}
		return modifierMap;
	}
	
	public Page<T> findByExample(T example,Pageable pageable,Collection<? extends Modifier> modifiers){
		return findByMapParameters(example.getClass(), ReflectionUtil.describe(example),pageable, modifiers);
	}

	@Override
	public DetachedCriteria findByMapParametersCriteria(Class<?> clazz,
			Map<String, Object> map, Collection<? extends Modifier> modifiers) {
		DetachedCriteria criteria = DetachedCriteria.forClass(clazz);
		HashMap<String, Modifier> modifierMap = createModifierMap(modifiers);
		Object bean = newBean(clazz);
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			if(entry.getValue()!=null){
				modifyCriteria(criteria, modifierMap, bean, entry.getKey(),
					entry.getValue(), entry.getKey());
			}
		}
		return criteria;
	}

	private Object newBean(Class<?> clazz) {
		ConcreteSearchClass annotation = clazz
				.getAnnotation(ConcreteSearchClass.class);
		Object bean = null;
		if (annotation != null) {
			bean = ReflectionUtil.newInstance(annotation.value());
		} else {
			bean = ReflectionUtil.newInstance(clazz);
		}
		return bean;
	}

	private void modifyCriteria(DetachedCriteria criteria,
			HashMap<String, Modifier> modifierMap, Object bean, String key,
			Object example, String fullPath) {
		if (key.contains(".")) {
			caseNested(criteria, modifierMap, bean, key, example, fullPath);
		} else {
			caseSimple(criteria, modifierMap, bean, key, example, fullPath);
		}
	}

	private void caseSimple(DetachedCriteria criteria,
			HashMap<String, Modifier> modifierMap, Object bean, String key,
			Object example, String fullPath) {
		PropertyDescriptor prop = ReflectionUtil.getDescriptor(bean, key);
		if (example == null
				|| prop.getReadMethod().isAnnotationPresent(Transient.class)) {
			return;
		}
		Class<?> type = prop.getPropertyType();
		Modifier modifier = getModifier(modifierMap, fullPath, type);
		if (modifier != null) {
			Object v = convertUtils.convert(example, type);
			if (Enum.class.isAssignableFrom(type)
					&& example.getClass() == String.class) {
				v = Enum.valueOf((Class<Enum>) type, (String) example);
			}
			modifier.modifyCriteria(criteria, key, v);
		} else if (example.getClass().isAnnotationPresent(Entity.class)) {
			caseEntity(criteria, modifierMap, bean, key, example, fullPath);
		} else if (Collection.class.isAssignableFrom(example.getClass())) {
			caseCollection(criteria, modifierMap, bean, key, example, fullPath);
		}
	}

	private void caseCollection(DetachedCriteria criteria,
			HashMap<String, Modifier> modifierMap, Object bean, String key,
			Object example, String fullPath) {
		Collection coll = (Collection) example;
		if (!coll.isEmpty() && coll.size() == 1) {
			Object valueItem = coll.iterator().next();
			Object idValue = ReflectionUtil.getProperty(valueItem, "id");
			PropertyDescriptor prop = ReflectionUtil.getDescriptor(bean, key);
			if (idValue != null) {
				criteria.createCriteria(prop.getName()).add(Restrictions.eq("id", idValue));
			}else{
				iterateExampleParameters(criteria, modifierMap, key, valueItem, fullPath);
			}
		}
	}

	private Modifier getModifier(Map<String, Modifier> modifierMap,
			String propName, Class<?> type) {
		Modifier m = modifierMap.get(propName);
		if (m == null) {
			if (String.class == type || Boolean.class == type
					|| Number.class.isAssignableFrom(type)
					) {
				m = new Modifiers.EqualModifier(propName);
			}else if(Date.class == type){
				m = new Modifiers.IgnoreModifier(propName);
			}
		}
		return m;
	}

	private void caseEntity(DetachedCriteria criteria,
			HashMap<String, Modifier> modifierMap, Object bean, String key,
			Object example, String fullPath) {
		PropertyDescriptor prop = ReflectionUtil.getDescriptor(bean, key);
		Object idValue = ReflectionUtil.getProperty(example, "id");
		if (idValue != null) {
			criteria.add(Restrictions.eq(key + ".id", idValue));
		} else {
			iterateExampleParameters(criteria, modifierMap, key, example, fullPath);
		}
	}

	private void iterateExampleParameters(DetachedCriteria criteria,
			HashMap<String, Modifier> modifierMap, String key, Object example,
			String fullPath) {
		Map<String, Object> map = ReflectionUtil.describe(example);
		DetachedCriteria subCriteria = null;
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			if(entry.getValue()==null) continue;
			if (subCriteria == null) {
				subCriteria = criteria.createCriteria(key);
			}
			PropertyDescriptor subProp = ReflectionUtil.getDescriptor(
					example, entry.getKey());
			modifyCriteria(subCriteria, modifierMap, example,
					entry.getKey(), entry.getValue(), fullPath+"."+entry.getKey());
		}
	}

	private void caseNested(DetachedCriteria criteria,
			HashMap<String, Modifier> modifierMap, Object bean, String key,
			Object example, String fullPath) {
		ReflectionUtil.instantiateNestedProperties(bean, key);
		String[] split = key.split("\\.");
		String first = split[0];
		PropertyDescriptor prop = ReflectionUtil.getDescriptor(bean, first);
		Class<?> type = prop.getPropertyType();
		if(Collection.class.isAssignableFrom(type)){
			DetachedCriteria subCriteria = criteria.createCriteria(first);
			if("id".equals(split[1])){
				subCriteria.add(Restrictions.eq("id", convertUtils.convert(example, Long.class)));
			}else{
				String newKey = StringUtils.join(
						Arrays.copyOfRange(split, 1, split.length), ".");
				Class<?> clazz = ReflectionUtil.getGenericTypeCollection(prop.getReadMethod().getGenericReturnType());
				modifyCriteria(subCriteria, modifierMap, ReflectionUtil.newInstance(clazz), newKey, example, fullPath);
			}
		}else if (split.length == 2 && "id".equals(split[1]) ) {
			criteria.add(Restrictions.eq(key, convertUtils.convert(example,Long.class)));
		} else {
			if (type.isAnnotationPresent(Entity.class)) {
				DetachedCriteria subCriteria = criteria.createCriteria(first);;
				String newKey = StringUtils.join(
						Arrays.copyOfRange(split, 1, split.length), ".");
				modifyCriteria(subCriteria, modifierMap,
						ReflectionUtil.getProperty(bean, first), newKey,
						example, fullPath);
			}
		}

	}

	@Override
	public Page<T> findByMapParameters(Class<?> clazz, Map<String, Object> map,
			Pageable pageable, Collection<? extends Modifier> modifiers) {
		DetachedCriteria criteria = findByMapParametersCriteria(clazz, map,
				modifiers);
		return executePagedCriteria(pageable, criteria);
	}

	public Page<T> executePagedCriteria(Pageable pageable,
			DetachedCriteria criteria) {
		Criteria executableCriteria = criteria
				.getExecutableCriteria(getSession());
		executableCriteria.setProjection(Projections.rowCount());
		long total = (Long) executableCriteria.list().get(0);
		executableCriteria.setProjection(null);
		executableCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		executableCriteria.setMaxResults(pageable.getPageSize());
		executableCriteria.setFirstResult(pageable.getOffset());
		if(pageable.getSort()!=null){
			Sort.Order order = pageable.getSort().iterator().next();
			Order hibernateOrder = order.isAscending() ? Order.asc(order.getProperty()) : Order.desc(order.getProperty()); 
			executableCriteria.addOrder(hibernateOrder);
		}
		List<T> tList = executableCriteria.list();
		return new PageImpl<T>(tList, pageable, total);
	}

}
