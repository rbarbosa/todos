package br.com.gtac.edl.core.dao.example;

import org.apache.commons.beanutils.PropertyUtils;

public abstract class ModifierBase implements Modifier{
	
	private final String field;
	
	public ModifierBase(String field){
		this.field = field;
	}

	@Override
	public String getField() {
		return field;
	}
	
	public Object getValue(Object example){
		 try {
			return PropertyUtils.getProperty(example, getField());
		} catch (Exception e) {
			return null;
		}
	}

}
