package br.com.gtac.edl.core.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;

public class CriteriaUtils {

	private final Map<String, Criteria> criteriaMap = new HashMap<String, Criteria>();

	private CriteriaUtils() {
	}

	public static Criteria createCriteria(Class<?> persistentClass,
			Session session, Map<String, ?> params) {
		Criteria criteria = session.createCriteria(persistentClass);
		CriteriaUtils utils = new CriteriaUtils();

		for (String key : params.keySet()) {
			utils.addFilter(criteria, key, params.get(key));
		}

		return criteria;
	}

	private void addFilter(Criteria criteria, String property, Object value) {
		if (value == null)
			return;
		int indexOfPoint = property.indexOf('.');
		if (indexOfPoint >= 0) {
			String path = property.substring(0, indexOfPoint);
			String subProperty = property.substring(indexOfPoint + 1);
			if (!criteriaMap.containsKey(path))
				criteriaMap.put(path, getSubCriteria(criteria, path, value));

			Criteria subCriteria = criteriaMap.get(path);
			addFilter(subCriteria, subProperty, value);
			return;
		}

		if (Collection.class.isAssignableFrom(value.getClass())) {
			Disjunction disjunction = Restrictions.disjunction();
			for (Object v : (Collection<?>) value) {
				Criterion restriction = createRestriction(property, v);
				disjunction.add(restriction);
			}
			criteria.add(disjunction);
		} else {
			criteria.add(createRestriction(property, value));
		}
	}

	private Criteria getSubCriteria(Criteria baseCriteria, String path, Object value) {
		if(value == null)
			return baseCriteria.createCriteria(path, Criteria.LEFT_JOIN);
		if(Collection.class.isAssignableFrom(value.getClass()) && ((Collection<?>)value).size() == 0)
			return baseCriteria.createCriteria(path, Criteria.LEFT_JOIN);
		
		return baseCriteria.createCriteria(path, Criteria.INNER_JOIN);
	}

	private static Criterion createRestriction(String property, Object value) {
		if (String.class.isAssignableFrom(value.getClass()))
			return Restrictions.ilike(property, value);
		return Restrictions.eq(property, value);
	}
}