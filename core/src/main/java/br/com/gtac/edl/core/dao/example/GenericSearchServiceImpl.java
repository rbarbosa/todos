package br.com.gtac.edl.core.dao.example;

import java.util.Collection;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public class GenericSearchServiceImpl<T> implements SearchService<T> {
	private Class<?> clazz;
	private ExampleFinderService<T> service;
	
	public GenericSearchServiceImpl(Class<?> clazz,ExampleFinderService<T> service) {
		this.clazz = clazz;
		this.service = service;
	}

	@Override
	public Page<T> findByMapParameters(Map<String, Object> map,
			Pageable pageable, Collection<? extends Modifier> modifiers) {
		return service.findByMapParameters(clazz, map, pageable, modifiers);
	}

}
