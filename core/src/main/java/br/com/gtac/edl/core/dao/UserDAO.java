package br.com.gtac.edl.core.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.gtac.edl.domain.register.User;

public interface UserDAO extends JpaRepository<User, Long>,
		JpaSpecificationExecutor<User>, UserDAOCustom {
	
	@Query("from User u where u.email=lower(?) and deleted=0")
	User findOneByEmail(String email);
	@Query("from User u where u.id=? and deleted=0")
	public User findOneNotDeleted(Long id);

	@Query("from User u where u.lostPassToken=? and deleted=0")
	User findOneByLostPassToken(String receivedToken);

	@Modifying
	@Query(value = "update User u SET u.deleted = :timestamp where roles='ROLE_ANONYMOUS' and completed=false and deleted=0")
	void deleteTemporaryUsers(@Param("timestamp") long timestamp);
	
	@Query(value="select count(distinct u) from User u where deleted=0 or completed=true")
	long countEditable();
	
	@Query(value = "select distinct u from User u where deleted=0 or completed=true and roles <> 'ROLE_ANONYMOUS'")
	Page<User> findAllEditable(Pageable pageable);

	@Query("select u from User u where u.id IN ?1 AND (roles='ROLE_USER,ROLE_ADMIN' OR roles='ROLE_USER')")
	List<User> findAllUsers(List<Long> usersId);
}
