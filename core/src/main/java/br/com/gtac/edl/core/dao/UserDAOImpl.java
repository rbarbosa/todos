package br.com.gtac.edl.core.dao;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.gtac.edl.domain.register.User;

public class UserDAOImpl {

	@PersistenceContext
	private EntityManager em;

	@Transactional(propagation = Propagation.REQUIRED)
	public void delete(User user) {
		user.setDeleted(new Date().getTime() / 1000);
		em.createQuery("DELETE from UserConnection uc WHERE uc.user.id = ?1").setParameter(1, user.getId())
				.executeUpdate();

		em.persist(user);
		em.flush();
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void delete(Long id) {
		User user = em.find(User.class, id);
		em.flush();
		delete(user);
	}

}
