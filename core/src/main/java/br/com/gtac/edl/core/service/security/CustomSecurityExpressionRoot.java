package br.com.gtac.edl.core.service.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.expression.WebSecurityExpressionRoot;

public class CustomSecurityExpressionRoot extends WebSecurityExpressionRoot {

	public CustomSecurityExpressionRoot(Authentication a, FilterInvocation fi) {
		super(a, fi);
	}


	public boolean isLocalhost() {
		if (request.getRemoteAddr().equals("localhost"))
			return true;
		return false;
	}

}
