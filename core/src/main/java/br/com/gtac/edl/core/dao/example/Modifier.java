package br.com.gtac.edl.core.dao.example;

import org.hibernate.criterion.DetachedCriteria;

public interface Modifier {
	
	String getField();
	void modifyCriteria(DetachedCriteria criteria, String field, Object value);

}
