package br.com.gtac.edl.core.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;

import br.com.gtac.edl.domain.register.User;

public interface UserDAOCustom {
	List<User> findByEmailStartingWith(String email, Pageable pageable);
}
