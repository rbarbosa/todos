package br.com.gtac.edl.core.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class DataIntegrityExceptionTranslator {

	private static final String FIELD_PREFIX = "field_";
	private static final Pattern BETWEEN_PARENTHESIS = Pattern
			.compile("'(.*?)'");

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	private MessageSource messageSource;

	public String translateException(DataIntegrityViolationException ex,
			Locale locale) {
		return translateException(ex, locale, null);
	}

	public String translateException(DataIntegrityViolationException ex,
			Locale locale, BindingResult br) {
		List<String> arguments = getArguments(ex);
		String code = arguments.get(arguments.size() - 1);
		String validationMessage = messageSource.getMessage(code,
				arguments.toArray(), defaultMessage(code), locale);
		return addToBindingResult(code, validationMessage, locale, br);
	}
	public String getErrorMessageCode(DataIntegrityViolationException ex){
		List<String> arguments = getArguments(ex);
		return arguments.get(arguments.size() - 1);
	}

	private List<String> getArguments(DataIntegrityViolationException ex) {
		Throwable throwable = ex.getRootCause();
		if (throwable instanceof MySQLIntegrityConstraintViolationException) {
			MySQLIntegrityConstraintViolationException exception = (MySQLIntegrityConstraintViolationException) throwable;
			return parseMessageArguments(exception);
		}
		throw ex;
	}
	public String getMessageCode(DataIntegrityViolationException ex){
		List<String> arg = getArguments(ex);
		return arg.get(arg.size()-1);
	}

	private List<String> parseMessageArguments(
			MySQLIntegrityConstraintViolationException exception) {
		String message = exception.getMessage();
		if(isDeleteMessage(message)){
			return parseDeleteMessage(message);
		}
		Matcher m = BETWEEN_PARENTHESIS.matcher(message);
		List<String> arguments = new ArrayList<String>();
		while (m.find()) {
			arguments.add(m.group(1));
		}
		return arguments;
	}

	private boolean isDeleteMessage(String message) {
		return message.contains("Cannot delete");
	}

	private List<String> parseDeleteMessage(String message) {
		int sIdx = message.indexOf("CONSTRAINT")+12;
		int eIdx = message.indexOf("`", sIdx);
		String code = message.substring(sIdx, eIdx);
		return Arrays.asList(code);

	}

	private String defaultMessage(String code) {
		return "??" + code + "??";
	}

	private String addToBindingResult(String code, String validationMessage,
			Locale locale, BindingResult br) {
		if (br != null) {
			String field = messageSource.getMessage(FIELD_PREFIX + code,
					new Object[0], "", locale);
			if (!field.isEmpty()) {
				br.addError(new FieldError(br.getObjectName(), field, br
						.getFieldValue(field), false, null, null,
						validationMessage));
				return null;
			}
		}
		return validationMessage;
	}

}
