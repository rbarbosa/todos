package br.com.gtac.edl.core.dao.todo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.gtac.edl.domain.todo.TodoList;

public interface TodoListDAO extends JpaRepository<TodoList, Long> {
	
	@Query(value = "FROM  TodoList t WHERE t.userId=?1 order by lastSeen desc")
	public Page<TodoList> findAllByUserId(Pageable pageRequest, Long userId);
	

}
