package br.com.gtac.edl.core.service.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import br.com.gtac.edl.domain.register.User;

public class EdlUser extends org.springframework.security.core.userdetails.User{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final User user;
	
	public static EdlUser createFromUser(User user){
		List<GrantedAuthority> roles=new ArrayList<GrantedAuthority>();
		for(String roleName:user.getRoles().split(",")){
			roles.add(new SimpleGrantedAuthority(roleName));
		}
		return new EdlUser(user.getEmail(), user.getPassword(),
			user.isActive(), true, true, true, roles, user);
	}

	public EdlUser(String username, String password, boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired,
			boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities,User user) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired,
				accountNonLocked, authorities);
		Validate.notNull(user);
		this.user=user;
	}
	
	public Long getId(){
		return user.getId();
	}
	
	public User getUser(){
		return user;
	}
	
	@Override
	public String toString() {
		return "EdlUser [user=" + user.getEmail() + "]";
	}
	
}
