package br.com.gtac.edl.core.dao.example;

import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;

public class ReflectionUtil {

	public static PropertyDescriptor getDescriptor(Object bean, String key) {
		try {
			return PropertyUtils.getPropertyDescriptor(bean, key);
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
	}

	public static <T extends Annotation> T getAnnotation(Class<?> clazz,
			Class<T> annotation) {
		return clazz.getAnnotation(annotation);
	}

	public static PropertyDescriptor getDescriptor(Class<?> clazz, String key) {
		try {
			return PropertyUtils.getPropertyDescriptor(clazz, key);
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
	}

	public static Object newInstance(Class<?> clazz) {
		try {
			return clazz.newInstance();
		} catch (Exception e) {
			throw new IllegalStateException("Should have no-args constructor",
					e);
		}
	}

	public static Object getProperty(Object value, String prop) {
		try {
			return PropertyUtils.getProperty(value, prop);
		} catch (Exception e) {
			return null;
		}
	}

	public static Class<?> getGenericTypeCollection(Type type) {
		if (type instanceof ParameterizedType)
		{
			ParameterizedType ptype = (ParameterizedType) type;
			Type[] typeArguments = ptype.getActualTypeArguments();
			for (Type typeArgument : typeArguments) {
				return (Class<?>) typeArgument;
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> describe(Object obj) {
		try {
			return PropertyUtils.describe(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static void instantiateNestedProperties(Object obj, String fieldName) {
		if (!fieldName.contains("."))
			return;
		try {
			String[] fieldNames = fieldName.split("\\.");
			if (fieldNames.length > 1) {
				StringBuffer nestedProperty = new StringBuffer();
				for (int i = 0; i < fieldNames.length - 1; i++) {
					String fn = fieldNames[i];
					if (i != 0) {
						nestedProperty.append(".");
					}
					nestedProperty.append(fn);

					Object value = PropertyUtils.getProperty(obj,
							nestedProperty.toString());

					if (value == null) {
						PropertyDescriptor propertyDescriptor = PropertyUtils
								.getPropertyDescriptor(obj,
										nestedProperty.toString());
						Class<?> propertyType = propertyDescriptor
								.getPropertyType();
						Object newInstance = propertyType.newInstance();
						PropertyUtils.setProperty(obj,
								nestedProperty.toString(), newInstance);
					}
				}
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
