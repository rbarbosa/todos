package br.com.gtac.edl.core.service;

import java.io.Serializable;
import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



/**
 * @author Digao
 * Interface utilizada pelo CRUDController. Prove as funcionalidades que ele utiliza, assim
 * pode ser feita uma implementação diferente para casos específicos Ex: {@link UserDtoCrudServiceImpl}
 *
 * @param <T> tipo a ser persistido
 * @param <K> tipo da chave primaria
 */
public interface CRUDService<T,K extends Serializable> {
	T save(T entity);

	void delete(K id);

	T findOne(K id);

	Collection<T> findAll();
	
	boolean exists(K id);
	
	long count();

	Page<T> list(Pageable pageable);
}
