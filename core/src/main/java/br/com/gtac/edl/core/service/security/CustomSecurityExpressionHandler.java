package br.com.gtac.edl.core.service.security;

import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.stereotype.Component;

@Component("customSecurityExpressionHandler")
public class CustomSecurityExpressionHandler extends
		DefaultWebSecurityExpressionHandler {

	@Override
	protected SecurityExpressionRoot createSecurityExpressionRoot(
			Authentication authentication, FilterInvocation fi) {
		return new CustomSecurityExpressionRoot(authentication, fi);
	}
}
