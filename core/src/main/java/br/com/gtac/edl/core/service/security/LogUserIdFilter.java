package br.com.gtac.edl.core.service.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import br.com.gtac.edl.core.service.UserService;


/**
 * @author Digao
 * Classe que faz a autenticação de um usuario anonimo. Utilizada somente na aplicacao aluno, declarada como bean.
 *
 */
@Component("logUserIdFilter")
public class LogUserIdFilter extends GenericFilterBean{
	
	@Autowired
	private UserService userService;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		try{
			
		Long user = userService.getCurrentUserId();
		MDC.put("userId", user!=null? user.toString():"anonymous");
		}finally{
			chain.doFilter(request, response);
		}
	}
	
	
}
