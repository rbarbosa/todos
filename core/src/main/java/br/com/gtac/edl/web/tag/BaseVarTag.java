package br.com.gtac.edl.web.tag;

import javax.servlet.jsp.JspWriter;

import lombok.Getter;
import lombok.Setter;

import org.springframework.web.servlet.tags.RequestContextAwareTag;
import org.springframework.web.util.TagUtils;

public abstract class BaseVarTag extends RequestContextAwareTag {
	private static final long serialVersionUID = -1058496394293605802L;
	@Getter
	@Setter
	private String var;

	protected abstract Object variable();

	@Override
	protected int doStartTagInternal() throws Exception {
		try {
			Object variable = variable();
			if (org.apache.commons.lang3.StringUtils.isBlank(var)) {
				JspWriter out = pageContext.getOut();
				out.write(variable.toString());
			} else {
				pageContext.setAttribute(var, variable,
						TagUtils.getScope("page"));
			}
		} catch (Exception e) {
		}

		return EVAL_BODY_INCLUDE;
	}
}
