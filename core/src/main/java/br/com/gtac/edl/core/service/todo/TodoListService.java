package br.com.gtac.edl.core.service.todo;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.Validate;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.com.gtac.edl.core.dao.todo.TodoEntryDAO;
import br.com.gtac.edl.core.dao.todo.TodoListDAO;
import br.com.gtac.edl.core.service.UserService;
import br.com.gtac.edl.domain.register.User;
import br.com.gtac.edl.domain.todo.TodoEntry;
import br.com.gtac.edl.domain.todo.TodoList;

@Component
@Slf4j
@Transactional
public class TodoListService {

	@Inject
	private TodoListDAO listDAO;
	@Inject
	private TodoEntryDAO entryDAO;

	@Inject
	private UserService userService;

	public TodoList findOneList(Long id) {
		return listDAO.findOne(id);
	}

	public TodoEntry findOneEntry(Long id) {
		return entryDAO.findOne(id);
	}

	public TodoList create(String name) {
		Validate.notNull(name, "List name cannot be null");
		TodoList list = new TodoList();
		list.setName(name);
		list.setUserId(userService.getCurrentUserId());
		log.debug("new list saved: " + name);
		return listDAO.save(list);
	}

	public TodoList markSeen(Long id) {
		Validate.notNull(id, "id cannot be null");
		TodoList one = listDAO.findOne(id);
		if (one != null) {
			one.setLastSeen(Calendar.getInstance());
		}
		return one;
	}
	public TodoEntry createEntry(TodoEntry entry, Long listParent)
			throws IllegalAccessException {
		TodoList one = listDAO.findOne(listParent);
		Validate.notNull(one, "TodoList not found");
		User user = userService.getCurrentUser();
		if (one.getUserId().equals(user.getId())) {
			entry.setParent(one);
			return entryDAO.save(entry);
		} else {
			throw new IllegalAccessException("User has no access to this list");
		}
	}

	public TodoEntry updateEntry(TodoEntry data)
			throws IllegalAccessException {
		Validate.notNull(data, "Invalid data");
		Validate.notNull(data.getId(), "Invalid data - must have id");
		TodoEntry entry = entryDAO.findOne(data.getId());
		Validate.notNull(entry, "TodoEntry not found");
		User user = userService.getCurrentUser();
		if (entry.getParent().getUserId().equals(user.getId())) {
			return entryDAO.save(data);
		} else {
			throw new IllegalAccessException("User has no access to this list");
		}
	}

	public void removeEntry(Long id)
			throws IllegalAccessException {
		Validate.notNull(id, "Invalid data");
		TodoEntry entry = entryDAO.findOne(id);
		Validate.notNull(entry, "TodoEntry not found");
		User user = userService.getCurrentUser();
		if (entry.getParent().getUserId().equals(user.getId())) {
			entryDAO.delete(entry);
		} else {
			throw new IllegalAccessException("User has no access to this list");
		}
	}

	public List<TodoList> findAllFromUser() {
		return listDAO.findAllByUserId(new PageRequest(0, 30),
				userService.getCurrentUserId()).getContent();

	}

}
