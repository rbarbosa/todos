package br.com.gtac.edl.core.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.com.gtac.edl.core.dao.UserDAO;
import br.com.gtac.edl.core.dao.UserLoginEntityDAO;
import br.com.gtac.edl.core.exception.PasswordChangeException;
import br.com.gtac.edl.core.service.UserService;
import br.com.gtac.edl.core.service.security.EdlUser;
import br.com.gtac.edl.core.service.security.UserEmailUnavailableException;
import br.com.gtac.edl.domain.register.ChangePasswordDTO;
import br.com.gtac.edl.domain.register.RegisterUserDTO;
import br.com.gtac.edl.domain.register.Roles;
import br.com.gtac.edl.domain.register.User;
import br.com.gtac.edl.domain.register.UserInfo;
import br.com.gtac.edl.domain.register.UserLoginEntity;
import br.com.gtac.edl.domain.util.EmailUtil;

@Component
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO dao;

	@Autowired
	private UserLoginEntityDAO loginDao;

	@Autowired
	private PasswordEncoder encoder;

	@Autowired
	private SecurityContextFacade securityContextFacade;


	@Override
	@Transactional(readOnly=true)
	public Long getCurrentUserId() {
		Object principal = securityContextFacade.getPrincipal();
		if (principal != null && principal instanceof EdlUser) {
			return ((EdlUser) principal).getId();
		}
		return null;
	}

	@Override
	@Transactional(readOnly=true)
	public User getCurrentUser() {
		Object principal = securityContextFacade.getPrincipal();
		if (principal == null) {
			return null;
		}
		if (principal instanceof EdlUser) {
			User user = dao.findOne(((EdlUser) principal).getId());
			if (user != null) {
				return user;
			}
		}
		return null;
	}

	@Override
	@Transactional(readOnly=true)
	public void authenticate(String email, String password) {
		Authentication auth = new UsernamePasswordAuthenticationToken(email,
				password);
		SecurityContextHolder.clearContext();
		SecurityContextHolder.getContext().setAuthentication(auth);
	}

	@Override
	@Transactional(readOnly=true)
	public void authenticate(User user) {
		EdlUser edlUser = EdlUser.createFromUser(user);
		Authentication auth = new UsernamePasswordAuthenticationToken(edlUser,
				user.getPassword(), edlUser.getAuthorities());
		SecurityContextHolder.clearContext();
		SecurityContextHolder.getContext().setAuthentication(auth);
	}

	@Override
	public void updateLastAccess(Authentication auth) {
		EdlUser edl = (EdlUser) auth.getPrincipal();
		UserLoginEntity one = loginDao.findOne(edl.getId());
		Date date = new Date();
		if (one.getFirstAccess() == null) {
			one.setFirstAccess(date);
		}
		one.setLastAccess(date);
		Object details = auth.getDetails();
		if (details instanceof WebAuthenticationDetails) {
			WebAuthenticationDetails webDetails = (WebAuthenticationDetails) details;
			one.setLastIp(webDetails.getRemoteAddress());
		}
		loginDao.save(one);
	}


	@Override
	public String createLostPassToken(User user) {
		String plainToken = String.format("%s-%s", user.getId(),
				new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));
		String token = encoder.encode(plainToken);

		user.setLostPassToken(token);
		dao.save(user);

		return token;
	}

	@Override
	public void resetPassword(String receivedToken, String newPassword) {
		User u = dao.findOneByLostPassToken(receivedToken);
		String newPassToken = encoder.encode(newPassword);

		u.setPassword(newPassToken);
		u.setLostPassToken("");

		dao.save(u);
		authenticate(u.getEmail(), newPassword);
	}

	@Override
	public User createNewUser(String email)
			throws UserEmailUnavailableException {
		User emailUser = dao.findOneByEmail(email);
		if (emailUser != null) {
			throw new UserEmailUnavailableException(
					"user.email.alreadyRegistered");
		}
		if (!EmailUtil.isValidEmailAddress(email)) {
			throw new UserEmailUnavailableException("user.email.invalid");
		}
		User user = getCurrentUser();
		// email como username para nao da conflito
		user.setEmail(email);

		this.createLostPassToken(user);

		return user;
	}

	@Override
	public void setPassword(String receivedToken, String newPassword,
			String username) {
		User u = dao.findOneByLostPassToken(receivedToken);
		String newPassToken = encoder.encode(newPassword);
		boolean makeTimeline = !u.getCompleted();
		u.setPassword(newPassToken);
		u.setRoles(Roles.USER.toString());
		u.setLostPassToken("");
		u.setCompleted(true);
		dao.save(u);
		authenticate(username, newPassword);
	}

	@Override
	@Transactional(readOnly=true)
	public User findOne(Long id) {
		return dao.findOne(id);
	}

	@Override
	@Transactional(readOnly=true)
	public User findOneNotDeleted(Long id) {
		return dao.findOneNotDeleted(id);
	}

	@Override
	public User changePassword(Long userId, String old, String newPass)
			throws PasswordChangeException {
		User user = dao.findOne(userId);
		if (!encoder.matches(old, user.getPassword()))
			throw new PasswordChangeException();
		user.setPassword(encoder.encode(newPass));
		return dao.save(user);
	}

	public void setSecurityContextFacade(
			SecurityContextFacade securityContextFacade) {
		this.securityContextFacade = securityContextFacade;
	}


	public void registerUser(RegisterUserDTO dto) {

		User user = new User();
		UserInfo ui = new UserInfo();

		ui.setFullName(dto.getFullName());

		user.setInfo(ui);
		user.setEmail(dto.getEmail());
		user.setPassword(encoder.encode(dto.getPassword()));
		user.setRoles(Roles.USER.toString());
		user.setLostPassToken("");
		user.setCompleted(true);
		dao.save(user);

	}

	@Override
	@Transactional(readOnly=true)
	public User findOneByEmail(String email) {

		User user = dao.findOneByEmail(email);

		return user;
	}

	@Override
	public void editUser(ChangePasswordDTO dto) {

		User user = dao.findOne(dto.getUser().getId());
		user.setEmail(dto.getUser().getEmail());
		user.setInfo(dto.getUser().getInfo());
		user.setPassword(encoder.encode(dto.getPassword()));

		dao.save(user);
	}

}
