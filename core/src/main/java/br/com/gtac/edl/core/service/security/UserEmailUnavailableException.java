package br.com.gtac.edl.core.service.security;

import br.com.gtac.edl.core.exception.GuicheMessageException;

public class UserEmailUnavailableException extends GuicheMessageException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String DEFAULT_MESSAGE="application.email.unavailable"; 

	public UserEmailUnavailableException() {
		super(DEFAULT_MESSAGE);
	}

	public UserEmailUnavailableException(Throwable cause) {
		super(DEFAULT_MESSAGE,cause);
	}

	public UserEmailUnavailableException(String string) {
		super(string);
	}
	
	
	
	

}
