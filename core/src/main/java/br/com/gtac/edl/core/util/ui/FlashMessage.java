package br.com.gtac.edl.core.util.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.context.MessageSourceResolvable;

public class FlashMessage implements MessageSourceResolvable,Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -318028604700451386L;
	private List<String> codes = new ArrayList<String>();
	private List<String> arguments = new ArrayList<String>();

	private String messageType = "";
	

	private FlashMessage() {
	}

	public static FlashMessage create(String messageType, String[] codes,
			String[] arguments) {
		FlashMessage fm = new FlashMessage();
		fm.codes = Arrays.asList(codes);
		fm.arguments = Arrays.asList(arguments);
		fm.messageType = messageType;

		return fm;
	}

	public void addMessage(String code, String... arguments) {
		codes.add(code);
		this.arguments.addAll(Arrays.asList(arguments));
	}

	@Override
	public String[] getCodes() {
		return codes.toArray(new String[codes.size()]);
	}

	@Override
	public Object[] getArguments() {
		return arguments.toArray();
	}

	@Override
	public String getDefaultMessage() {
		return "";
	}

	public String getMessageType() {
		return messageType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FlashMessage [codes=").append(codes)
				.append(", arguments=").append(arguments)
				.append(", messageType=").append(messageType).append("]");
		return builder.toString();
	}

}
