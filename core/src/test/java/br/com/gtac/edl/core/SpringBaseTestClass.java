package br.com.gtac.edl.core;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.dbunit.util.fileloader.DataFileLoader;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

@ContextConfiguration(locations="/test-ctx.xml")
public abstract class SpringBaseTestClass extends AbstractTransactionalJUnit4SpringContextTests {
	
	@PersistenceContext
	protected EntityManager em;
	
	
	protected Map<String, Object> getReplacements(){
		final Map<String, Object> replacement= new HashMap<String, Object>();
			replacement.put("[null]", null);
			replacement.put("[now]", new Date());
			return replacement;
	}
	
	
	protected IDatabaseConnection getConnection(){
		try{
		Connection conn = DataSourceUtils
					.getConnection(((JdbcTemplate) simpleJdbcTemplate.getJdbcOperations()).getDataSource());
		return new DatabaseConnection(conn);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
		
	}
	
	/**SEMPRE EXECUTAR DENTRO DA TRANSAÇÃO. Ou seja, DENTRO de @Test
	 * @param file
	 */
	public void cleanLoadData(String file){
		try {
			executeOperation(file,DatabaseOperation.CLEAN_INSERT);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	/**SEMPRE EXECUTAR DENTRO DA TRANSAÇÃO. Ou seja, DENTRO de @Test
	 * @param file
	 */
	public void loadData(String file){
		try {
			executeOperation(file,DatabaseOperation.UPDATE);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	/**SEMPRE EXECUTAR DENTRO DA TRANSAÇÃO. Ou seja, DENTRO de @Test
	 * @param file
	 */
	public void deleteData(String file){
		try {
			executeOperation(file,DatabaseOperation.DELETE_ALL);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	@SuppressWarnings("deprecation")
	public void executeOperation(String file,DatabaseOperation operation) throws DatabaseUnitException, SQLException, IOException{
		DataFileLoader loader = new FlatXmlDataFileLoader(getReplacements());
		IDataSet dataset = loader.load("/dbunit/"+file);
		operation.execute(getConnection(), dataset);
	}
	

}
