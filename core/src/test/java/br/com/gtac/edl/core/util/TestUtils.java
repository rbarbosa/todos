package br.com.gtac.edl.core.util;

import org.springframework.aop.framework.Advised;

public class TestUtils {
	
	public static <T> T cast(Object object, Class<T> clazz) throws Exception{
		if(object instanceof Advised){
			return (T) ((Advised) object).getTargetSource().getTarget();
		}
		return (T) object;
	}

}
