package br.com.gtac.edl.core.service.security;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import br.com.gtac.edl.core.SpringBaseTestClass;

public class UserDetailsServiceImplTest extends SpringBaseTestClass{
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Test(expected=UsernameNotFoundException.class)
	public void testFindUserNotExistent(){
		userDetailsService.loadUserByUsername("teste123432101010101");
	}
	@Test
	public void testFindCaseInsensitive(){
		cleanLoadData("user.xml");
		EdlUser admin = (EdlUser) userDetailsService
				.loadUserByUsername("uSer@guichevIrtual.com");
		assertEquals(Long.valueOf(1L), admin.getId());
	}
	
	@Test
	public void testFindWithEmail(){

		cleanLoadData("user.xml");

		EdlUser admin = (EdlUser) userDetailsService
				.loadUserByUsername("user@guichevirtual.com");
		assertEquals(Long.valueOf(1L), admin.getId());
	}
	
	@Test(expected=UsernameNotFoundException.class)
	public void testWontFindEmpty(){
		assertNull(userDetailsService.loadUserByUsername(""));
	}


}
