package br.com.gtac.edl.web.helper;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;

public class MessageSourceStub implements MessageSource{

	@Override
	public String getMessage(String code, Object[] args, String defaultMessage,
			Locale locale) {
		return defaultMessage;
	}

	@Override
	public String getMessage(String code, Object[] args, Locale locale)
			throws NoSuchMessageException {
		return code;
	}

	@Override
	public String getMessage(MessageSourceResolvable resolvable, Locale locale)
			throws NoSuchMessageException {
		return null;
	}
	

}
