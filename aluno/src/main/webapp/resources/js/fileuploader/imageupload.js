(function($){
	$.fn.imageupload = function(options){
		var sizes = {P:'small',M:'medium',G:'large'};
		var settings = $.extend( {
		      'size'         : 'P',
		}, options);
		this.each(function(i,v){
			new qq.FileUploader({
			    // pass the dom node (ex. $(selector)[0] for jQuery users)
			    element: v,
			    // path to server-side upload script
			    action: '/admin/image/upload',
			    multiple: false,
			    allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
			    onComplete: function(id,filename,json){
			    	size = sizes[settings.size];
			    	multimediaId = json[size+'Id'];
			    	if(multimediaId){
			    		var img = $('img',v);
			    		img.attr('src','/admin/multimedia/view/'+multimediaId);
			    		$('input.id',v).val(json.id).change();
//			    		$('input.smallId',v).val(json.smallId);
			    	}
			    	$('ul',v).hide();
			    },
			    onSubmit: function(id, fileName){
			    	$('ul',v).show();
			    },
			    messages: settings.messages,
			    template: settings.template,
			});
		});
	};
})(jQuery);