function TodoListController($scope,$http){
			$scope.orderField = "priority";
			var refreshTodoLists = function(){
				$scope.newList={};
				$http.get(EDL.ROOT_PATH+'todos/rest/list').success(function(data){
					$scope.todolists = angular.fromJson(data);
					if($scope.todolists && $scope.todolists.length>0){
						$scope.list=$scope.todolists[0];
						$scope.order();
//						$scope.list.entries.splice(0,0,{});
					}
				}).error(function(data){
					errorNoty();
				});
			};
			var saveSuccess = function(text){
				setTimeout(function(){
				if(text)
					noty({text:text,type:'success',timeout:3000});
				else
					noty({text:'Save successful',type:'success',timeout:3});
				},100);
			};
			var errorNoty = function(text){
				setTimeout(function(){
				if(text)
					noty({text:text,type:'error',timeout:3000});
				else
					noty({text:'An error ocurred',type:'error',timeout:3});
				},100);
				refreshTodoLists();
			};
			refreshTodoLists();
			$scope.createTodoList = function(){
				$http.post(EDL.ROOT_PATH+'todos/rest/list',$scope.newList).success(function(data){
					refreshTodoLists();
				}).error(function(data){
					errorNoty();
				});
			};
			
			$scope.removeTodo = function(todo){
				$http.post(EDL.ROOT_PATH+'todos/rest/entry/remove/'+todo.id).success(function(data){
					refreshTodoLists();
					saveSuccess('Task removed');
				}).error(function(data){
					errorNoty();
				});
			};
			//saves a todo, creating or updating
			$scope.saveTodo = function(todo){
				if(todo.id){
					todo.parent = {id:$scope.list.id};
					$http.post(EDL.ROOT_PATH+'todos/rest/entry/'+todo.id,todo).success(function(data){
						refreshTodoLists();
						saveSuccess();
					}).error(function(data){
						errorNoty();
					});
				}else{
					todo.parent = {id:$scope.list.id};
					$http.post(EDL.ROOT_PATH+'todos/rest/entry',todo).success(function(data){
						refreshTodoLists();
						saveSuccess('New entry saved');
					}).error(function(data){
						errorNoty();
					});
				}
			};
			$scope.order = function(){
				if(!angular.isDefined($scope.orderField) || $scope.orderField===''){
					return;
				}
				var params = $scope.orderField.split('-');
				$scope.list.entries.sort(function(a,b){
					var field = params[0];
					if(field!=='completed' && a.completed!=b.completed){
						return a.completed ? 1:-1;
					}
					var av=a[field]||null;
					var bv = b[field]||null;
					if(av==bv)
						return 0;
					var r = av<bv;
					if(params[1]=='asc')
						return r ? -1:1;
					return r ? 1:-1;
				});
			};
			$scope.newEntry = function(){
				$scope.list.entries.splice(0,0,{edit:true});
			};
			$scope.options = {
				"5":"Critical",
				"4":"High",
				"3":"Medium",
				"2":"Low",
				"":"None"
			};
			$scope.formatDate = function(date){
				if(angular.isString(date)){
					date = $.datepicker.parseDate('yy-mm-dd',date);
				}
				return $.datepicker.formatDate('mm/dd/yy',date);
			};
			
			
			
			
		}
