<#import "/masterEmail.ftl" as layout>

<@layout.masterTemplate title="Bem Vindo!">
		<h2 class="title"><p>Olá ${name}, </p></h2>
		<hr>
		<p class="text">
			Para redefinir sua senha, clique no link: <a href="${resetLink}">recuperar senha</a> .<br>
			<br>Se isso não funcionar, copie o endereço abaixo e cole na barra de endereços do seu navegador:
		</p>
		<p class="text">${resetLink}</p>
		<p class="text">Atenciosamente, equipe Guichê Virtual</p>
			
</@layout.masterTemplate>