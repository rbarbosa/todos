<#macro masterTemplate title="defaultTitle">
  <!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </meta>
    <title>
      ${title}
    </title>
    <link href='http://fonts.googleapis.com/css?family=Rambla:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <style type="text/css">
      body{
      	font-family: 'Rambla', sans-serif;
        background-color: #DFDFDF!important;
        background:url('http://i49.tinypic.com/2h2egxc.png') repeat-x center top;
      }
      .body{
        background-color: #DFDFDF!important;
        background:url('http://i49.tinypic.com/2h2egxc.png') repeat-x center top;
      }
      .bgwhite{
        background-color: white;
      }
      #footer{
        color:white;
      }
      .content{
        margin:10px;
      }
      .title{
      color:#ffa500;
      text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.6)!important;
      }
      .text{
      color:#31849B;
      }
      h1{
      font-size: 46px;
      margin-bottom: -30px;
      }
      strong{
      color:#000000;
      }
    </style>
    
  </head>
  <body class="body" style="background-color: #31849B!important; background:url('http://i49.tinypic.com/2h2egxc.png') repeat-x center top;font-family: 'Rambla', sans-serif;">
    <table border="0" cellspacing="0" width="600" align=center>
      <tr>
        <td>
          <div id="header">
          	<center>
            	<img src="http://i49.tinypic.com/ip75o5.png" style="margin-bottom: 10px;" />
            </center>
          </div>
        </td>
      </tr>
      <tr class="bgwhite" style="background-color:white;">
        <td style="-webkit-border-radius: 10px; -moz-border-radius: 10px; border-radius: 10px;">
          <div id="content">
          	<div class="content" style="text-align: justify; ">
            	<#nested />
            	<br>
            </div>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <div id="footer">
            <img src="http://i48.tinypic.com/9r2ibs.png"/>
            <br>
              Email enviado por Guichê Virtual®
              <br>
              Todos os direitos reservados
            </div>
          </td>
      </tr>
      <tr>
        <td>
          <br>
          Conecte-se ao Guichê Virtual também nas redes sociais:
          <br>
          <div style="display:inline;">
            <a href="https://www.facebook.com/pages/Guich%C3%AA-Virtual/126311034187483" target="_blank">
              <img src="http://i47.tinypic.com/jj3a52.png"/>
            </a>
            <a href="https://twitter.com/guichevirtual" target="_blank">
              <img src="http://i48.tinypic.com/246pe0m.png"/>
            </a>
          </div>
        </td>
      </tr>
    </body>
  </html>
</#macro>