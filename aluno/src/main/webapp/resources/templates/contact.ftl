<#import "/masterEmail.ftl" as layout>

<@layout.masterTemplate title="Email de Contato">
		<h2 class="title"><p>Olá,</p></h2>
		<hr>
		<p class="text">
		Usuário: ${contact.name}<br> 
		E-mail: ${contact.email}<br>
		Mensagem:<br>
			${contact.message}<br>
		</p>
		<p class="text">Atenciosamente, equipe Guichê Virtual</p>
			
</@layout.masterTemplate>