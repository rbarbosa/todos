<#import "/masterEmail.ftl" as layout>

<@layout.masterTemplate title="Compra Cancelada">
		<h2>Olá ${user.info.firstName},</h2>
		<p class="text">
			<#if reason??>
			 	${reason}
			<#else>
			<p class="text">
				Sua compra foi cancelada por um dos seguintes motivos:<br>
				<li>Seu cartão não foi aprovado. Nesse caso entre em contanto com a administradora do seu cartão.</li>
				<li>Foi efetuado um pedido de cancelamento e reembolso do seu dinheiro.</li>
				<li>Sua reserva expirou antes da confirmação do cartão.</li>
			</p>
			</#if>
			Acesse seu pedido: <a href="${url}">${url}</a><br>
			<br>
			Qualquer dúvida entre em contato com a nossa equipe: <a href="mailto:contanto@guichevirtual.com.br?subject=Cancelamento da Compra">contato@guichevirtual.com.br</a>
		</p>
</@layout.masterTemplate>