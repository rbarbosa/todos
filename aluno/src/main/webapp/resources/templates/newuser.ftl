<#import "/masterEmail.ftl" as layout>

<@layout.masterTemplate title="Bem Vindo!">	
		<h2 class="title"><p>Ol&aacute; ${name}, </p></h2>
		<hr>
		<p class="text">
			<b style="font-size: 22px;">Seu cadastro foi realizado com sucesso!</b><p>
		</p>	
			<h3>Vantagens de se usar o Guich&ecirc; Virtual</h3>
		<p class="text">	
			Em nosso site voc&ecirc; pode comprar passagens para maioria dos estados
			brasileiros de uma maneira simples, pr&aacute;tica e r&aacute;pida, com a vantagem
			de comprar no conforto de sua casa, indo a rodovi&aacute;ria somente uma 
			vez, pouco antes da sua viagem! <br>
			
			<br><b>Conhe&ccedil;a algumas de nossas vantagens:</b>
			
			<ul class="unstyled text">
  				<li>Melhores formas de pagamento do mercado;</li>
  				<li>Menores taxas;</li>
  				<li>Pesquisou e pagou! Sem burocracia, Simples, Pr&aacute;tico e R&aacute;pido;</li>
  				<li>Site Seguro! Trabalhamos ao m&aacute;ximo para a garantir a seguran&ccedil;a em todas as suas compras;</li>
			</ul>
		</p>
			<h3>Como retirar a passagem?</h3>
		<p class="text">
			 Para retirar sua passagem, &eacute; necess&aacute;rio apenas apresentar o documento de identifica&ccedil;ão pessoal no 
			 guich&ecirc; da empresa rodovi&aacute;ria meia hora antes da viagem.<br><br>
			 
			Assine nosso boletim de not&iacute;cias e fique por dentro de todas as novidades!<br><br>
	
			<b style="font-size: 22px;">Seja bem vindo ao Guich&ecirc; Virtual!</b><br>
			Confira: <a href="http://www.guichevirtual.com.br/">www.guichevirtual.com.br/</a> 
		</p>
</@layout.masterTemplate>