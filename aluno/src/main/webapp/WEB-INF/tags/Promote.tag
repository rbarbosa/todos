<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="gtac" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="g" tagdir="/WEB-INF/tags"%>

<div class="promote">
	<div>
		<div class="borderPromote">
			<center>
				<h3 class="textw fonteEspecial" style="font-size: 23px!important; position: relative; top: -4px;">Por que comprar pela internet?</h3>
			</center>
		</div>
		<g:img src="resources/img/promote4.png" style="float: right; width: 350px; margin-right: -50px; margin-top: -10px;" alt="Garanta seu lugar no ônibus, comprando no conforto de sua casa, sem ir à rodoviária" />
		<g:img src="resources/img/promote5.png" style="margin-top:-20px; width:350px;" alt="Meia hora antes da sua viagem, retire a passagem no guichê da empresa de ônibus, com seu documento em mãos" />
		<g:img src="resources/img/promote6.png" style="margin-top:-20px; float: right; width: 350px; margin-right: -50px;" alt="Pronto! Agora é só embarcar rumo ao seu destino e Boa Viagem!" />
	</div>
</div>