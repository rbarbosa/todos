<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="gtac" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="gtacForm" tagdir="/WEB-INF/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
  <!--header-->
    <div id="header" class="off">
      <div class="container">

        <div id="nav">
          <a href="<spring:url value="/"/>" class="home" tabindex="1" title="Página Inicial"><span></span>Home</a>
          <a href="<spring:url value="/itinerario"/>" class="routes" tabindex="2" title="Itinerários"><span></span><spring:message code="itineraries"/></a>
          <a href="#oportunidades" class="opportunities" tabindex="3" title="Oportunidades"><span></span><spring:message code="opportunities"/></a>
          <a href="<spring:url value="/conteudos"/>" class="communities" tabindex="4" title="Comunidades"><span></span><spring:message code="communities"/></a>

          <div class="join">
            <a href="#login" class="login active">Login</a>
            <a href="#register" class="register"><spring:message code="profile.register"/></a>
			
            <form action="<spring:url value="/j_spring_security_check"/>" id="form-login" method="POST">
              <spring:message code="login.user" var="login_user"></spring:message>
              <input type="text" placeholder="${login_user}" name="j_username" maxlength="255"/>
              <input type="password" placeholder="senha" name="j_password" maxlength="255"/>
              <input type="submit" value="Ok" title="Clique para realizar login." />

              <a href="#help" class="help" id="login-help" title="<spring:message code="login.reset.help"/>" >Ajuda</a>
              <div class="clearfix">
                <a href="#facebook" class="facebook">Facebook</a>
                <a href="#gmail" class="gmail">Gmail</a>
              </div>
              <span class="error fail">&raquo; <spring:message code="login.error.fail"/> &laquo;</span>
              <span class="error email">&raquo; <spring:message code="login.error.email"/> &laquo;</span>
              <span class="error empty">&raquo; <spring:message code="login.error.empty"/> &laquo;</span>
              <span class="accepted okForgotten">&raquo; <spring:message code="login.forgot.success"/> &laquo;</span>
              <span class="error emailForgotten">&raquo; <spring:message code="login.error.emailForgotten"/> &laquo;</span>
            </form>
            <form action="<spring:url value="/user/create"/>" id="form-register">
              <input type="email" placeholder="e-mail" name="email" maxlength="300"/>
              <input type="submit" value="Ok" title="Clique para se cadastrar no site." />
              <a href="#help" class="help" title="Se não possui e-mail, clique aqui!" >Ajuda</a>
              <div class="terms">
                <input type="checkbox" name="term"/>
                <label style="display: inline;">Aceito os <a href="#termos_de_uso">termos de uso</a></label>
              </div>

              <div class="clearfix">
                <a href="#facebook" class="facebook">Facebook</a>
                <a href="#gmail" class="gmail">Gmail</a>
              </div>
               <span class="error fail">&raquo; <spring:message code="login.error.fail"/> &laquo;</span>
               <span class="error email">&raquo; <spring:message code="login.error.email"/> &laquo;</span>
               <span class="error empty">&raquo; <spring:message code="login.error.empty"/> &laquo;</span>
               <span class="accepted ok">&raquo; <spring:message code="user.email.success"/> &laquo;</span>
               <span class="error term">&raquo; <spring:message code="login.error.term" /> &laquo;</span>
            </form>
          </div>
        </div>

        <gtacForm:searchBox />

        <div id="social">
          <div class="fb-like" data-send="false" data-layout="button_count" data-width="120" data-show-faces="true"></div>
         <a href="https://twitter.com/share?url=/" class="twitter-share-button" data-count="none" data-hashtags="edulivre" >Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
        </div>
      </div>
    </div>
    <c:if test="${showLoginError }">
    <script type="text/javascript">
    	$(document).ready(function(){
    		HeaderLoginController.errorEffect($('#form-login')[0],'fail');
    	});
    </script>
    </c:if>
    <!--/header-->