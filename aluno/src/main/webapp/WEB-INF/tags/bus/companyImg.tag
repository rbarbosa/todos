<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib  prefix="g" tagdir="/WEB-INF/tags"%>
<%@ taglib  prefix="helper" uri="/WEB-INF/tags/helper.tld"%>
<%@ attribute name="key" description="Chave da companhia de onibus" required="false" type="java.lang.String" %>
<%@ attribute name="size" description="T,P,M,G default T" required="false" type="java.lang.String" %>
<%@ attribute name="cssClass" description="Css Class" required="false" type="java.lang.String" %>
<%@ attribute name="cssStyle" description="css Style" required="false" type="java.lang.String" %>
<c:set var="key" value="${empty key ? 'DEFAULT' : key }"/>
<c:set var="size" value="${empty size ? 'T' : size }"/>
<helper:BusCompanyTag var="company" key="${key}"/>
<c:if test="${not empty company }">
<g:cimg size="${size}" imageId="${company.image.id}" fallbackImage="/resources/images/company-default.png" alt="${company.name}" 
cssClass="${cssClass }" style="${cssStyle }" title="${company.name }"/>
</c:if>
<c:if test="${empty company }">
	<g:img src="/resources/images/company-default.png" alt="${key}" cssClass="${cssClass }" style="${cssStyle }" ></g:img>
</c:if>



