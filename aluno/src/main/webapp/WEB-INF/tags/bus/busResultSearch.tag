<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="g" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="bus" tagdir="/WEB-INF/tags/bus"%>
<%@ taglib prefix="helper" uri="/WEB-INF/tags/helper.tld"%>
<%@ taglib prefix="f" tagdir="/WEB-INF/tags/form"%>

<%@ attribute name="result" description="List of results the search" 
			  required="true" type="java.util.Collection" %>
<%@ attribute name="travelName" description="Name of layout(Go or Back)" 
			  required="true" type="java.lang.String" %>
<%@ attribute name="image" description="Path image" required="true"
			  type="java.lang.String" %>
<%@ attribute name="travelDate" description="Selection date Go or Back"
			  required="true" type="java.util.Date" %>			  


 <div>
            <br>
            <table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 0px!important;">
              <tr>
                <td valign=top>
                  <g:img src="${image}">
                  </g:img>
                  <div class="setSizeTable">
                    <table class="table table-bordered table-hover tableResults">
                      <thead>
                        <tr class="setSizeTR" style="color: white; text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.6) !important;">
                          <td class="td1 sortable" style="text-align: center;" data-sort="company" data-sortfn="byimg">
                            Empresa <i></i>
                          </td>
                          <td class="td2 sortable" style="text-align: left;" data-sort="price" data-sortfn="bynumber">
                            Preço<i></i>
                          </td>
                          <td class="td3 sortable" style="text-align: center;" data-sort="available" data-sortfn="bynumber">
                            Disponíveis<i></i>
                          </td>
                          <td class="td4 sortable" style="text-align: center;" data-sort="type">
                            Tipo<i></i>
                          </td>
                          <td style="width: 137px; text-align: center;" class="sortable asc" data-sort="departure" data-sortfn="bytime">
                            Saída <i class="icon-chevron-down"></i>
                          </td>
                          <td style="width: 137px; text-align: center;" class="sortable" data-sort="arrival" data-sortfn="bytime">
                            Chegada <i></i>
                          </td>
                        </tr>
                        <tr class="setSizeTR2">
                          <td class="td1">
                          </td>
                          <td class="td2">
                          </td>
                          <td class="td3">
                          </td>
                          <td class="td4">
                          </td>
                          <td style="width: 136px;">
                          </td>
                          <td style="width: 136px;">
                          </td>
                        </tr>
                      </thead>
                      <tbody>
                      <c:if test="${empty result}">
                      	<td colspan="6">Nenhum resultado encontrado</td>
                      	<tr>
                      	</tr>
                      </c:if>
                      <c:forEach items="${result}" var="item">
                          <tr class="backgroundSearch search-result" style="background-color: white;">
                            <td class="td1 company">
                              <label><input type="radio" name="service${travelName}" data-type="${travelName}" 
                              value="${item.servico},${item.grupo},${item.empresa}" class="bus-select" id="${item.servico},${item.grupo},${item.empresa}" />
                              <center><bus:companyImg key="${item.empresa}" size="P" cssStyle="max-width: 80px; max-height:30px;margin-top: -20px; margin-bottom: -5px;"/></center></label>
                            </td>
                            <td class="td2 resultsCell price">
                              <label for="${item.servico},${item.grupo},${item.empresa}"> <fmt:formatNumber maxFractionDigits="2" minFractionDigits="2" value="${item.preco}" /> </label>
                            </td>
                            <td class="td3 resultsCell available">
                              <label for="${item.servico},${item.grupo},${item.empresa}">${item.assentosLivres }</label>
                            </td>
                            <td class="td4 resultsCell type">
                              <label for="${item.servico},${item.grupo},${item.empresa}">${item.classe }</label>	
                            </td>
                            <td style="width: 135px;" class="resultsCell departure" >
                              <label for="${item.servico},${item.grupo},${item.empresa}"><helper:BusDateFormat date="${item.saida }"
                              fromDate="${travelDate}" /> &nbsp; <helper:BusDateFormat date="${item.saida }" fromDate="${travelDate}" type="time" /></label>
                            </td>
                            <td style="width: 135px;" class="resultsCell arrival">
                              <label for="${item.servico},${item.grupo},${item.empresa}"><helper:BusDateFormat date="${item.chegada }"
                              fromDate="${travelDate}" />&nbsp;
                              <helper:BusDateFormat date="${item.chegada }"
                              fromDate="${travelDate}" type="time" /></label>
                            </td>
                          </tr>
                      </c:forEach>
                      </tbody>
                    </table>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                <br>
                  <div id="layout-${travelName}">
                    <!-- preenchido com layout de onibus de ida -->
                  </div>
                </td>
              </tr>
            </table>
          </div>
