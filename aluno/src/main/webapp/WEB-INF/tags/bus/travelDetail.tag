<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ tag body-content="empty"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="f" tagdir="/WEB-INF/tags/form"%>
<%@ taglib prefix="gtac" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="helper" uri="/WEB-INF/tags/helper.tld"%>
<%@ attribute name="detail" required="true" type="br.com.gtac.edl.core.service.companies.j3.data.J3TravelDetail" %>
<%@ attribute name="date" required="true" type="java.util.Date" %>
<%@ attribute name="go" required="true" type="java.lang.Boolean" %>

<div class="well" style="width: auto;" >
	<div class="tabbable" style="margin-top: -15px;">
	<c:if test="${detail.layout.size() gt 1 }">
		<ul class="nav nav-tabs">
			<c:forEach items="${detail.layout }" var="section" varStatus="status">
				<li class="${status.index==0?'active':''}">
			     <a href="#${detail.servico }${section.nome}" data-toggle="tab"><b>${section.nome}</b></a>
			    </li>
			</c:forEach>
			</ul>
	</c:if>
		<div class="busPosition tab-content">
			<c:forEach items="${detail.layout }" var="section" varStatus="status">
				<div id="${detail.servico }${section.nome}" class="section tab-pane ${status.index==0?'active':''}" style="margin-top: -20px;">
					<br>
					<table>
						<tr>
							<td style="width: 115px;" border="0" cellspacing="0">
								<div class="bus-front"></div>
							</td>
							<td border="0" cellspacing="0">
								<div  class="bus-layout" style="padding-top: 8px; margin-top: 8px;">
									<div style="float: left;margin-top: -20px;">
										<c:set var="lastX" value="${0}"/>
										<c:set var="lastY" value="${0}"/>
										<c:forEach items="${section.seats }" var="seat">
											<c:if test="${not empty lastY and not (seat.y eq lastY)}"><br/></c:if>
											<c:if test="${not empty lastY and lastY-seat.y gt 1}"><br><br></c:if>
											<input type="checkbox" class="busSeat max-seats ${seat.info.ocupado?'seat-ocuppied':'' }" id="${detail.servico}-${section.nome}-${seat.info.numero}" name="${go ? 'seatsGo' : 'seatsBack'}" value="${seat.info.numero}" 
												${seat.info.ocupado ? 'disabled':''} ><label for="${detail.servico}-${section.nome}-${seat.info.numero}" class="${seat.info.ocupado?'seat-ocuppied':'' } sizeSeat" style="margin-top: -14px;"><center class="seat-number">${seat.info.numero }</center></label>
											<!-- </div> -->
											<c:set var="lastX" value="${seat.x}"/>
											<c:set var="lastY" value="${seat.y}"/>
										</c:forEach>
									</div>
								</div>
							</td>
							<td style="width: 50px;" border="0" cellspacing="0">
								<div class="bus-back"></div>
							</td>
						</tr>
					</table>
				</div>
			</c:forEach>
		</div>
		<div class="divLegenda">
		<b>Legenda:</b>	<gtac:img src="/resources/img/cadeira-Livre2.png" cssClass="legenda"/> Cadeira livre | 
						<gtac:img src="/resources/img/cadeira-Selecionada2.png" cssClass="legenda"/> Cadeira selecionada | 
						<gtac:img src="/resources/img/cadeira-Ocupada2.png" cssClass="legenda"/> Cadeira ocupada
		</div>	
	</div>
</div>



