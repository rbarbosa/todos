<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ attribute name="imageId" description="Id da imagem a ser carregada" required="true" type="java.lang.String" %>
<%@ attribute name="fallbackImage" description="url da imagem a ser colocada não imageId não exista" required="false" type="java.lang.String" %>
<%@ attribute name="size" description="tamanho da imagem a ser carregada (P,M,G)" required="true" type="java.lang.String" %>
<%@ attribute name="cssClass" description="Classe css da imagem" required="false" type="java.lang.String" %>
<%@ attribute name="style" description="Style da imagem" required="false" type="java.lang.String" %>
<%@ attribute name="alt" description="atributo da HTML" required="false" type="java.lang.String" %>
<%@ attribute name="title" description="atributo da HTML" required="false" type="java.lang.String" %>
<%@ attribute name="onMouseOver" description="onMouseOver imagen" required="false" type="java.lang.String" %>
<%@ attribute name="onMouseOut" description="onMouseOut imagen" required="false" type="java.lang.String" %>



<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<spring:url value="${fallbackImage}" var="in_url"></spring:url>
<c:if test="${not empty imageId }">
<c:url value="/image/view/${imageId}?size=${size}" var="in_url"></c:url>
</c:if>
<c:set value="" var="alt_att"></c:set>
<c:if test="${not empty alt }">
	<c:set value="alt='${alt}'" var="alt_att"></c:set>
</c:if>
<c:set value="" var="title_att"></c:set>
<c:if test="${not empty title }">
	<c:set value="title='${title}'" var="title_att"></c:set>
</c:if>
<c:set value="" var="css_att"></c:set>
<c:if test="${not empty cssClass }">
	<c:set value="class='${cssClass}'" var="css_att"></c:set>
</c:if>
<c:set value="" var="style_att"></c:set>
<c:if test="${not empty style }">
	<c:set value="style='${style}'" var="style_att"></c:set>
</c:if>

<img src="${in_url}" ${css_att} ${style_att} ${alt_att} ${title_att}/>
