<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="gtac" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="g" tagdir="/WEB-INF/tags"%>

<%@ attribute name="links" type="java.util.Collection" required="true" %>



<!-- slideshow -->
<script type="text/javascript">

	function slideSwitch() {
	    var $active = $('#slideshow DIV.active');
	
	    if ( $active.length == 0 ) $active = $('#slideshow DIV:last');
	
	    var $next =  $active.next().length ? $active.next()
	        : $('#slideshow DIV:first');
	
	    // Descomente para deixar randômico
	    // var $sibs  = $active.siblings();
	    // var rndNum = Math.floor(Math.random() * $sibs.length );
	    // var $next  = $( $sibs[ rndNum ] );
	
	
	    $active.addClass('last-active');
	
	    $next.css({opacity: 0.0})
	        .addClass('active')
	        .animate({opacity: 1.0}, 1000, function() {
	            $active.removeClass('active last-active');
	        });
	}
	
	$(function() {
	    setInterval( "slideSwitch()", 5000 );
	});

</script>
<style type="text/css">

/*** set the width and height to match your images **/

#slideshow {
    position:relative;
}

#slideshow DIV {
    position:absolute;
    top:0;
    left:0;
    z-index:1;
    opacity:0.0;
}

#slideshow DIV.active {
    z-index:1;
    opacity:1.0;
}

#slideshow DIV.last-active {
    z-index:1;
}

#slideshow DIV IMG {
    display: block;
    border: 0;
    margin-bottom: 10px;
    width: 480px;
    height: 320px;
}

#slideshow DIV SPAN {
	position: absolute;
	margin-top: -60px;
	margin-left: 10px;
	padding-left: 5px;
	padding-right: 5px;
	background: rgba(0, 0, 0, 0.60);
	color: rgb(255, 153, 0);
}

</style>


<br>
<div class="topDestination">
	<div class="coluna1" style="margin-top: -23px !important;">
		<center>
			<h3 class="textw fonteEspecial" style="font-size: 23px!important; position: relative; top: -4px;">Dicas de Viagem</h3>
		</center>
	</div>
	<jsp:useBean id="used" class="java.util.HashMap"></jsp:useBean>
	<div class="coluna4">
		<div class="inline">
			
			<div id="slideshow">
    			<c:set var="count" value="${0}"></c:set>
		        	<c:forEach items="${links}" var="link">
		        		<c:if test="${count<20 and ( empty used[link.url] ) and link.type eq 'BIG'}">
							<div>
								<a href="${link.url}" target="_blank">
					            	<gtac:cimg size="G" imageId="${link.image.id}"></gtac:cimg>
				            	</a>
           						<span><strong><a href="${link.url}" target="_blank">${link.title }</a> </strong><br />${link.description}</span>
           					</div>
           				<c:set var="count" value="${count+1}"></c:set>
	        		</c:if>
	        	</c:forEach>
           </div>
		</div>
		
	</div>
</div>