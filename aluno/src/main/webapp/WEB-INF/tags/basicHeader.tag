<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="gtac" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>

<div id="loading-header">Carregando...</div>
<!-- The top navigation menu -->
<a href="" name="topo" id="topo"></a>
<div class="navbar navbar-fixed-top noPrint ">
	<div class="navbar">
		<div class="navbar-inner backgroundNavbar" style="height: 100px;">
			<div class="container">
				<div class="header colorMenu">
					<gtac:a href="/">
						<gtac:img src="/resources/img/logo.png" cssClass="logo" />
					</gtac:a>
					<div class="container inline colorMenu"
						style="margin-top: 8px;">
						<div class="nav2 colorMenu">
							<div class="menu colorMenu" style="margin-left: 19px;">
								<p style="font-size: 20px;">
									<gtac:a href="/" cssClass="noUnderline"
										cssStyle="margin-left: 25px; margin-right:2px;">Início</gtac:a>
									|
									<gtac:a href="/perfil#secondary" cssClass="noUnderline"
										cssStyle="margin-left: 2px; margin-right: 2px;">Minhas Viagens</gtac:a>
									|
									<gtac:a href="/rotas" cssClass="noUnderline"
										cssStyle="margin-left: 2px; margin-right: 2px;">Principais Rotas</gtac:a>
									|
									<gtac:a href="/faq" cssClass="noUnderline"
										cssStyle="margin-left: 3px; margin-right: 3px;">Dúvidas</gtac:a>
									|
									<gtac:a href="http://www.guichevirtual.com.br/blog" target="_blank" cssClass="noUnderline"
										cssStyle="margin-left: 3px; margin-right: 3px;">Blog</gtac:a>
									|
									<gtac:a href="/contato" cssClass="noUnderline"
										cssStyle="margin-left: 3px;">Fale Conosco</gtac:a>
								</p>
							</div>
							<div style="margin-left: 50px;">
								<gtac:socialIntegration />
							</div>
								
							<div>
								<div class="busca" style="margin-top: 0px; width: 450px;">
									<sec:authorize access="hasRole('ROLE_USER')" var="isUser" />
									<sec:authentication property="principal" var="principal" />

									
									<div class="noUnderline">
										<c:if test="${isUser }">
										<div class="fonteEspecial" style=" margin-top: 8px; position: absolute;color: #31849b!important; font-size: 15px;">Atendimento: <a href="tel:(12) 3206 1332">(12) 3206 1332</a></div>
											<div class="well"
												style="padding: 4px; min-width: 120px; margin-right: 64px; margin-top: 5px; float: right; text-align: center;">
												<a href='<spring:url value="/perfil"></spring:url>'
													class="noUnderline"><i class="icon-user"></i>
													<c:if test="${not empty principal.user.info.firstName}">
														${principal.user.info.firstName}
													</c:if>
													<c:if test="${empty principal.user.info.firstName}">
														Meus Dados
													</c:if>
													</a> |
												<gtac:a href="/j_spring_security_logout"
													cssClass="noUnderline">
													<i class="icon-remove"></i> Sair</gtac:a>
											</div>
										</c:if>
									</div>

									<c:if test="${not isUser }">
										<c:url var="loginUrl" value="/j_spring_security_check" />
										<c:if test="${not isLogin}">
											<script>
												$(document).ready(function() {
													$("#openLogin").click(
														function() {
															$("#loginForm").toggle();
														});
												});
											</script>
											
										<div style="float: right; margin-right: 65px;">
										<div class="fonteEspecial" style=" margin-top: 8px; position: absolute;color: #31849b!important; font-size: 15px;">Atendimento: <a href="tel:(12) 3206 1332">(12) 3206 1332</a></div>
											<label style="margin-right: -275px; margin-left: 227px;"><gtac:a cssClass="btn btn-link" href="/login?register=true" cssStyle="font-size:12px;">Cadastre-se</gtac:a></label>
											<button class="btn btn-mini" type="button" id="openLogin"
												style="margin-left: 275px;">
												<i class="icon-user"></i> Login
											</button>
										</div>

											<div id="loginForm"
												style="display: none; background-color: white; position: relative; z-index: 10; width: 285px; margin-left: 185px; border: 1px solid #B8B8B8; margin-top: 35px;">
												<form class="form-inline margin10" action="${loginUrl }"
													method="post" style="padding: 15px;">
													<input type="text" id="j_username" name="j_username"
														style="width: 220px;"
														placeholder='<spring:message code="application.username"/>'><br>
													<input type="password" id="j_password" name="j_password"
														style="width: 220px;"
														placeholder='<spring:message code="application.password"/>'><br>
													<button type="submit" class="btn" style="width: 235px;">
														<spring:message code="application.login.button" />
													</button>
													<br>
													<div class="control-group" style="margin-top: -10px; display: inline;">
														<input type="checkbox" name="_spring_security_remember_me"
															id="keepConnected" style="color: #31849B;"> <label
															for="keepConnected"
															style="margin-top: 5px; width: 167px; color: #31849B !important;"><spring:message
																code="application.remember" /></label>
													</div>
													<br> <a href="#forgett" id="login-help"
														style="margin-left: 17px;"> <label id="forgett-msg"
														style="color: #31849B; cursor: pointer; width: 150px;"><spring:message
																code="application.forget" /></label>
																</a><br> <br>
													<div
														style="background: #31849B; width: 285px; margin-left: -25px;">
														<center>
															<label style="margin-left: 6px; font-size: 16px;">
																<a href='<spring:url value="/login?register=true"/>'
																class="btn btn-info"><b><spring:message
																			code="application.signup" /></b></a>
															</label>
														</center>
													</div>
												</form>
											</div>


										</c:if>
									</c:if>

								</div>
							</div>
						</div>
					</div>
				</div>
				<br>

			</div>
		</div>
	</div>
	<div></div>
</div>
