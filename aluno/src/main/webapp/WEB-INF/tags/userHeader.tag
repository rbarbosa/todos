<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="gtac" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="gtacForm" tagdir="/WEB-INF/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
    <!--header-->
    <div id="header">
      <div class="container">
        <a href="<spring:url value="/"/>" id="logo" title="Educação Livre - Página Inicial">Educação Livre</a>

        <div id="nav">
        	
          <a href="<spring:url value="/"/>" class="home" tabindex="1" title="Página Inicial"><span></span>Home</a>
          <a href="<spring:url value="/itinerario"/>" class="routes" tabindex="2" title="Itinerários"><span></span><spring:message code="itineraries"/></a>
          <a href="#oportunidades" class="opportunities" tabindex="3" title="Oportunidades"><span></span><spring:message code="opportunities"/></a>
          <a href="<spring:url value="/conteudos"/>" class="communities" tabindex="4" title="Comunidades"><span></span><spring:message code="communities"/></a>

          <div class="join">
          	<a href="/aluno/j_spring_security_logout" class="logout"><spring:message code="logout"/></a>
          	<spring:url value="/userinfo/edit" var="edit_url"></spring:url>
            <a href="${edit_url}" class="edit"><spring:message code="profile.edit"/></a>
            

            <!-- start info user -->
            <div id="user-info">
            <gtac:a href="/user/perfil">
            	<gtac:userImg includeLink="false"></gtac:userImg>
            </gtac:a>
              <strong class="name"><sec:authentication property="principal.username"/> </strong>
              <span class="description">Guru</span>
              <div class="badges">
                <img src="/aluno/resources/images/ico/badge.png" />
                <img src="/aluno/resources/images/ico/badge.png" />
                <img src="/aluno/resources/images/ico/badge.png" />
              </div>

              <div class="info">
                <div class="proficiency">
                  <span class="title">Proficiência</span>
                  <div class="progress">
                    <div class="bar" style="width: 80%"></div>
                  </div>
                </div>

                <div class="score">
                  <span class="title">Pontuação</span>
                  <span class="value">275</span>
                </div>

                <div class="goal">
                  <span class="title">Meta</span>
                  <span class="value">3/20</span>
                </div>
              </div>

            </div>

          </div>
        </div>
        
        <gtacForm:searchBox />

       <div id="social">
          <div class="fb-like" data-href="https://www.facebook.com/ZigottoTecnologia" data-send="false" data-layout="button_count" data-width="120" data-show-faces="true"></div>
          <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.zigotto.com.br" data-text="Modelo de texto para twitter" data-via="zigotto" data-lang="pt" data-related="zigotto" data-count="none" data-hashtags="zigotto">Tweetar</a>
          <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
        </div>
      </div>
    </div>
    <!--/header-->
    