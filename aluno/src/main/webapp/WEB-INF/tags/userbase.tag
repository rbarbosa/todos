<!DOCTYPE h1 PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="gtac" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="gtacForm" tagdir="/WEB-INF/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ attribute name="titleCode" description="Title code" required="true" type="java.lang.String" %>
<gtac:master>
	<jsp:body>
	<!--content-->
    <div id="content">
      <div class="new-user">
       	<h1><spring:message code="${titleCode}"/></h1>
		<jsp:doBody/>
      </div>
      <br class="clearfix" />
    </div>
    <!--/content-->
	
	</jsp:body>
</gtac:master>