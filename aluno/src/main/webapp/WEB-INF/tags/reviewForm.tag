<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ attribute name="reviewState" description="ReviewState da operacao de compra" required="true" type="br.com.gtac.edl.domain.review.ReviewState" %>
<%@ attribute name="operationId" description="Id da operacao relacionada" required="true" type="java.lang.Long" %>
<c:if test="${empty reviewState  or reviewState eq 'NONE'}">

<div id="review-form-modal" class="modal hide fade">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>Queremos saber como foi sua experiência conosco</h3>
  </div>
  <div id="review-body" class="modal-body" data-operation="${operationId}">
    
  </div>
  <div class="modal-footer">
    <a href="javascript:void(0);" class="btn" data-dismiss="modal">Aah... hoje não.</a>
    <a href="javascript:void(0);" class="btn btn-primary" data-dismiss="">Enviar</a>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		var opId = $('#review-body').data('operation');
		$('#review-body').load(EDL.ROOT_PATH+'/pesquisa?id='+opId,function(){
			$('#review-form-modal').modal({backdrop:false}).modal('show');
			$('#review-form-modal .btn-primary').click(function(e){
				$('#review-form-modal form').submit();	
			});
			$('#review-form-modal form').submit(function(e){
				var serial = $(e.target).serialize();
				$.post(EDL.ROOT_PATH+'/pesquisa/salvar',serial,function(data){
					noty({text:'Muito obrigado!! :)',type:'success',layout:'center'});	
				});
				$('.modal').modal('hide');
				return false;
			});
		});
	});
</script>
</c:if>
<c:if test="${not empty reviewState and not (reviewState eq 'READ' or reviewState eq 'ANSWERED') }"></c:if>