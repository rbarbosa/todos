<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute name="href" description="URL absoluta do arquivo a ser importado" required="true" type="java.lang.String" %>
<%@ attribute name="rel" description="Campo html" required="false" type="java.lang.String" %>
<%@ attribute name="type" description="Campo html" required="false" type="java.lang.String" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:url value="${href}" var="in_url" ></c:url>
<link href="${in_url}" rel="${rel}" type="${type}" />