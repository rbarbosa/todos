<%@ tag language="java" pageEncoding="UTF-8" body-content="empty"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="gtac"%>


<script type="text/javascript">
	$(function(){
		$.extend($.noty.defaults,{
			timeout:5000,
			layout:'bottomCenter'
		});
	});
</script>

<c:forEach items="${flash}" var="item" varStatus="status">
	<script type="text/javascript">
	$(function(){
		setTimeout(
			function(){
				noty({text: '<spring:message message="${item}" />', type: '${item.messageType}'});
			},500 * ${status.count - 1});
	});
	</script>
</c:forEach>