<%@ tag language="java" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<c:url value="/j_spring_security_check" var="in_url" ></c:url>
<form class="form-horizontal" action="${in_url}"
	method="post">

	<div class="control-group" style="margin-bottom: 0px!important; margin-top: 40px;">
		<input type="text" id="username" name="j_username"
			placeholder='<spring:message code="application.username"/>'>
		<input type="password" id="password" name="j_password"
			placeholder='<spring:message code="application.password" />'>
		<div class="control-group">
			<input type="checkbox" name="_spring_security_remember_me"
				id="keepConnectedLogin" style="margin-top: 0px!important;"> <label for="keepConnectedLogin"><spring:message
					code="application.remember" /></label><br>
			<a href="#forgett" id="login-singup" style="margin-left: 17px;"> 
			<label  style="color: #31849B; cursor: pointer;" >
					<spring:message code="application.forget" /></label></a>
			<button type="submit" class="btn" style="width: 206px; margin-top: 10px;">
				<spring:message code="application.login.button" />
			</button>
		</div>
		<label class="error" style="color: red;">${error}</label>
	</div>
</form>