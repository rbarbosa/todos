<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="g" tagdir="/WEB-INF/tags"%>
<%@ attribute name="size" description="Size of the image T,P,M,G" required="false" type="java.lang.String" %>
<%@ attribute name="cssClass" description="Css Class" required="false" type="java.lang.String" %>
<%@ attribute name="cssStyle" description="Css Style" required="false" type="java.lang.String" %>
<%@ attribute name="includeLink" description="Include Link. Default value true" required="false" type="java.lang.Boolean" %>
<%@ attribute name="user" description="user with the image info" required="false" type="br.com.gtac.edl.domain.register.User" %>
<c:if test="${empty user}">
	<sec:authentication property="principal.user" var="user" />
</c:if>
<c:if test="${empty includeLink or includeLink }">
<g:a href="/perfil/${user.username}" title="${user.username}" cssClass="friend">
		<g:img src="/user/view/${user.id}?size=${not empty size ? size : 'T' }"
		fallbackSrc="/resources/images/user/anonymous.jpg" cssClass="photo ${cssClass}" style="${cssStyle}"/>
	</g:a>
</c:if>
<c:if test="${not empty includeLink and not includeLink }">
		<g:img src="/user/view/${user.id}?size=${not empty size ? size : 'T' }"
		fallbackSrc="/resources/images/user/anonymous.jpg" cssClass="photo ${cssClass}" style="${cssStyle}"/>
</c:if>