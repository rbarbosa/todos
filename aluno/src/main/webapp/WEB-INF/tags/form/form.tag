<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="f" tagdir="/WEB-INF/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="gtac"%>

<%@ attribute name="acceptCharset" required="false" type="java.lang.String"%>
<%@ attribute name="action" required="false" type="java.lang.String"%>
<%@ attribute name="autocomplete" required="false" type="java.lang.String"%>
<%@ attribute name="commandName" required="false" type="java.lang.String"%>
<%@ attribute name="cssClass" required="false" type="java.lang.String"%>
<%@ attribute name="cssStyle" required="false" type="java.lang.String"%>
<%@ attribute name="dir" required="false" type="java.lang.String"%>
<%@ attribute name="enctype" required="false" type="java.lang.String"%>
<%@ attribute name="htmlEscape" required="false" type="java.lang.String"%>
<%@ attribute name="id" required="true" type="java.lang.String"%>
<%@ attribute name="lang" required="false" type="java.lang.String"%>
<%@ attribute name="method" required="false" type="java.lang.String"%>
<%@ attribute name="methodParam" required="false" type="java.lang.String"%>
<%@ attribute name="modelAttribute" required="false" type="java.lang.String"%>
<%@ attribute name="name" required="false" type="java.lang.String"%>
<%@ attribute name="onclick" required="false" type="java.lang.String"%>
<%@ attribute name="ondblclick" required="false" type="java.lang.String"%>
<%@ attribute name="onkeydown" required="false" type="java.lang.String"%>
<%@ attribute name="onkeypress" required="false" type="java.lang.String"%>
<%@ attribute name="onkeyup" required="false" type="java.lang.String"%>
<%@ attribute name="onmousedown" required="false" type="java.lang.String"%>
<%@ attribute name="onmousemove" required="false" type="java.lang.String"%>
<%@ attribute name="onmouseout" required="false" type="java.lang.String"%>
<%@ attribute name="onmouseover" required="false" type="java.lang.String"%>
<%@ attribute name="onmouseup" required="false" type="java.lang.String"%>
<%@ attribute name="onreset" required="false" type="java.lang.String"%>
<%@ attribute name="onsubmit" required="false" type="java.lang.String"%>
<%@ attribute name="target" required="false" type="java.lang.String"%>
<%@ attribute name="title" required="false" type="java.lang.String"%>

<script type="text/javascript">
    jQuery(document).ready(function($) {
    	var form = $("#${id}");
		form.validate();
		//se tiver mathquill, tratar antes de submit
		
		if($('#${id} .math-input').length>0){
			form.submit(function(){
				$('#${id} .math-input').each(function(index,item){
					var div=$(item);
					var value = getAlohaContents(div.find('.aloha-textarea'));
					div.find('input:hidden').val(value);
				});
			});
		}
    });
</script>

<form:form acceptCharset="${acceptCharset}" action="${action}"
	autocomplete="${autocomplete}" commandName="${commandName}"
	cssClass="${cssClass}" cssStyle="${cssStyle}" dir="${dir}" enctype="${enctype}"
	htmlEscape="${htmlEscape}" id="${id}" lang="${lang}" method="${method}"
	methodParam="${methodParam}" modelAttribute="${modelAttribute}" name="${name}"
	onclick="${onclick}" ondblclick="${ondblclick}" onkeydown="${onkeydown}"
	onkeypress="${onkeypress}" onkeyup="${onkeyup}" onmousedown="${onmousedown}"
	onmousemove="${onmousemove}" onmouseout="${onmouseout}"
	onmouseover="${onmouseover}" onmouseup="${onmouseup}" onreset="${onreset}"
	onsubmit="${onsubmit}" target="${target}" title="${title}">
	<c:if test="${not empty constraintError}">
		<f:errors path="" errorMessage="${constraintError}"/>
	</c:if>
	<jsp:doBody></jsp:doBody>
</form:form>