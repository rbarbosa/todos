<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="g" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="f" tagdir="/WEB-INF/tags/form"%>
<%@ attribute name="legendCode" type="java.lang.String" required="false"%>
<%@ attribute name="subindex" type="java.lang.Integer" required="false"%>
<%@ attribute name="btnLabelCode" type="java.lang.String" required="false"%>
<%@ attribute name="insertPosition" type="java.lang.String" description="posi��o para colocar a nova linha: before,after"%>
<%@ attribute name="afterAdd" type="java.lang.String" description="Fun��o javascript a ser chamada ap�s a adi��o do fieldset. Recebe o fieldset como argumento"%>
<%@ attribute name="items" description="" required="true"
	type="java.util.Collection"%>
<%@ attribute name="indexVar" description="name of the iteration variable" required="true"
	type="java.lang.String" rtexprvalue="false"%>
<c:set var="iterationName" value="${not empty iterationName ? iterationName : 'iterationIndex'}"></c:set>
<c:set value="${not empty subindex ? subindex : 0}" var="subindex"></c:set>
<%@ variable variable-class="java.lang.Integer" declare="true"
	description="The iteration's index" name-from-attribute="indexVar" alias="iterationIndex"%>

<style>
.innerEnclosure {
	float: left;
}
.clone {
display:none;
}
.addFunction {
display:none;
}
.addPosition {
display:none;
}

.outerEnclosure {
	clear: both;
	margin: 1px;
}
</style>
<div class="multipleField">
<g:script url="/resources/js/multipleRecordsList.js"></g:script>
		<c:if test="${not empty legendCode}">
			<label><spring:message code="${legendCode}" /></label>
		</c:if>
		<c:set value="${empty btnLabelCode ? 'multiple.add' : btnLabelCode}" var="btnLabel"></c:set>
		<div class="addFunction" >${afterAdd}</div>
		<div class="addPosition" >${insertPosition}</div>
		<div class="addSubindex" style="display:none;">${subindex}</div>
		<c:if test="${not empty insertPosition and insertPosition=='before'}">
		 <p class="description">
           <a href="#add" class="btn addNewLine"><spring:message code="${btnLabel}" /></a>
         </p>
		</c:if>
		<c:forEach var="item" varStatus="index" items="${items}">
		<fieldset>
			<c:set var="iterationIndex" value="${index.count - 1}" />
			<jsp:doBody />&nbsp;&nbsp;
			<a class="removeLine">Remover</a>
		</fieldset>
		</c:forEach>
		
		<c:set var="iterationIndex" value="${iterationIndex+1}" />
		<fieldset class="clone"><jsp:doBody />&nbsp;&nbsp;
			<a class="removeLine">Remover</a>
		</fieldset>
		<c:if test="${empty insertPosition or insertPosition=='after'}">
		 <p class="description">
           <a href="#add" class="btn addNewLine"><spring:message code="${btnLabel}" /></a>
         </p>
		</c:if>
		
		<script type="text/javascript" >
	$('.multipleField').multipleField();
</script>
</div>
			
