<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ tag body-content="empty"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="f" tagdir="/WEB-INF/tags/form"%>
<%@ taglib prefix="gtac" tagdir="/WEB-INF/tags"%>


<%@ attribute name="id" type="java.lang.String" required="true"
	description="The identifier for this tag"%>

<%@ attribute name="path" type="java.lang.String" required="true"
	rtexprvalue="true" description="The field exposed from the form backing object"%>
<%@ attribute name="value" type="br.com.gtac.edl.domain.register.Image" required="true"
	rtexprvalue="true" description="The field exposed from the form backing object"%>
<%@ attribute name="imageClass" type="java.lang.String" required="false"
	description="css class for the image"%>

<div>
	
			<gtac:script url="/resources/js/fileuploader/fileuploader.js"></gtac:script>
			<gtac:css url="/resources/js/fileuploader/fileuploader.css"></gtac:css>
			<gtac:script url="/resources/js/fileuploader/imageupload.js"></gtac:script>
		<c:set var="id" value="${fn:replace(id,'.','_') }"></c:set>
		<div id="${id}" class="file-uploader">
			<noscript>
				<p>Please enable JavaScript to use file uploader</p>
				<!-- or put a simple form for upload here -->
			</noscript>
			<script type="text/javascript">
				$('#${id}').imageupload({ messages: {
		            typeError: '<spring:message code="image.typeError"/>',
		            sizeError: '<spring:message code="image.sizeError"/>',
		            minSizeError: '<spring:message code="image.minSizeError"/>',
		            emptyError: '<spring:message code="image.emptyError"/>',
		            onLeave: '<spring:message code="image.sizeError"/>'            
		        },
		        template: '<div class="qq-uploader">' +
	            	'<center><div class="qq-upload-button description" style="display:none; color: #4BBDD3; font-weight: bold; text-shadow: 0 1px 0 white;" ><spring:message code="image.upload"/></div></center>' +
	            	'<ul class="qq-upload-list"></ul>' +
	        	'</div>'
				});
			</script>
			<input type="hidden" name="${path}" value="${value!=null ? value.id : ''}" class="id" />
			<c:set var="begin" value="${value!=null ? '/admin/multimedia/view/' : '/aluno/resources/img/anonymous.jpg'}"></c:set>
			<gtac:cimg size="P" imageId="${value.id}" cssClass="${imageClass}" fallbackImage="/resources/images/user/anonymous.jpg"></gtac:cimg>
		</div>
	</div>

