<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ tag body-content="empty"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ attribute name="id" description="Tag's Id" required="false"
	type="java.lang.String"%>
<%@ attribute name="path" description="Entity's path" required="true"
	type="java.lang.String"%>
<%@ attribute name="label" description="Entity's path" required="true"
	type="java.lang.String"%>
<%@ attribute name="cssClass" description="Field's css class" required="false"
	type="java.lang.String"%>
<%@ attribute name="cssStyle" description="Field's css style" required="false"
	type="java.lang.String"%>
<%@ attribute name="placeholder" description="Field's css style" required="false"
	type="java.lang.String"%>
<div class="control-group ${cssClass }" style="${cssStyle}">
  	<form:label path="${path }" cssClass="control-label">${label }</form:label>
    <div class="controls">
      <form:input path="${path }" id="${ id}" placeholder="${placeholder }"/>
      <form:errors path="${path }"/>
    </div>
</div>
