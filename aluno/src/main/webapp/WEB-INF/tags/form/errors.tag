<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ tag body-content="empty"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="f" tagdir="/WEB-INF/tags/form"%>
<%@ attribute name="id" description="Tag's Id" required="false"
	type="java.lang.String"%>
<%@ attribute name="path" description="Entity's path" required="true"
	type="java.lang.String"%>
<%@ attribute name="cssClass" description="Field's css class" required="false"
	type="java.lang.String"%>
<%@ attribute name="errorMessage" description="Message code to show as error" required="false"
	type="java.lang.String"%>

<!-- Spring's fields -->
<%@ attribute name="cssStyle" required="false" type="java.lang.String"%>


<c:set var="id" value="${id}_${fn:replace(path, '[', '') }_error" />
<c:set var="id" value="${fn:replace(id, ']', '') }" />
<c:set var="id" value="${fn:replace(id, '.', '_') }" />
<c:choose>
	<c:when test="${not empty errorMessage}">
		<span id="${id}" class="${cssClass!=null ? cssClass:''} alert-error " style="${cssStyle}" >
			<c:out value="${errorMessage}"></c:out>
		</span>
	</c:when>
	<c:otherwise>
		<form:errors id="${id}" cssClass="${cssClass!=null ? cssClass:''} alert-error " cssStyle="${cssStyle}" path="${path}"  />
	</c:otherwise>
</c:choose>