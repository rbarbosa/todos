<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<form action="<spring:url value="/s/conteudo"/>" id="search" method="post">
	<input type="text" placeholder="Pesquisar" id="search_query" class="search autocomplete" value="${searchQuery}" />
	<input type=submit value="Pesquisar" />
</form>

<script type="text/javascript">
<!--
if(document.location.href.indexOf('/s/') >= 0){
    var action = document.location.href;    
    var match = /\/s\/.*?(\/.*)/.exec(action)[1];
    var beforeQ = action.lastIndexOf('/');
    action = action.substring(0,beforeQ)+ action.substring(beforeQ).replace(match, '');
    
    jQuery('#search').attr('action', action);
}

(function($){
    var cache = {};
    function requestAutocomplete(request, response){
		var term = request.term;
		if ( term in cache ) {
			response(cache[term] );
			return;
		}
	
		$.ajax({
			url: '<spring:url value="/s/frequent-search" />',
			data: {
			    'query': request.term
			},
			success: function(data){
			    var elements = eval(data);
				var store = $.map(elements, function(element){
				    return {'label': element.query, 'value': element.query};
				});
				cache[term] = store;
				response(store);
			}
		});
    }
    
    $('form#search input.autocomplete').autocomplete({
	    'source': requestAutocomplete
	});
    
    function hasError(val){
    	var match = val.match(/[%\(\)\~\^\\\/\[\]\{\}\:\;\|]/);
    	if(match) return '<spring:message code="search.forbiden" text="search.forbiden" /> ' + match[0];
    	
    	if(/(^\*)|(^\?)|(^\.)|(^\#)/.test(val)) return '<spring:message code="search.wildcards" text="search.wildcards" />';
    	if(/\"/.test(val)) return '<spring:message code="search.quotes" text="search.quotes" />';
    	return '';
    }
    if(!document.isSearchBoxInitialized){
	    $(document).on('submit', '#search', function(evt){
			evt.preventDefault();
			evt.stopPropagation();
			var text = '<spring:message code="search.empty" text="search.empty" />';
			var val = $('#search_query').val();
			var errorMsg = hasError(val);
			if(val == ""){
			    new noty({'type':'alert', 'text': text});
			    return false;
			}else if(errorMsg != ""){
				new noty({'type':'error', 'text': errorMsg});
			    return false;
			}else{
			    var form = evt.target;
			    var action = $(form).attr('action');
			    action += '/' + val;
			    $(form).attr('action', action);
			    
			    document.location = action;
			}
	    });
	    
	    document.isSearchBoxInitialized = true;
    }
    
})(jQuery);
//-->
</script>

