<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ tag body-content="empty"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="f" tagdir="/WEB-INF/tags/form"%>

<%@ attribute name="id" description="Tag's Id" required="false"
	type="java.lang.String"%>
<%@ attribute name="path" description="Entity's path" required="true"
	type="java.lang.String"%>
<%@ attribute name="labelCode" description="Field's label code" required="false"
	type="java.lang.String"%>
<%@ attribute name="message" description="Error message to display"
	required="false" type="java.lang.String"%>
<%@ attribute name="alt" description="Alt text" required="false"
	type="java.lang.String"%>
<%@ attribute name="required" description="Is required" required="false"
	type="java.lang.Boolean"%>
<%@ attribute name="cssClass" description="Field's css class" required="false"
	type="java.lang.String"%>
<%@ attribute name="cssStyle" description="Field's css style" required="false"
	type="java.lang.String"%>
<%@ attribute name="placeholder" description="Placeholder pr�-preenchido" required="false"
	type="java.lang.String"%>

<!-- Spring's fields -->
<%@ attribute name="accesskey" required="false" type="java.lang.String"%>
<%@ attribute name="autocomplete" required="false" type="java.lang.String"%>
<%@ attribute name="cssErrorClass" required="false" type="java.lang.String"%>
<%@ attribute name="dir" required="false" type="java.lang.String"%>
<%@ attribute name="disabled" required="false" type="java.lang.String"%>
<%@ attribute name="htmlEscape" required="false" type="java.lang.Boolean"%>
<%@ attribute name="maxlength" required="false" type="java.lang.Integer"%>
<%@ attribute name="onblur" required="false" type="java.lang.String"%>
<%@ attribute name="onchange" required="false" type="java.lang.String"%>
<%@ attribute name="onclick" required="false" type="java.lang.String"%>
<%@ attribute name="ondblclick" required="false" type="java.lang.String"%>
<%@ attribute name="onfocus" required="false" type="java.lang.String"%>
<%@ attribute name="onkeydown" required="false" type="java.lang.String"%>
<%@ attribute name="onkeypress" required="false" type="java.lang.String"%>
<%@ attribute name="onkeyup" required="false" type="java.lang.String"%>
<%@ attribute name="onmousedown" required="false" type="java.lang.String"%>
<%@ attribute name="onmousemove" required="false" type="java.lang.String"%>
<%@ attribute name="onmouseout" required="false" type="java.lang.String"%>
<%@ attribute name="onmouseover" required="false" type="java.lang.String"%>
<%@ attribute name="onmouseup" required="false" type="java.lang.String"%>
<%@ attribute name="onselect" required="false" type="java.lang.String"%>
<%@ attribute name="readonly" required="false" type="java.lang.String"%>
<%@ attribute name="size" required="false" type="java.lang.String"%>
<%@ attribute name="tabindex" required="false" type="java.lang.Integer"%>
<%@ attribute name="title" required="false" type="java.lang.String"%>
<%@ attribute name="value" required="false" type="java.lang.String"%>

<c:set var="id" value="${id}_${fn:replace(path, '[', '') }" />
<c:set var="id" value="${fn:replace(id, ']', '') }" />
<c:set var="id" value="${fn:replace(id, '.', '_') }" />

<div class="input-prepend">

	<c:if test="${!empty labelCode }">
	
		<spring:message var="label" code="${labelCode}" />
		<form:label id="${id}_label" path="${path}" for="${id}" class="add-on" cssStyle="width:106px;" >${label}:</form:label>
		
	</c:if>
	
	<form:input id="${id}" path="${path}"
		cssClass="${cssClass}  ${not empty required and required ? 'required' : '' }"
		cssStyle="${cssStyle}" alt="${alt}" accesskey="${accesskey}"
		autocomplete="${autocomplete}" cssErrorClass="${cssErrorClass}" dir="${dir}"
		disabled="${disabled}" htmlEscape="${(empty htmlEscape) ? true : htmlEscape}" lang="${lang}"
		maxlength="${maxlength}" onblur="${onblur}" onchange="${onchange}"
		onclick="${onclick}" ondblclick="${ondblclick}" onfocus="${onfocus}"
		onkeydown="${onkeydown}" onkeypress="${onkeypress}" onkeyup="${onkeyup}"
		onmousedown="${onmousedown}" onmousemove="${onmousemove}"
		onmouseout="${onmouseout}" onmouseover="${onmouseover}"
		onmouseup="${onmouseup}" onselect="${onselect}" readonly="${readonly}"
		size="${size}" tabindex="${tabindex}" title="${title}" value="${value}" placeholder="${placeholder}"/>
		
	<f:errors id="${id}_error" path="${path}"></f:errors>

</div>

<script type="text/javascript">
		$(".cpf").mask("999.999.999-99").addClass("hasMask");
</script>