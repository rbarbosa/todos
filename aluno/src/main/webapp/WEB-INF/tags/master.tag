<!DOCTYPE html>
<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="gtac" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@attribute name="head" fragment="true"%>
<%@attribute name="title" required="false" description="Titulo para a página"%>
<%@attribute name="description" required="false" description="Description para meta-tag na página"%>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#"
	xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
<c:set var="titleTxt" value="The best TodoList in town"></c:set>
<c:set var="titleTxt" value="${not empty title  ? title : titleTxt}"></c:set>
<title>${titleTxt}</title>

<meta name="description" content="Yout todo list" />
<meta property="og:description" content="Your Personal TODO List"/>
<!-- Open Graph tags para facebook -->
<!-- ICO -->
<gtac:link href="/resources/img/fav-icon.ico"
	rel="shortcut icon" />
<gtac:css url="/resources/css/smoothness/jquery-ui-1.10.1.custom.min.css"></gtac:css>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!-- CSS -->
<gtac:css url="/resources/css/style.css"></gtac:css>
<gtac:css url="/resources/bootstrap/css/bootstrap.min.css" />
<gtac:css url="/resources/css/normalize.css" />

<jsp:invoke fragment="head" />

<!-- less -->
<script type="text/javascript">
	EDL = {}
	EDL.ROOT_PATH = '<spring:url value="/"/>';
		
	/*setando defaults de inputs*/
	$(function(){
		$('input, textarea').placeholder();
	});
	
</script>
 <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

<gtac:flashMessage />
</head>
<body>
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <gtac:a href="/" cssClass="brand">Todo List</gtac:a>
          <div class="nav-collapse collapse">
            <p class="navbar-text pull-right">
            <sec:authorize ifAnyGranted="ROLE_USER" var="loggedin" />
            <c:if test="${loggedin}">
            <sec:authentication property="principal" var="principal"/>
              Logged in as <a href="#" class="navbar-link">${principal.user.email }</a>
            </c:if>
            <c:if test="${not loggedin}">
            	Check out our login
            </c:if>
            </p>
            <ul class="nav">
              <li class="active"><a href="#">Home</a></li>
              
              <li><gtac:a href="/about" title="everything about TodoList">About</gtac:a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid content">
    	<div class="row-fluid">
		<jsp:doBody/>
    	</div>

      <hr>

      <footer>
        <p>&copy; TodoList 2013</p>
      </footer>
    </div>



	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
	<gtac:script url="/resources/javascripts/jquery.validate.js"></gtac:script>
	<gtac:script url="/resources/js/jquery.placeholder.min.js"></gtac:script>
	<gtac:script url="/resources/noty/jquery.noty-promise-themes.default-layouts.topCenter.js" />
	<gtac:script url="/resources/bootstrap/js/bootstrap.min.js" />
	<gtac:script url="/resources/js/modernizr.custom.63321.js" />
</body>
</html>