
<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ attribute name="showChat" type="java.lang.Boolean" description="Show bubble?"%>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
if (boolMovel === false){
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//cdn.zopim.com/?pgKb2TPsvVRYauSrODoYJnbrWYMDnYF7';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
}
</script>

<c:if test="${empty showChat or showChat == false}">
	<script type="text/javascript"> 
	$zopim(function() {
    	$zopim.livechat.bubble.hide();
	});
	</script>
</c:if>
<c:if test="${not empty showChat and showChat}">
<script type="text/javascript">
		$zopim(function() {
		    $zopim.livechat.bubble.setTitle("Dúvidas?");
		    $zopim.livechat.bubble.setText("Pergunte aqui!");
		    $zopim.livechat.bubble.show();
		  });
</script>
</c:if>
<!--End of Zopim Live Chat Script-->
<sec:authentication property="principal" var="principal" />
<sec:authorize access="hasRole('ROLE_USER')" >
	<%-- coloca o nome e email do usuário --%>

	<script type="text/javascript">
		$zopim(function() {
		    $zopim.livechat.setName("${fn:escapeXml(principal.user.info.fullName)}");
		    $zopim.livechat.setEmail("${fn:escapeXml(principal.user.email)}");
		  });
</script>
</sec:authorize>