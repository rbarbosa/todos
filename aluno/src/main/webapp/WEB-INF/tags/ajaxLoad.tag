<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ attribute name="url" description="URL para carregar" required="true" type="java.lang.String" %>
<%@ attribute name="id" description="Id da div" required="true" type="java.lang.String" %>
<%@ attribute name="cssClass" description="html class" type="java.lang.String" %>
<div id="${id}" class="ajaxLoad ${cssClass}" data-url="${url}">

</div>

