<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ attribute name="url" description="URL absoluta do arquivo a ser importado" required="true" type="java.lang.String" %>
<%@ attribute name="idContent" description="Id do Conte�do" required="true" type="java.lang.Long"%>

	<a class="btn share-link hide-share">Compartilhar</a>

        <div id="sharelink" style="display:none">
        
        	<input id="selectAllHref" type="text" value='${url}'>
	        <a class="btn share-email hide-share">Email</a>
	        
	        <a class="facebook" 
	           target="_blank" 
	           href="http://www.facebook.com/sharer.php?u=${url}">
	           <img src="/aluno/resources/images/facebook_icon.jpg"/>
	        </a>
	        
			<a href="https://twitter.com/share" 
			   class="twitter-share-button" 
			   data-url="${url}" 
			   data-text="Veja o" 
			   data-via="edu_livre" 
			   data-lang="pt" 
			   data-hashtags="educacaolivre" 
			   data-count="none">Tweetar</a>
			   
	        <div id="emailcontent" style="display: none">
		        <input id="email" type="text" placeholder="E-mail"><br>
		        <input id="comment" type="text" placeholder="Coment�rios (Opicional)"><br> 
		        <a id="sendMail" class="btn btn-primary" data-id="${idContent}" data-url="${url}">Enviar e-mail</a>
	        </div>
	        
	        <div class="otherMail" style="display:none">
	        	<a id="otherMail" class="btn btn-success"><i class="icon-ok icon-white"></i> Enviar outro</a>
	        </div>
				

			
        </div>
