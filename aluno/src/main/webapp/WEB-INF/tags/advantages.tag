<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="gtac" tagdir="/WEB-INF/tags"%>
<div>
	<div class="coluna1" style="width: 500px; float: left; margin-top: -55px;">
		<center>
			<h3 class="textw fonteEspecial" style="margin-top: -3px; font-size: 23px!important;">Por que comprar conosco?</h3>
		</center>
	</div>
	
	<div class="roundedAdvantage" style="float: left; background-color: #d9d9d9; width: 500px; height: 145px; margin-top: -22px;">
		<div class="advantages advantageFundo" style="margin-top: 3px; margin-left: 4px;">
			<ul class="growUl" style="margin-left: 40px;">
			
			
				<li class="growLi" id="numero1" data-msg="0"><a href="javascript:void(0)"><gtac:img
							src="resources/img/lock.png" cssClass="growImg" id="11" alt="Segurança na compra de sua passagem" /></a>
				</li>
				<li class="growLi" id="numero2" data-msg="1"><a href="javascript:void(0)"><gtac:img
							src="resources/img/message.png" cssClass="growImg" id="22" alt="Central de atendimento ao consumidor" /></a>
				</li>
				<li class="growLi" id="numero3" data-msg="2"><a href="javascript:void(0)"><gtac:img
							src="resources/img/notebook.png" cssClass="growImg" id="33" alt="Acompanhe seu pedido online" /></a>
				</li>
				<li class="growLi" id="numero4" data-msg="3"><a href="javascript:void(0)"><gtac:img
							src="resources/img/contact.png" cssClass="growImg" id="44" alt="Suporte também pelo telefone" /></a>
				</li>
				<li class="growLi" id="numero5" data-msg="4"><a href="javascript:void(0)"><gtac:img
							src="resources/img/clock.png" cssClass="growImg" id="55" alt="Informações precisas" /></a>
				</li>
				<li class="growLi" id="numero6" data-msg="5"><a href="javascript:void(0)"><gtac:img
							src="resources/img/favorite.png" cssClass="growImg" id="66" alt="Facilidade nas compras" /></a>
				</li>
			</ul>
		</div>
			<ul class="vantagens">
			
				<li class="primeiro">Segurança na compra de sua passagem</li>
				<li class="segundo">Central de atendimento ao consumidor</li>
				<li class="terceiro">Acompanhe seu pedido online</li>
				<li class="quarto">Suporte também pelo telefone</li>
				<li class="quinto">Informações precisas</li>
				<li class="sexto">Facilidade nas compras</li>
			
			</ul>
			<script type="text/javascript">
				$(".growUl li").mouseover(function(e){
					$(".vantagens li").hide();
					var index = $(e.target).parents('li').data("msg");
					$(".vantagens li:nth("+index+")").show();
				});
				
			</script>
	</div>
</div>	
<script type="text/javascript">
	$("li#1").mouseover(function(){
		$("div#11").css("display","block");
	})
</script>
