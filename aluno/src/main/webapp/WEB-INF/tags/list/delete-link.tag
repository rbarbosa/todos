<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ attribute name="href" description="delete ling" required="true"
	type="java.lang.String"%>
<%@ attribute name="confirmMessage" description="confirmation message"
	required="true" type="java.lang.String"%>

<core:set var="in_href" value="${href}" />
<core:set var="queryString"
	value="${requestScope['javax.servlet.forward.query_string']}" />

<core:if test="${fn:startsWith(href,'/') }">
	<core:url value="${href}" var="in_href" />
</core:if>
<core:if test="${not empty queryString}">
	<core:set var="separator" value="&" />
	<core:if test="${not fn:contains(in_href, '?') }">
		<core:set var="separator" value="?"/>
	</core:if>
	<core:set var="in_href" value="${in_href}${separator}${queryString}" />
</core:if>

<core:set var="script">
	javascript:if(confirm('${confirmMessage}'))window.location='${in_href}';	
</core:set>
<a href="${confirmMessage != null && confirmMessage != '' ? script : in_href}"
	class="${cssClass}" ${att_title }> <jsp:doBody />
</a>