<%@ tag language="java" pageEncoding="UTF-8" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="gtac" %>
<%@ taglib prefix="f" tagdir="/WEB-INF/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute name="id" description="ID da tabela sendo usada" required="true" type="java.lang.String" %>

<gtac:css url="http://twitter.github.com/bootstrap/assets/css/bootstrap.css"></gtac:css>
<gtac:master>
<div >
<c:if test="${not empty constraintError}">
		<f:errors path="" errorMessage="${constraintError}"/>
</c:if>
		<jsp:doBody/><br>
</div>
</gtac:master>
