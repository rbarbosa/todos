<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ attribute name="href" description="URL absoluta da âncora" required="true" type="java.lang.String" %>
<%@ attribute name="confirmMessage" description="Mensagem de confirmação para o usuário" required="false" type="java.lang.String" %>
<%@ attribute name="cssClass" description="Mensagem de confirmação para o usuário" required="false" type="java.lang.String" %>
<%@ attribute name="onMouseOver" description="onMouseOver imagen" required="false" type="java.lang.String" %>
<%@ attribute name="onMouseOut" description="onMouseOut imagen" required="false" type="java.lang.String" %>
<%@ attribute name="title" description="atributte HTML" required="false" type="java.lang.String" %>
<%@ attribute name="id" description="atributte HTML" required="false" type="java.lang.String" %>
<%@ attribute name="cssStyle" description="inserir style manual" required="false" type="java.lang.String" %>
<%@ attribute name="target" description="inserir target no link" required="false" type="java.lang.String" %>
<%@ attribute name="datatoggle" description="data-toggle" required="false" type="java.lang.String" %>
<%@ attribute name="role" description="role" required="false" type="java.lang.String" %>

<core:set var="att_title" value=""></core:set>
<core:if test="${not empty title }">
	<core:set value="title='${title}'" var="att_title"></core:set>
</core:if>
<core:if test="${not empty id }">
	<core:set value="id='${id}'" var="att_id"></core:set>
</core:if>
<core:set var="in_href" value="${href}"/>
<core:if test="${fn:startsWith(href,'/') }">
	<core:url value="${href}" var="in_href"/>
</core:if>

<core:set var="script" >
	javascript:if(confirm('${confirmMessage}'))window.location='${in_href}';	
</core:set>
<a href="${confirmMessage != null && confirmMessage != '' ? script : in_href}" class="${cssClass}" ${att_title } ${att_id } style="${cssStyle}" target="${target}" data-toggle="${datatoggle}" role="${role}">
	<jsp:doBody />
</a>