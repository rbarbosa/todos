<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="g" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="bus" tagdir="/WEB-INF/tags/bus"%>
<%@ taglib prefix="helper" uri="/WEB-INF/tags/helper.tld"%>
<%@ taglib prefix="f" tagdir="/WEB-INF/tags/form"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<spring:url value="/pesquisa/salvar" var="action"/>
<g:script url="/resources/js/rating/jquery.rating.pack.js"/>
<g:css url="/resources/js/rating/jquery.rating.css"/>
<form:form  method="POST" commandName="reviewForm" action="${action}">
	<div class="review-header">
		<p>Você pode nos responder algumas perguntas? É rapido... só <b>1 minutinho</b>.</p>
	</div>
	<c:forEach items="${reviewForm.questions}" varStatus="status" var="question">
		<div class="review-question">
			${question.content}
		</div>
		<form:hidden path="answers[${status.index}].question.id"/>
		<form:hidden path="operationId"/>
		<div class="review-stars">
		<c:if test="${question.allowStars}">
			<form:label path="answers[${status.index}].stars"></form:label>
			<c:forEach items="${starOptions}" var="option">
				<form:radiobutton path="answers[${status.index}].stars" value="${option.value }" 
					label="" cssClass="star" title="${option.label}"></form:radiobutton>
			</c:forEach>
		</c:if>
		</div>
		<br>
		<c:if test="${question.allowComment}">
			<f:textareaField path="answers[${status.index}].comment" cssStyle="width:300px;height:60px;"
				placeholder="Deixe aqui sua opinião sobre o assunto..." labelCode="reviewform.comment"/>
		</c:if>
	</c:forEach>
</form:form>
