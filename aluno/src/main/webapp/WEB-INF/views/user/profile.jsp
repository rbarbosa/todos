<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib tagdir="/WEB-INF/tags" prefix="gtac" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<gtac:master>
<jsp:attribute name="head">
	<gtac:link rel="stylesheet/less" type="text/css"
	href="/resources/stylesheets/profile.less" />
</jsp:attribute>

<jsp:body>
	    <!--content-->
    <div id="content">

      <div id="sidebar">
        <gtac:userImg size="P" cssClass="image-user"></gtac:userImg>

        <ul class="nav">
          <li><a href="#messages" class="messages" rel="tooltip"
						title="Mensagens">Mensagens</a></li>
          <li><a href="#ratings" class="ratings" rel="tooltip"
						title="Avalia��es">Avalia��es</a></li>
          <li><a href="<c:url value="/userinfo/edit"/>"
						class="settings" rel="tooltip" title="Configura��es">Configura��es</a></li>
          <li><a href='<c:url value="/fav/"/>' class="bookmarks" rel="tooltip"
						title="Favoritos">Favoritos</a></li>
        </ul>
      </div>

      <div id="middle">
        <ul id="suggestions">
          <li class="first">
            <gtac:a href="/social/invite">
              <span class="icon photo"></span>
              Convidar amigos 
            </gtac:a>
          </li>
          <li>
            <a href="javascript: void(0)">
              <span class="icon routes"></span>
              Perseguir objetivo
            </a>
          </li>
          <li>
            <a href="javascript: void(0)">
              <span class="icon profile"></span>
              Preencher metas
            </a>
          </li>
          <li>
            <a href="javascript: void(0)">
              <span class="icon content"></span>
              Visualizar conte�do
            </a>
          </li>
          <li class="last">
            <a href="javascript: void(0)" class="plus">
              <span class="icon plus"></span>
               Mais
            </a>
          </li>
        </ul>

        <div id="goals" class="maximized">
          <h3>Metas</h3>

          <div class="control">
            <a href="#minimize" class="minimize">Minimizar</a>
            <a href="#help" class="help">Ajuda</a>
          </div>

          <ul>
            <li class="first">
              <b>1.</b>
              <span>Nome da meta aqui, conte�do declarado</span>

              <div class="pull-right">
                <span class="people">225 pessoas</span>
                <a href="#close" class="close">Fechar</a>
                <input type="checkbox" />
              </div>
            </li>
            <li>
              <b>2.</b>
              <span>Nome da meta aqui, conte�do declarado</span>

              <div class="pull-right">
                <span class="people">225 pessoas</span>
                <a href="#close" class="close">Fechar</a>
                <input type="checkbox" />
              </div>
            </li>
            <li>
              <b>3.</b>
              <span>Nome da meta aqui, conte�do declarado</span>

              <div class="pull-right">
                <span class="people">225 pessoas</span>
                <a href="#close" class="close">Fechar</a>
                <input type="checkbox" />
              </div>
            </li>
            <li class="add">
              <b>4.</b>
              <input type="text" placeholder="Adicionar nova meta" />
              <button type="submit">Adicionar</button>

              <br class="clearfix" />
            </li>
          </ul>
        </div>

        <div id="tabs" class="timeline">
          <ul class="nav-tabs">
            <li class="active">
              <a href="#community-timeline"><i
								class="icon community"></i> Novidade</a>
            </li>
            <li>
              <a href="#timeline"><i class="icon timeline"></i> Minhas a��es</a>
            </li>
            <li>
              <a href="#historicTimeLine"><i class="icon timeline"></i> �ltimos objetos</a>
            </li>
          </ul>

          <div class="tab-content">
            <div id="community-timeline" class="active tab-pane">
                <gtac:userTimeline id="communityTimeline" forUser="true"
								fetchFollowing="true" view="userSocialTimeline" />
               
            </div>

            <div id="timeline" class="tab-pane">
            	<gtac:userTimeline id="userTimeline" forUser="true"
								fetchFollowing="false" view="userTimeline" />
            </div>
            

            <div id="historicTimeLine" class="tab-pane">
            	<gtac:userHistoric id="historicTimeLine"/>          
            </div>
          </div>
        </div>

        <div id="friends" class="box">
          <div class="header">
            <input type="text" placeholder="nome do amigo" />
          </div>

          
          <gtac:friendsLink ></gtac:friendsLink>
<!--             <a href="#friend" class="friend" rel="tooltip" -->
<!-- 							title="�der Costa"><img -->
<!-- 							src="http://lorempixel.com/50/60/people" /></a> -->
<!--             <a href="#friend" class="friend" rel="tooltip" -->
<!-- 							title="�der Costa"><img -->
<!-- 							src="http://lorempixel.com/50/60/people" /></a> -->
<!--             <a href="#friend" class="friend" rel="tooltip" -->
<!-- 							title="�der Costa"><img -->
<!-- 							src="http://lorempixel.com/50/60/people" /></a> -->
<!--             <a href="#friend" class="friend" rel="tooltip" -->
<!-- 							title="�der Costa"><img -->
<!-- 							src="http://lorempixel.com/50/60/people" /></a> -->
          

          <a href="/aluno/social/seguidores" class="more">Ver todos os amigos</a>
        </div>

        <div id="community" class="box">
          <div class="header">
            <input type="text" placeholder="nome do comunidade" />
          </div>

          <div class="items">
            <a href="#friend" class="friend" rel="tooltip"
							title="�der Costa"><img
							src="http://lorempixel.com/50/60/people" /></a>
            <a href="#friend" class="friend" rel="tooltip"
							title="�der Costa"><img
							src="http://lorempixel.com/50/60/people" /></a>
            <a href="#friend" class="friend" rel="tooltip"
							title="�der Costa"><img
							src="http://lorempixel.com/50/60/people" /></a>
            <a href="#friend" class="friend" rel="tooltip"
							title="�der Costa"><img
							src="http://lorempixel.com/50/60/people" /></a>
          </div>

          <a href="#more" class="more">Ver todas as comunidades</a>
        </div>

      </div>

      <br class="clearfix" />
    </div>
    <!--/content-->
    
    <script type="text/javascript">
    // Tabs Timeline
					$(document)
							.ready(
									function() {
										$('ul a', '#tabs, .tabs').click(
												function(event) {
            event.preventDefault();
            $(this).tab('show');
          });

    });
    </script>
    
</jsp:body>
</gtac:master>