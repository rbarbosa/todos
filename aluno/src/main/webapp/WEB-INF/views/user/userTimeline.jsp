<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="g" %>

<script type="text/javascript">
    timelineUpdate = ${timelineUpdate};
</script>
<c:forEach items="${messageList}" var="item">
	<c:if test="${not empty item.message }">
		<li>		
			<span class="state"><g:img src="${item.image}" fallbackSrc="/resources/images/ico/state-example.png" /></span>
			<div class="content">
		 	<a href="#user">${item.user}</a> ${item.message}
		 	<span class="time">${item.prettyDate}</span>
			</div>
		</li>
	</c:if>
</c:forEach>