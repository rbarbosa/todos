<!DOCTYPE h1 PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="gtac" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<gtac:userbase titleCode="user.change.title">
<form onsubmit="parent.location='/aluno';return false;">
	<input type="submit" value="<spring:message code="Button.ok" />"/>
</form>
</gtac:userbase>