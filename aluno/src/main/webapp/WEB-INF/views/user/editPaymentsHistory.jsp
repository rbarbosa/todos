<%@ taglib prefix="gtac" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="form" tagdir="/WEB-INF/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="springForm"
	uri="http://www.springframework.org/tags/form"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>	


<div id="secondary" class="tab-pane">

        <display:table class="table table-striped table-bordered" id="list" style="margin-bottom:0px!important;"
		uid="row" name="listPaymentsHistory" pagesize="${pageSize}" size="resultSize"  partialList="true" requestURI="perfil#secondary"  >
		
		<display:column property="id" titleKey="payment.id"  />
		<display:column titleKey="payment.timestamp" >
				<fmt:formatDate value="${row.timestamp}" type="Both"/>
		</display:column>
		<display:column  titleKey="payment.status.reserve" >
			<spring:message code="${row.status.labelCode}" text="??${row.status.labelCode }" ></spring:message> 
		</display:column>
		<display:column titleKey="payment.detail.title" >
			<gtac:a href="/pagamento/sec/r/${row.id}" cssStyle="color:#08C!important;">
				<spring:message code="payment.detail" text="payment.detail"></spring:message>
			</gtac:a>
		</display:column>
	</display:table>
</div>
