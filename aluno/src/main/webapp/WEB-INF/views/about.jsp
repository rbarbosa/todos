<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="g" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="gf" tagdir="/WEB-INF/tags/form"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page pageEncoding="UTF-8"%>
<g:master
	description="The Best Todo List in town" >
	<jsp:attribute name="head">
</jsp:attribute>

	<jsp:body>
  <!--content-->
  <div class="span12">
  	<h1 class="text-center">The Best Todo List in Town</h1>
  </div>
  	<div class="span8 offset2">
  		<h3 class="text-center subtitle">TodoList App</h3>
  		<p>New TodoList app, totally in ajax. No refreshes while editing your <b>TodoList</b>.
  		No page refresh at any time.</p>
  		<p><b>TodoList</b> is made with <a href="http://angularjs.org" alt="angularjs page">angularjs</a> as the main
  		front-end framework. We also use <a href="http://jquery.com">jquery</a>, <a href="http://angular-ui.github.com/">angular-ui</a>
  		and <a href="http://twitter.github.com/bootstrap/">bootstrap from twitter</a> as the main css library.
  		</p>
  		<p>The server is a Java application architechtured with <a href="http://www.springsource.org/spring-framework">Spring Framework</a>.
  		Spring used for security, simple implementation of MVC pattern, DAO implementations and of course, IoC.</p>
  		
  		<h3 class="subtitle text-center">REST API</h3>
  		<p>We have an REST API, so you can integrate your <b>TodoList</b> anywhere. 
  		Like the command line? Make a script using curl to update your <b>TodoList</b></p>
  		<h3>API Docs</h3>
  		<p>All requests use http Basic authentication, using the http header with username:password encoded</p>
  		<dl>
  		<dt>List all TodoLists from user</dt>
  		<dd>GET /todos/rest/list<br>
  		Returns json of all TodoLists by order of creation
  		</dd>
  		<dt>Create new TodoList</dt>
  		<dd>POST /todos/rest/list<br>
  		Returns id of TodoList
  		</dd>
  		<dt>Access TodoList by id</dt>
  		<dd>GET /todos/rest/list/{id}<br>
  		Returns json of TodoList
  		</dd>
  		<dt>Create new TodoEntry (Task)</dt>
  		<dd>POST /todos/rest/entry<br>
  		the POST content should be a json of the TodoEntry to save.<br>
  		Returns id of TodoEntry
  		</dd>
  		<dt>Update TodoEntry (Task)</dt>
  		<dd>POST /todos/rest/entry/{id - id of TodoEntry}<br>
  		the POST content should be a json of the TodoEntry to save.<br>
  		Returns id of TodoEntry
  		</dd>
  		<dt>Remove TodoEntry (Task)</dt>
  		<dd>POST /todos/rest/entry/remove/{id - id of TodoEntry}<br>
  		Returns ok if succeded
  		</dd>
  		<dt>Access TodoEntry by id</dt>
  		<dd>GET /todos/rest/entry/{id}<br>
  		Returns json of TodoEntry
  		</dd>
  		</dl>
  	</div>
    <!--/content-->
</jsp:body>
</g:master>