<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="gtac" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="gtacForm" tagdir="/WEB-INF/tags/form"%>
<%@ page session="false"%>
<c:set var="meta" value="${company.metaDescription}"/>
<c:if test="${empty meta}">
	<c:set var="meta">Compre passagens da ${company.name} aqui.</c:set>	
</c:if>
<gtac:master description="${meta}">
<div class="well">
	<div style="background-color: white;">
		<c:if test="${not empty company.description}">
			<div>
				<br>
				<blockquote>
					<h1><strong>
						<span style="line-height: 1.6em; color:#08005c;">${company.name}</span></strong></h1>
				</blockquote>
			</div>
			<div>			
				${company.description}
			</div>
		</c:if>
		<c:if test="${empty company.description}">
			<br>
				<blockquote>
					<h1><strong>
						<span style="line-height: 1.6em; color:#08005c;">${company.name}</span></strong></h1>
				</blockquote>
			<br>	
		</c:if>
	</div>
</div>
	<script type="text/javascript">
		$(".newsletter").css("height", "30px!important");
	</script>
</gtac:master>
