<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="g" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="gf" tagdir="/WEB-INF/tags/form"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page pageEncoding="UTF-8"%>
<g:master
	description="The Best Todo List in town" >
	<jsp:attribute name="head">
</jsp:attribute>

	<jsp:body>
  <!--content-->
  <div class="span12">
  	<h1 class="text-center">The Best Todo List in Town</h1>
  </div>
  <div class="span2"></div>
  <sec:authorize ifAnyGranted="ROLE_USER"  var="loggedin"/>
  <c:if test="${not loggedin }">
  <div class="span4 well">
  	<h3 class="text-center subtitle">New here? Register now</h3>
  	<c:url var="registerurl" value="/register"></c:url>
    <form:form id="register-form" action="${registerurl }" method="POST" modelAttribute="registerUser"
    cssClass="form-horizontal ">
  <gf:inputField path="fullName" label="Name"/>
  <gf:inputField path="email" label="E-mail"/>
  <gf:inputField path="emailVerify" label="E-mail (confirmation)"/>
  <gf:passwordField label="Password" path="password"/>
  <gf:passwordField label="Password again" path="confirmPass"/>
  <div class="control-group">
    <div class="controls">
      <button type="submit" class="btn btn-primary">Sign in</button>
    </div>
   </div>
  	</form:form>
  </div>
  <div class="span4 well">
  	<h3 class="text-center subtitle">Already a user? Login below</h3>
  	<c:url var="loginurl" value="/j_spring_security_check"></c:url>
    <form:form id="login-form" action="${loginurl }" method="POST" modelAttribute="loginUser"
    cssClass="form-horizontal">
    	<c:if test="${not empty param.loginerror and param.loginerror}">
    		<span class="label label-important text-right">Invalid email or password</span>
    	</c:if>
  		<gf:inputField path="j_username" label="E-mail"/>
  		<gf:passwordField label="Password" path="j_password"/>
  	<div class="control-group">
    <div class="controls">
    	<label class="checkbox">
        	<form:checkbox path="rememberme"/> Keep Logged in
      	</label>
      <button type="submit" class="btn btn-primary">Sign in</button>
    </div>
   </div>
  	</form:form>	
  </div>
  </c:if>
  <c:if test="${loggedin }">
  	<div class="span8 text-center">
  		<h3 class=" subtitle">Get in and get stuff done</h3>
  		<g:a href="/todos" cssClass="btn btn-primary btn-large text-center">Hurry!!</g:a>
  	</div>
  </c:if>
  <div class="span2"></div>
  
    
    <!--/content-->
</jsp:body>
</g:master>