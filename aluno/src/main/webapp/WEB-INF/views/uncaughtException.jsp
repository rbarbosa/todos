<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="gtac"%>

<gtac:master>

	<div>
		<spring:message var="title" code="error_uncaughtexception_title" />
		<h2>${title}</h2>
		<p>
			<spring:message code="error_uncaughtexception_problemdescription" />
		</p>
		<sec:authorize ifAllGranted="ROLE_ADMIN">
		<c:if test="${not empty exception}">
			<p>
			<h4>
				<spring:message code="exception_details" />
			</h4>
			<div>
				Mensagem: <c:out value="${exception.localizedMessage}" />
			</div>
			Stacktrace:
			<div style="color: blue;">
				<c:forEach items="${exception.stackTrace}" var="trace">
					<c:out value="${trace}" />
					<br />
				</c:forEach>
			</div>
		</c:if>
		</sec:authorize>
	</div>

</gtac:master>