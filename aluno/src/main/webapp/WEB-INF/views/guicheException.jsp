<!DOCTYPE h1 PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="gtac" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="gtacForm" tagdir="/WEB-INF/tags/form"%>
<%@ page session="false"%>
<c:set var="content">
	<div  class="content" style="height:356px;">
		<div class="alert alert-error">
			<strong><h3>Ops</h3></strong>
			<c:if test="${empty exception.args }">
				<spring:message code="${exception.messageCode}"></spring:message><br><br><br>
			</c:if>
			<c:if test="${not empty exception.args }">
				<spring:message code="${exception.messageCode}" arguments="${exception.args}"></spring:message><br><br><br>
			</c:if>
		</div>
		<gtac:img src="/resources/img/error.png" cssClass="placa"></gtac:img>
	</div>
</c:set>
<c:if test="${exception.isAjax != true}">
	<gtac:master>
		<div class="well">
			${content}
		</div>
	</gtac:master>
	</c:if>
<c:if test="${exception.isAjax == true}">
	<script type="text/javascript">
		noty({text:'Desculpe. Ocorreu um erro de processamento.',type:'error'});
	</script>
	${content}			
</c:if>
