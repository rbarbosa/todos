<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="g" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="form" tagdir="/WEB-INF/tags/form"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page pageEncoding="UTF-8"%>
<g:master description="Quem somos?" showChat="true">
	<jsp:body>
	
	<script type="text/javascript">
	$(document).ready(function(){
		
		var loading = function(target,add){
			if(add){
				$(target).attr('disabled', true);
				$(target).text("Enviando ...");
			}else{
				$(target).attr('disabled', false);
				$(target).text("Enviar");
			}
		};
		$('#contact').live('click',function(e){
			
			var name = $('input.name').val();
			var email = $('input.e-mail').val();
			var message = $('textarea.message').val();
			var subject = $('input.subject').val();
			
			loading(e.target,true);
			$.ajax({
				type: 'POST',
				url : "contact",
				data : {
					name : name,
					email : email,
					message : message,
					subject : subject
				}
			}).complete(function(){
				loading(e.target,false);
			}).done(function(data){
				if(data === "ok"){
					noty({text: 'Contato realizado com sucesso, aguarde entraremos em contato com você por e-mail.', type: 'success'});
					$('.contactUs').val('');
				}else if(data === "name"){
					noty({text: 'Nome inválido! O nome deve ter entre 3 a 50 caracteres.', type: 'error'});
				}else if(data === "email"){
					noty({text: 'Digite um e-mail válido.', type: 'error'});
				}else if(data === "subject"){
					noty({text: 'O assunto deve ter entre 3 a 50 caracteres.', type: 'error'});
				}else if(data === "message"){
					noty({text: 'A mensagem deve ter entre 10 a 1000 caracteres.', type: 'error'});
				}
			});
			
		});
	});
	</script>
  	<!--content-->
  
	<div id="faq" class="well" style="height: 530px; background: url('resources/img/arroba.png') right bottom whiteSmoke no-repeat;">
	  <h1 class="title" style="margin-top: 0px;">Contato</h1>
	  <p>Estamos sempre disponíveis para um melhor atendimento. Abaixo segue os meios de contato:</p>
	  
		<div style="width: 550px;">
			<div class="coluna1">
		  		<center>
							<h3 class="textw fonteEspecial"
								style="font-size: 23px !important; position: relative; top: -4px;">Mande uma mensagem para nós</h3>
						</center>
			</div>
		  <div class="coluna4" style="background: #D9D9D9 !important; height: 370px!important;">
		  		<input type="text" placeholder="Nome" class="contactUs name" style="width: 517px;"><br>
		  		<input type="text" placeholder="E-mail" class="contactUs e-mail" style="width: 517px;"><br>
		  		<input type="text" placeholder="Assunto" class="contactUs subject" style="width: 517px;"><br>
		  		<textarea rows="10" placeholder="Mensagem" class="contactUs message" style="resize:none ; width: 517px;"></textarea>
						<br>
		  		<button id="contact" class="btn btn-info"
							style="margin-top: 10px; width: 150px; margin-left: 380px;" type="submit" formaction='<spring:url value="/who"/>'>Enviar</button>
		  </div>
		</div>
	  	<div style="margin-left: 550px; margin-top: -430px;">
			<ul style="list-style: none">
				<li><b>Se preferir, contate-nos também pelo:</b></li><br>
			  	<li>Email: <a href="mailto:contato@guichevirtual.com.br">contato@guichevirtual.com.br</a></li>
		  		<li>Telefone: <a href="tel:1232061332">(12) 3206-1332</a></li>
		  		<li style="font-size: 13px;">Horário de atendidmento: <strong>09:00 às 18:00</strong><br></li>
		  	</ul>
		 </div>
	</div>
	<!--/content-->
</jsp:body>
</g:master>