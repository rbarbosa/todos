<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="g" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="form" tagdir="/WEB-INF/tags/form"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page pageEncoding="UTF-8"%>
<g:master  description="Quem somos?" showChat="true">
	<jsp:body>
  <!--content-->
  <div id="faq">
  <h1 class="title">Termos de serviço</h1>
  <h3>Regulamento da viagem de Ônibus</h3>
            <h3>São direitos e obrigações do usuário:</h3>
            <ul class="well terms-list" >
            	<li>receber serviço adequado;</li>
                <li>receber da ANTT e da transportadora informações para defesa de interesses individuais ou coletivos;</li>
                <li>obter e utilizar o serviço com liberdade de escolha;</li>
                <li>levar ao conhecimento do órgão de fiscalização as irregularidades de que tenha conhecimento, referentes ao serviço delegado;</li>               
                <li>zelar pela conservação dos bens e equipamentos por meio dos quais lhes são prestados os serviços;</li>
                <li>ser transportado com pontualidade, segurança, higiene e conforto, do início ao término da viagem;</li>
                <li>ter garantida sua poltrona no ônibus, nas condições especificadas no bilhete de passagem;</li>
                <li>ser atendido com urbanidade pelos prepostos da transportadora e pelos agentes de fiscalização;</li>
                <li>ser auxiliado no embarque e desembarque, em se tratando de crianças, pessoas idosas ou com dificuldades de locomoção;</li>
                <li>receber da transportadora informações acerca das características dos serviços, tais como horários, tempo de viagem, localidades atendidas, preço de passagem e outras relacionadas com os serviços;</li>
                <li>transportar, gratuitamente, bagagem no bagageiro observada os limites de peso total de trinta quilogramas, de volume máximo de trezentos decímetros cúbicos e de maior dimensão de um metro, bem como volume no porta-embrulhos limitado a cinco quilogramas e dimensões compatíveis;</li>
                <li>receber os comprovantes dos volumes transportados no bagageiro;</li>
                <li>ser indenizado por extravio ou dano da bagagem transportada no bagageiro; </li>
                <li>receber a diferença do preço da passagem, quando a viagem se faça, total ou parcialmente, em ônibus de características inferiores às daquele contratado;</li>
                <li>receber, às expensas da transportadora, enquanto perdurar a situação, alimentação e pousada, nos casos de venda de mais de um bilhete de passagem para a mesma poltrona, ou interrupção ou retardamento da viagem, quando tais fatos forem imputados à transportadora;
XVI - receber da transportadora, em caso de acidente, imediata e adequada assistência;</li>
                <li>transportar, sem pagamento, crianças de até seis anos incompletos, desde que não ocupem poltronas, observadas as disposições legais e regulamentares aplicáveis ao transporte de menores;</li>
                <li>efetuar a compra de passagem com data de utilização em aberto, sujeita a reajuste de preço se não utilizada dentro de um ano da data da emissão;</li>
                <li>receber a importância paga, no caso de desistência da viagem, hipótese em que o transportador terá o direito de reter até cinco por cento da importância a ser restituída ao passageiro, ou revalidar o bilhete de passagem para outro dia ou horário, desde que, em ambos os casos, se manifeste com antecedência mínima de três horas em relação ao horário de partida;</li>
                <li>estar garantido pelo Seguro de Responsabilidade Civil contratado pela transportadora, sem prejuízo da cobertura do seguro obrigatório de danos pessoais (DPVAT).</li>            
            	<a href="#topo" rel="" id="topo" class="btn btn-mini btn-faq"><i class="icon-chevron-up"></i> Voltar ao topo</a><br>
            </ul>
            <h3>O usuário terá recusado o embarque ou determinado seu desembarque, quando:</h3>
            <ul class="well terms-list">
            	<li>não se identificar quando exigido;</li>
                <li>em estado de embriaguez;</li>
                <li>portar arma, sem autorização da autoridade competente;</li>
                <li>transportar ou pretender embarcar produtos considerados perigosos pela legislação específica;</li>
                <li>transportar ou pretender embarcar consigo animais domésticos ou silvestres, sem o devido acondicionamento ou em desacordo com disposições legais ou regulamentares;</li>
                <li>pretender embarcar objeto de dimensões e acondicionamento incompatíveis com o porta-embrulhos;</li>
                <li>comprometer a segurança, o conforto ou a tranqüilidade dos demais passageiros;</li>
                <li>fizer uso de aparelho sonoro, depois de advertido pela tripulação do ônibus; </li>
                <li>demonstrar incontinência no comportamento;</li>
                <li>recusar-se ao pagamento da tarifa;</li>
                <li>fizer uso de produtos fumígenos no interior do ônibus, em desacordo com a legislação pertinente.</li>
                <a href="#topo" rel="" id="topo" class="btn btn-mini btn-faq"><i class="icon-chevron-up"></i> Voltar ao topo</a><br>          
            </ul>
	</div>
  	
    <!--/content-->
</jsp:body>
</g:master>