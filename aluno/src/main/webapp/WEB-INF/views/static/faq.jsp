<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="g" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="form" tagdir="/WEB-INF/tags/form"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page pageEncoding="UTF-8"%>
<g:master  description="Dúvidas sobre os serviços prestados pelo Guichê Virtual? Selecionamos as dúvidas mais frequentes que você possa ter e respodemos nesta página. Confira!" showChat="true">
	<jsp:body>
	
	
  <!--content-->
  <div id="faq">
  <h1 class="title">Dúvidas</h1>
  <p>Reunimos aqui as dúvidas mais frequentes quanto a nossos serviços.</p>
  <ul>
  	<li class="recuoLi"><a href="#cadastro">Cadastro</a></li>
  	<li class="recuoLi"><a href="#comprar">Comprar no Guichê Virtual</a></li>
  	<li class="recuoLi"><a href="#alteracoes">Alterações e Cancelamentos</a></li>
  	<li class="recuoLi"><a href="#embarque">Embarque</a></li>
  	<li class="recuoLi"><a href="#bagagens">Bagagens</a></li>
  	<li class="recuoLi"><a href="#criancas">Crianças</a></li>
  	<li class="recuoLi"><a href="#animais">Animais</a></li>
  	<li class="recuoLi"><a href="#usabilidade">Dúvidas de usabilidade</a></li>
  </ul>
  
  <div class="well">
	  <h3 id="cadastro" class="title tituloWell">Cadastro</h3>
	  <h4>Por que não recebi o e-mail de confirmação de cadastro?</h4>
	  <p>Verifique em sua caixa de correio se o e-mail se encontra na caixa de spam. Também é possível que o e-mail fornecido estivesse errado; neste caso, será necessário realizar um novo cadastro.</p>
	  <h4>Como faço para alterar meus dados cadastrais?</h4>
	  <p>Basta acessar a sua conta no site do Guichê Virtual e atualizar as suas informações na seção <g:a href="/perfil#primary" title="Meus Dados" >Meus Dados</g:a>.</p>
	  <a href="#topo" rel="" id="topo" class="btn btn-mini btn-faq"><i class="icon-chevron-up"></i> Voltar ao topo</a><br>
  </div>
  
  <div class="well">
	  <h3 id="comprar" class="title tituloWell">Comprar no Guichê Virtual</h3>
	  <h4>Por que não encontro passagens para o trecho que eu procuro?</h4>
	  <p>É possível que o trecho desejado não tenha passagens disponíveis para venda na internet. Além disso, ...</p>
	  <h4>Posso escolher o meu assento no ônibus?</h4>
	  <p>Sim. Normalmente as empresas disponibilizam quais os assentos estão à venda. No entanto, algumas empresas não permitem a marcação prévia dos assentos. Recomendamos a chegada à rodoviária com maior antecedência, para que confira quais as opções oferecidas pelas companhias quando for embarcar.
		 Informamos que as empresas de ônibus reservam-se o direito de bloquear a marcação antecipada de assentos, assim como cancelar ou alterar os assentos reservados sem aviso prévio.</p>
	  <h4>Quais são as formas de pagamento?</h4>
	  <p>O pagamento pode ser realizado através de cartão de crédito e (completem).</p>
	  <h4>Em quanto tempo receberei a confirmação da minha compra?</h4>
	  <p>Você receberá o seu e-ticket em um prazo de até 72 horas. Você também poderá encontrá-lo acessando a sua conta no site do Guichê Virtual.</p>
	  <h4>Como vejo o status da minha solicitação de compra?</h4>
	  <p>Você pode visualizar a situação de sua compra ao realizar seu login na página do Guichê Virtual, na área <g:a href="/perfil#secondary" title="Minhas Viagens" >Minhas Viagens</g:a>.</p>
	  <h4>Houve um erro no meu pagamento. O que devo fazer?</h4>
	  <p>Acesse a sua conta no Guichê Virtual, e verifique em <g:a href="/perfil#secondary" title="Minhas Viagens" >Minhas Viagens</g:a> a sua última reserva. Nesta tela você pode efetuar o pagamento.</p>
	  <h4>Não consigo imprimir a página de detalhes da passagem. O que faço?</h4>
	  <p>Não é necessária a impressão da página de pedidos. Você só precisa ir ao balcão da empresa e apresentar os documentos informados no ato da compra.</p>
	  <h4>As passagens serão entregues na minha casa?</h4>
	  <p>Não. Isso não é necessário, pois você pode imprimir sua passagem diretamente na rodoviária, no guichê da empresa de ônibus.</p>
	  <a href="#topo" rel="" id="topo" class="btn btn-mini btn-faq"><i class="icon-chevron-up"></i> Voltar ao topo</a><br>
	 </div>
  
  	<div class="well">
	  <h3 id="alteracoes" class="title tituloWell">Alterações e Cancelamentos</h3>
	  <h4>Posso solicitar a alteração da minha passagem?</h4>
	  <p>A transferência de horário ficará condicionada à disponibilidade de passagens na data e horário desejados pelo usuário, ficando ainda assegurado ao passageiro a opção pela passagem com data e horário "em aberto", assim permanecendo pelo prazo, máximo, de 12 (doze) meses, contados da data do bilhete original, ficando sujeito a reajuste de preço se não utilizada dentro desse prazo.</p>
	  <h4>Como cancelar minha passagem?</h4>
	  <p>Você pode cancelar sua passagem através de um e-mail para contato@guichevirtual.com.br em horário comercial. O pedido deve ser feito com até 12 (doze) horas de antecedência, para uma garantia de cancelamento.
	A passagem também pode ser cancelada no guichê da própria empresa com até 3 (três) horas de antecedência.</p>
	  <h4>Ao cancelar meu pedido, terei meu dinheiro de volta?</h4>
	  <p>Sim. O reembolso dependerá da politica de cancelamento correspondente a sua reserva. O mesmo será efetuado e estará disponível na próxima fatura do seu cartão de crédito ou na fatura subsequente, dependendo da data de fechamento da mesma.</p>
	  <a href="#topo" rel="" id="topo" class="btn btn-mini btn-faq"><i class="icon-chevron-up"></i> Voltar ao topo</a><br></div>
	 </div>
	 
	 <div class="well">	  
	  <h3 id="embarque" class="title tituloWell">Embarque</h3>
	  <h4>Quais os documentos necessários na hora de embarcar?</h4>
	  <p>Para viagens nacionais, leve um documento de identificação e a sua passagem. Para viagens internacionais, a maioria dos países vizinhos aceita a Carteira de Identidade como documento de identificação, desde que seja original.</p>
	  <h4>Com que antecedência devo chegar no terminal rodoviário para embarcar?</h4>
	  <p>Você deve estar no terminal rodoviário pelo menos 30 minutos antes do horário de partida do seu ônibus.</p>
	  <a href="#topo" rel="" id="topo" class="btn btn-mini btn-faq"><i class="icon-chevron-up"></i> Voltar ao topo</a><br>
	</div>
	
	<div class="well">
	  <h3 id="bagagens" class="title tituloWell">Bagagens</h3>
	  <h4>Existe limite de peso para viagens de ônibus?</h4>
	  <p>O preço da passagem abrange, a título de franquia, o transporte obrigatório e gratuito de volumes no bagageiro e no porta-embrulhos, observados os seguintes limites máximos de peso e dimensão:</p>
	  <li>No bagageiro, 30 kg de peso total e volume máximo de 300dm³ (0,3m³), limitada a maior dimensão do volume a um metro.</li>
	  <li>No porta-embrulhos, 5 kg de peso total com dimensões que se adaptem ao porta-embrulhos, desde que não sejam comprometidos o conforto e a segurança do cliente.</li>
	  <p>Excedida a franquia acima, haverá cobrança pelo excesso de bagagens. Consulte a empresa de ônibus com a qual irá viajar para certificar-se dos limites estabelecidos.
	É vedado o transporte de materiais considerados perigosos, como por exemplo explosivos, armas de fogo, produtos corrosivos, etc.</p>
	  <h4>Perdi minha bagagem. Com quem devo reclamar?</h4>
	  <p>Em caso de perda ou extravio de bagagem, procure a gerência da empresa de ônibus em que realizou a viagem ou, se a bagagem foi perdida dentro do terminal rodoviário, a seção de Achados e Perdidos da rodoviária. Informe-se na administração do terminal.
	Para seu conforto, é recomendável que a bagagem receba uma identificação externa e uma identificação interna contendo seus dados pessoais, pois, em caso de extravio ou esquecimento, será mais fácil devolvê-la.</p>
	<a href="#topo" rel="" id="topo" class="btn btn-mini btn-faq"><i class="icon-chevron-up"></i> Voltar ao topo</a><br>
	</div>
	
	<div class="well">
	  <h3 id="criancas" class="title tituloWell">Crianças</h3>
	  <h4>Tenho um filho menor de 18 anos. Ele pode viajar sozinho?</h4>
	  <p>Os pais que desejam viajar com crianças menores de 12 anos deverão apresentar a certidão de nascimento do filho no momento do embarque. O mesmo procedimento se dá para avós, tios (diretos) e irmãos maiores de 21 anos. O documento de identidade dos adultos deve ser apresentado.
	As crianças menores de 12 anos que não forem viajar acompanhadas pelos pais precisarão de autorização dos pais e do Juizado de Menores, normalmente localizado na própria rodoviária.
	Crianças maiores de 12 anos (adolescentes) podem viajar com pai, mãe, tios avós, irmãos maiores de 21 anos, sem autorização dentro do território nacional, mediante apresentação do documento original de identidade do adulto e certidão de nascimento da criança, para comprovar o grau de parentesco. Viajando na companhia de pessoa maior ou de parente por afinidade, a mãe ou o pai deverão fazer uma autorização por escrito, juntar cópia da identidade e cópia da certidão de nascimento da criança.
	Não deixe que as crianças fiquem sozinhas ou sem identificação. Em caso de desencontro, procure o posto do Juizado de Menores presente no terminal rodoviário ou a administração.</p>
	  <a href="#topo" rel="" id="topo" class="btn btn-mini btn-faq"><i class="icon-chevron-up"></i> Voltar ao topo</a><br>
	</div>
	  
	<div class="well">
	  <h3 id="animais" class="title tituloWell">Animais</h3>
	  <h4>Posso transportar animais na viagem de ônibus?</h4>
	  <p>Cada empresa possui uma política interna de transporte de animais. Sugerimos que entre em contato com a própria empresa transportadora para que ela explique quais são as regras de transporte de animais.</p>
	  <br>
	  <p>Deficientes físicos: nem sei se existe assento especial para eles, mas se existir, vamos precisar descobrir. Nas companhias aéreas o kra tem um botão lá que ele sinaliza ser deficiente físico. Não sei se somos obrigados a algo assim, acho que não, mas num custa dar uma checada.</p>
	  <a href="#topo" rel="" id="topo" class="btn btn-mini btn-faq"><i class="icon-chevron-up"></i> Voltar ao topo</a><br>
	 </div> 
	 
	 <div class="well">
	  <h3 id="usabilidade" class="title tituloWell">Dúvidas de usabilidade</h3>
	  <h4>Não recebi/perdi o e-mail de confirmação de compra, o que devo fazer?</h4>
	  <p>Você pode entrar na área de <g:a href="/perfil#secondary" title="Minhas Viagens" >Minhas Viagens</g:a> e acessar o pedido correspondente.
	  Lá você pode ter todas as informações da passagem.</p>
	  <a href="#topo" rel="" id="topo" class="btn btn-mini btn-faq"><i class="icon-chevron-up"></i> Voltar ao topo</a><br>
  	</div>
  </div>
    <!--/content-->
</jsp:body>
</g:master>