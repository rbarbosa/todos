<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="g" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="form" tagdir="/WEB-INF/tags/form"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page pageEncoding="UTF-8"%>
<g:master description="Companias de ônibus" showChat="true">
	<jsp:body>

  	<!--content-->
	 <div class="well">
		<div class="tabbable tabs-left">
		  <ul class="nav nav-tabs">
		    <li class="active"><a href="#1001" data-toggle="tab">1001</a></li>
		    <li class=""><a href="#andorinha" data-toggle="tab">Andorinha</a></li>
		    <li class=""><a href="#brisa" data-toggle="tab">Brisa</a></li>
		    <li class=""><a href="#catarinense" data-toggle="tab">Catarinense</a></li>
		    <li class=""><a href="#exSul" data-toggle="tab">Expresso Sul</a></li>
		    <li class=""><a href="#macaense" data-toggle="tab">Macaense</a></li>
		    <li class=""><a href="#exNacional" data-toggle="tab">Expresso Nacional</a></li>
		    <li class=""><a href="#rapidFederal" data-toggle="tab">Rápido Federal</a></li>
		    <li class=""><a href="#rapidRibeirao" data-toggle="tab">Rápido Ribeirão</a></li>
		    <li class=""><a href="#rotas" data-toggle="tab">Rotas</a></li>
		    <li class=""><a href="#unica" data-toggle="tab">Unica</a></li>
		    <li class=""><a href="#comet" data-toggle="tab">Cometa</a></li>
		    <li class=""><a href="#costaVerde" data-toggle="tab">Costa Verde</a></li>
		    <li class=""><a href="#estrela" data-toggle="tab">Viação Estrela</a></li>
		    <li class=""><a href="#araguari" data-toggle="tab">Expresso Araguari</a></li>
		    <li class=""><a href="#garcia" data-toggle="tab">Garcia</a></li>
		    <li class=""><a href="#guanabara" data-toggle="tab">Guanabara</a></li>
		    <li class=""><a href="#gardenia" data-toggle="tab">Gardênia</a></li>
		    <li class=""><a href="#ouroBraco" data-toggle="tab">Ouro Branco</a></li>
		    <li class=""><a href="#pluma" data-toggle="tab">Pluma</a></li>
		    <li class=""><a href="#princesa" data-toggle="tab">Princesa do Ivaí</a></li>
		    <li class=""><a href="#util" data-toggle="tab">Util</a></li>
		    <li class=""><a href="#brasilSul" data-toggle="tab">Brasil Sul</a></li>
		    <li class=""><a href="#expressoLuxo" data-toggle="tab">Expresso Luxo</a></li>
		    <li class=""><a href="#facil" data-toggle="tab">Expresso Facil</a></li>
		    <li class=""><a href="#normandy" data-toggle="tab">Normandy</a></li>
		    <li class=""><a href="#paraibuna" data-toggle="tab">Viação Paraibuna</a></li>
		    <li class=""><a href="#transcontinental" data-toggle="tab">Viação Transcontinental</a></li>
		  </ul>
		  <div class="tab-content">
		    <div class="tab-pane active" id="1001">
		      <p>1001</p>
		    </div>
		    <div class="tab-pane" id="util">
		      <p>Viação Util</p>
		    </div>
		    <div class="tab-pane" id="gardenia">
		      <p>Viação Gardênia</p>
		    </div>
		  </div>
		</div>
	</div>

	<!--/content-->
</jsp:body>
</g:master>