<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="g" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" tagdir="/WEB-INF/tags/form"%>
<%@ taglib prefix="form" tagdir="/WEB-INF/tags/form"%>
<%@ taglib prefix="bus" tagdir="/WEB-INF/tags/bus"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page pageEncoding="UTF-8"%>

<g:master
	description="Compra de passagens de ônibus online de forma fácil.">

	<jsp:attribute name="head">
<meta name="viewport"
			content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style"
			content="black-translucent">
<meta charset="UTF-8">

<!-- CSS -->
	<g:css url="/resources/stylesheets/jqtouch.css" />
	<style type="text/css">
.navbar {
	display: none;
}

#footer {
	display: none !important;
}

#jqt.fullscreen #home .info {
	display: none;
}

div#jqt #about {
	padding: 100px 10px 40px;
	text-shadow: rgba(0, 0, 0, 0.3) 0px -1px 0;
	color: #999;
	font-size: 13px;
	text-align: center;
	background: #161618;
}

div#jqt #about p {
	margin-bottom: 8px;
}

div#jqt #about a {
	color: #fff;
	font-weight: bold;
	text-decoration: none;
}
</style>
	
	<!-- JavaScript -->
	<g:script url="/resources/js/zepto.js" />
    <g:script url="/resources/js/jqtouch.js" />

     <script type="text/javascript" charset="utf-8">
						var jQT = new $.jQTouch({
							icon : 'jqtouch.png',
							icon4 : 'jqtouch4.png',
							addGlossToIcon : false,
							startupScreen : 'jqt_startup.png',
							statusBar : 'black-translucent',
							themeSelectionSelector : '#jqt #themes ul',
							preloadImages : []
						});

						// Some sample Javascript functions:
						$(function() {

							// Show a swipe event on swipe test
							$('#swipeme').swipe(
									function(evt, data) {
										var details = !data ? '' : '<strong>'
												+ data.direction + '/'
												+ data.deltaX + ':'
												+ data.deltaY + '</strong>!';
										$(this).html('You swiped ' + details);
										$(this).parent().after(
												'<li>swiped!</li>')
									});

							$('#tapme').tap(function() {
								$(this).parent().after('<li>tapped!</li>')
							});

							$('a[target="_blank"]')
									.bind(
											'click',
											function() {
												if (confirm('This link opens in a new window.')) {
													return true;
												} else {
													return false;
												}
											});

							// Page animation callback events
							$('#pageevents')
									.bind(
											'pageAnimationStart',
											function(e, info) {
												$(this)
														.find('.info')
														.append(
																'Started animating '
																		+ info.direction
																		+ '&hellip;  And the link '
																		+ 'had this custom data: '
																		+ $(
																				this)
																				.data(
																						'referrer')
																				.data(
																						'custom')
																		+ '<br>');
											})
									.bind(
											'pageAnimationEnd',
											function(e, info) {
												$(this)
														.find('.info')
														.append(
																'Finished animating '
																		+ info.direction
																		+ '.<br><br>');

											});

							// Page animations end with AJAX callback event, example 1 (load remote HTML only first time)
							$('#callback')
									.bind(
											'pageAnimationEnd',
											function(e, info) {
												// Make sure the data hasn't already been loaded (we'll set 'loaded' to true a couple lines further down)
												if (!$(this).data('loaded')) {
													// Append a placeholder in case the remote HTML takes its sweet time making it back
													// Then, overwrite the "Loading" placeholder text with the remote HTML
													$(this)
															.append(
																	$(
																			'<div>Loading</div>')
																			.load(
																					'ajax.html .info',
																					function() {
																						// Set the 'loaded' var to true so we know not to reload
																						// the HTML next time the #callback div animation ends
																						$(
																								this)
																								.parent()
																								.data(
																										'loaded',
																										true);
																					}));
												}
											});
							// Orientation callback event
							$('#jqt').bind(
									'turn',
									function(e, data) {
										$('#orient').html(
												'Orientation: '
														+ data.orientation);
									});

						});
					</script>

</jsp:attribute>
	<jsp:body>
  <!--content-->
	<div id="jqt">
            <div id="home" class="current">
                <div class="toolbar">
                	<div class="posicionaCentro">
                		<g:a href="/mobile">
							<g:img
								src="http://www.guichevirtual.com.br/resources/img/logo.png"
								alt="logo" style="width: 180px" />
						</g:a>
                    	<h1 class="Destaque">O lugar certo para comprar sua passagem de ônibus</h1>
                    </div>
                </div>
                <br>
                <!-- <center> -->
	                <div class="scroll">
		            	<div id="faq">
		            		<g:img src="/resources/img/busGo.png"></g:img>
		            		<table border="0" cellpadding="0" cellspacing="0"
							style="margin-bottom: 0px !important; margin-top: -20px;">
              <tr>
                <td valign=top>
                  <g:img src="${image}">
                  </g:img>
                  <div class="setSizeTable">
                    <table class="table table-bordered table-hover tableResults" style="width: 359px !important;">
                      <thead>
                        <tr class="setSizeTR"
													style="color: white; text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.6) !important;">
                          <td style="width: 137px; text-align: center;">
                            Saída
                          </td>
                          <td class="td2" style="text-align: center;">
                            Preço
                          </td>
                          <td class="td1" style="text-align: center;">
                            Empresa
                          </td>
                        </tr>
                        <tr class="setSizeTR2">
                          <td style="width: 136px;">
                          </td>
                          <td class="td2">
                          </td>
                          <td class="td1">
                          </td>
                        </tr>
                      </thead>
                      <c:if test="${empty result}">
                      	<tr>
                      	<td colspan="6">Nenhum resultado encontrado</td>
                      	</tr>
                      </c:if>
                      <c:forEach items="${result}" var="item">
                          <tr class="backgroundSearch search-result"
													style="background-color: white;">
                            <td style="width: 135px;"
														class="resultsCell">
                              <input type="radio"
														name="service${travelName}" data-type="${travelName}"
														value="${item.servico},${item.grupo},${item.empresa}"
														class="bus-select"
														id="${item.servico},${item.grupo},${item.empresa}" />
                              <label
														for="${item.servico},${item.grupo},${item.empresa}"><helper:BusDateFormat
																date="${item.saida }" fromDate="${travelDate}" />
                              <helper:BusDateFormat
																date="${item.saida }" fromDate="${travelDate}"
																type="time" /></label>
                            </td>
                            <td class="td2 resultsCell">
                              <label
														for="${item.servico},${item.grupo},${item.empresa}">${item.preco } </label>
                            </td>
                            <td class="td1 resultsCell">
                              <bus:companyImg key="${item.empresa}"
															size="P"
															cssStyle="max-height:30px;margin-top: -10px; margin-bottom: -5px;" />
                            </td>
                          </tr>
                      </c:forEach>
                    </table>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                <br>
                  <div id="layout-${travelName}">
                    <!-- preenchido com layout de onibus de ida -->
                  </div>
                </td>
              </tr>
            </table>
						  </div>
						 </div>
		                <br>
	             <!-- </center> -->
                <div class="rodape Destaque">
                	 <center>
                		<h1 class="Destaque">Guichê Virtual</h1>
                		<a href="http://www.guichevirtual.com.br/blog/"
							class="ellipsis fontFooter" style="" target="_blank">Blog</a>
                		<a href="#" class="fontFooter" style="" target="">Minhas Viagens</a>
                		<a href="#" class="fontFooter" style="" target="">Dúvidas</a>
                	 </center>
                </div>
            </div>
        </div>
  <!--/content-->
</jsp:body>
</g:master>