<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="g" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="gf" tagdir="/WEB-INF/tags/form"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page pageEncoding="UTF-8"%>
<g:master
	description="The Best Todo List in town" >
	<jsp:attribute name="head">
	<g:css url="/resources/js/select2/select2.css"></g:css>
</jsp:attribute>
	<jsp:body>
	<div ng-app="todomodule" ng-cloak>
	<g:script url="/resources/js/todos.js"></g:script>
  	<!--conten-->
	  <div class="span12">
	  	<h1 class="text-center">The Best Todo List in Town</h1>
	  </div>
	  
	   <div id="todos-content" ng-controller="TodoListController">
	   <div ng-show="todolists.length==0" class="well form-horizontal span8 offset2">
	   		<h4 class="text-center">New Todo List</h4>
	   		<form action="#" >
	   			<div class="control-group">
		   			<div class="controls">
			   			<label for="todolist-name">List name</label>
		   				<input id="todolist-name" type="text" maxlength="140" ng-model="newList.name">
		   			</div>
	   			</div>
		   		<div class="controls">
	   				<input type="button" ng-click="createTodoList(newList.name)" value="Save" 
	   					class="btn-primary btn">
	   			</div>
	   		</form>
	   </div>
	   <div ng-hide="todolists.length==0" class="span8 offset2">
	   		<h4 class="text-center">{{list.name}}</h4>
	   		<hr>
	   		<div class="text-center basic-controls form-horizontal">
		   		<input type="button" class="btn btn-primary" value="New Task" ng-click="newEntry()">
		   		<span class="text-right inline">
			   		<label for="order-field" style="display:inline;">Order by</label>
			   		<select ui-select2 ng-change="order()" ng-model="orderField">
			   			<option value="priority-desc">Highest priority</option>
			   			<option value="priority-asc">Lowest priority</option>
			   			<option value="dueDate-asc">Closest due date</option>
			   			<option value="dueDate-desc">Longer due date</option>
			   			<option value="completed-desc">Completed first</option>
			   		</select>
		   		</span>
	   		</div>
	   		<div ng-repeat="todo in list.entries">
	   			<div class="todo-resume alert alert-info" >
	   			<p class="{{todo.completed}}">
	   				<span class="bold">{{todo.title}}</span> <span class="label label-info" ng-show="todo.priority"> {{options[todo.priority+'']}}</span> <span class="label label-info" ng-show="todo.dueDate"> {{formatDate(todo.dueDate)}}</span>
	   				<input type="checkbox" ng-model="todo.completed" ng-change="todo.dirty=true">
	   				<a href="javascript:void(0)" class="pull-right btn " ng-hide="todo.edit" ng-click="todo.edit=!todo.edit">Open</a>
	   				<a href="javascript:void(0)" class="pull-right btn btn-danger" ng-show="todo.edit" ng-click="todo.edit=!todo.edit">Close</a>
	   				<a href="javascript:void(0)" ng-show="todo.dirty||todoForm.$dirty" class="pull-right btn btn-primary" ng-click="saveTodo(todo)">Save</a>
	   			</p>
	   			</div>
	   			<form name="todoForm" ng-show="todo.edit" class="well todo-edit form-horizontal">
	   			<div class="edit-header">
	   				<h4>{{todo.title}}<a class="close" href="javascript:void(0)" ng-click="todo.edit=false">&times;</a></h4>
	   				<hr>
	   			</div>
	   			<div class="control-group">
		   			<div class="controls">
		   				<input type="button" ng-click="saveTodo(todo)" class="btn btn-primary" value="Save" ng-disabled="!todoForm.$valid">
			   			<input type="button" ng-click="removeTodo(todo)" class="btn btn-danger" value="Delete">
		   			</div>
	   			</div>
		   		<div class="control-group">
		   			<label class="control-label" for="title-{{$index}}">Task title</label>
		   			<div class="controls">
			   			<input id="title-{{$index}}" type="text" required ng-model="todo.title" placeholder="Titulo da task">
			   		</div>
			   	</div>
		   		<div class="control-group">
			   		<label class="control-label" for="priority-{{$index}}">Priority</label>
			   		<div class="controls inline">
			   			<select id="priority-{{$index}}" ng-model="todo.priority" ui-select2>
			   				<option value="" selected="selected">No Priority</option>
			   				<option value="5" >Critical</option>
			   				<option value="4" >High</option>
			   				<option value="3" >Medium</option>
			   				<option value="2" >Low</option>
			   			</select>
			   		</div>
			   	</div>
		   		<div class="control-group">
			   		<label class="control-label" for="dueDate-{{$index}}">Due date</label>
			   		<div class="controls">
			   			<input id="dueDate-{{$index}}" type="text" ui-date ng-model="todo.dueDate" ui-date-parse="yy-mm-dd">
			   		</div>
			   	</div>
		   		<div class="control-group">
			   		<label class="control-label" for="description-{{$index}}">Description</label>
			   		<div class="controls">
		   			<textarea id="description-{{$index}}" rows="3" cols="20" ng-model="todo.description">
		   			</textarea>
		   			</div>
		   		</div>
		   		</form>
		   		</div>
		   			
	   			</div>
	   		</div>
	   	
	   </div>
	   
	   
       </div>
	</div>
  <g:script url="/resources/js/angular.min.js"></g:script>
  <g:script url="/resources/js/angular-ui.js"></g:script>
  <g:script url="/resources/js/angular-ui-ieshiv.min.js"></g:script>
  <g:script url="/resources/js/select2/select2.min.js"></g:script>
  <script type="text/javascript">
  	angular.module('todomodule',['ui']);
  </script>
  
</jsp:body>
</g:master>