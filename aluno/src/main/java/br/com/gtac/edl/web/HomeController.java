package br.com.gtac.edl.web;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import lombok.extern.java.Log;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.gtac.edl.domain.register.LoginUserDTO;
import br.com.gtac.edl.domain.register.RegisterUserDTO;

/**
 * Handles requests for the application home page.
 */
@Controller
@Log
public class HomeController {

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model,
			@RequestParam(value = "error", required = false) Boolean error,
			@RequestParam(required = false) String flashCode,
			HttpServletRequest request) {
		
		model.addAttribute("registerUser",  new RegisterUserDTO());
		model.addAttribute("loginUser", new LoginUserDTO());
		return "home";
	}

	@RequestMapping(value = "/about", method = RequestMethod.GET)
	public String home(Locale locale, Model model,
			HttpServletRequest request) {

		return "about";
	}


}
