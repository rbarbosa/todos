package br.com.gtac.edl.web.user;

import java.util.Locale;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import lombok.extern.slf4j.Slf4j;

import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.gtac.edl.core.service.UserService;
import br.com.gtac.edl.core.util.ServerUtils;
import br.com.gtac.edl.core.util.ui.Flash;
import br.com.gtac.edl.core.web.DataIntegrityExceptionTranslator;
import br.com.gtac.edl.domain.register.RegisterUserDTO;
import br.com.gtac.edl.domain.register.User;
import br.com.gtac.edl.web.mail.SendEmailService;

@Controller
@Slf4j
public class LoginController {
    
    	@Inject
    	private UserService userService;
    
	@Inject
	private HttpSessionRequestCache requestCache;
	
	@Inject
	private Flash flash;
	
	@Inject
	private DataIntegrityExceptionTranslator exceptionTranslator;
	
	@Inject
	private SendEmailService sendEmailService;
	
	@Inject
	private MessageSource messageSource;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String getLoginPage(@RequestParam(required = false) boolean error,
			@RequestParam(required = false) boolean register,
			Model model, HttpServletRequest request) {
		log.info("Received request to show login page");
		model.addAttribute("isRegister", register);
		RegisterUserDTO registerUser = new RegisterUserDTO();
		model.addAttribute("registerUser", registerUser);
		if (error == true) {
			model.addAttribute(
					"error",
					messageSource.getMessage("login.error", null,
							request.getLocale()));
		}

		return "login";
	}
	

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String register(
			@Valid @ModelAttribute(value = "registerUser") RegisterUserDTO registerUser,
			BindingResult br, Model model, Locale locale,
			HttpServletRequest request, HttpServletResponse response) {

		if (!registerUser.getConfirmPass().equals(registerUser.getPassword()))
			br.addError(new FieldError("registerUser",
					"confirmPass",
				"Senhas diferentes!"));

		if (br.hasErrors()) {
			log.info("bind errors: " + br.getErrorCount());
			model.addAttribute("register", false);
			return validationError(registerUser, model, "registerUser");
		}try {
			userService.registerUser(registerUser);

		} catch (DataIntegrityViolationException ex) {
			log.info("exception thrown on save: " + br.getErrorCount());
			String message = exceptionTranslator.translateException(ex, locale,
					br);
			if (message != null) {
				model.addAttribute("constraintError", message);
			}
			return "login";
		}
		
		User createdUser = userService.findOneByEmail(registerUser.getEmail());
		userService.authenticate(createdUser);
		SavedRequest last = requestCache.getRequest(request, response);
		if (last != null) {
			flash.success("application.register.success");
			return "redirect:" + last.getRedirectUrl();
		}
		return "redirect:/";
	}

	private String validationError(RegisterUserDTO registerUser, Model model,
			String modelName) {
		model.addAttribute(modelName, registerUser);
		return "login";
	}

	@RequestMapping("/forgottenPass")
	@ResponseBody
	public String forgottenPass(@RequestParam("email") String email,
			HttpServletRequest request) throws Exception {
		User user = userService.findOneByEmail(email);
		if (user == null)
			return "email";


		String token = userService.createLostPassToken(user);
		String url = String.format("http://%s%s/user/reset?token=%s",
				ServerUtils.getHostname(request), request.getContextPath(),
				token);
		return "ok";
	}
}
