package br.com.gtac.edl.web.tag;

import javax.servlet.jsp.JspWriter;

import lombok.Getter;
import lombok.Setter;

import org.springframework.web.servlet.tags.RequestContextAwareTag;
import org.springframework.web.util.TagUtils;

import br.com.gtac.edl.domain.util.StringUtils;

public class UrlPrettifier extends RequestContextAwareTag {
	private static final long serialVersionUID = -1058496394293605802L;
	@Getter @Setter
	private String url;
	@Getter @Setter
	private String var;
	
	
	@Override
	protected int doStartTagInternal() throws Exception {
		try{
			String prettyUrl = StringUtils.urlPrettyfier(url);
			if(org.apache.commons.lang3.StringUtils.isBlank(var)){
				JspWriter out = pageContext.getOut();
				out.write(prettyUrl);
			}else{
				pageContext.setAttribute(var, prettyUrl, TagUtils.getScope("page"));
			}
		}catch(Exception e){}
		
		return EVAL_BODY_INCLUDE;
	}
}
