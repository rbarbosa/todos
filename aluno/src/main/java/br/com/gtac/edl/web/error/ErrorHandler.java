package br.com.gtac.edl.web.error;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ErrorHandler {
    
    @RequestMapping(value="/nao-encontrado" )
    public String pageNotFound(HttpServletResponse response,Model model){
	response.setStatus(404);
	return "pageNotFound";
    }
}
