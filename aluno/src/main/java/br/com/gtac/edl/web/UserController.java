package br.com.gtac.edl.web;

import java.util.Locale;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.gtac.edl.core.dao.UserConnectionDAO;
import br.com.gtac.edl.core.dao.UserDAO;
import br.com.gtac.edl.core.exception.PasswordChangeException;
import br.com.gtac.edl.core.service.UserService;
import br.com.gtac.edl.core.util.ServerUtils;
import br.com.gtac.edl.core.util.ui.Flash;
import br.com.gtac.edl.domain.register.ChangePasswordDTO;
import br.com.gtac.edl.domain.register.RegisterUserDTO;
import br.com.gtac.edl.domain.register.ResetUserDTO;
import br.com.gtac.edl.domain.register.User;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserDAO dao;
	@Autowired
	private UserService service;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private PasswordEncoder encoder;

	@Inject
	private UserConnectionDAO userConnectionDao;
	
	@Inject
	private RequestCache requestCache;

	@Inject
	private Flash flash;

	@RequestMapping("/forgottenPass")
	@ResponseBody
	public String forgottenPass(@RequestParam("email") String email,
			HttpServletRequest request) throws Exception {
		User user = dao.findOneByEmail(email);
		if (user == null)
			return "email";

		String token = service.createLostPassToken(user);
		String url = String.format("http://%s%s/user/reset?token=%s",
				ServerUtils.getHostname(request), request.getContextPath(),
				token);
		return "ok";
	}

	@RequestMapping(value = "/reset", method = RequestMethod.GET)
	public String reset(@RequestParam("token") String token, Model model) {
		User user = dao.findOneByLostPassToken(token);
		if (user == null)
			return "user/reset-error";

		if (StringUtils.isBlank(user.getEmail()))
			return "redirect:/user/set?token=" + token;

		ResetUserDTO dto = ResetUserDTO.create(user);
		model.addAttribute("bean", dto);

		return "user/reset";
	}

	@RequestMapping(value = "/reset", method = RequestMethod.POST)
	public String resetPost(@RequestParam("token") String token,
			@Valid @ModelAttribute("bean") ResetUserDTO ruDto,
			BindingResult br, Model model,HttpServletRequest request, HttpServletResponse response) {
		User user = dao.findOneByLostPassToken(token);

		if (br.hasErrors()) {
			model.addAttribute("bean", ruDto);
			return "user/reset";
		}

		if (user == null)
			return "user/reset-error";

		try {
			service.resetPassword(token, ruDto.getPassword());
			flash.success("reset.password.success");
			SavedRequest last = requestCache.getRequest(request, response);
			if (last != null) {
				return "redirect:" + last.getRedirectUrl();
			}
			return "redirect:/";
		} catch (Exception e) {
			return "user/reset-error";
		}
	}

	@RequestMapping(value = "/change", method = RequestMethod.GET)
	public String change(Model model, BindingResult bindingResult)
			throws PasswordChangeException {
		User user = service.getCurrentUser();

		if (bindingResult.hasErrors()) {
			flash.error("Senha inválida.");
			return "redirect:/perfil";
		}

		if (user == null)
			return "user/reset-error";

		model.addAttribute("changePassword", ChangePasswordDTO.create(user));
		return "user/edit";
	}

	@RequestMapping(value = "/change", method = RequestMethod.POST)
	public String changePost(
			@Valid @ModelAttribute(value = "changePassword") ChangePasswordDTO changePasswordDTO,
			BindingResult bindingResult, Model model, Locale locale)
			throws PasswordChangeException {

		if (bindingResult.hasErrors()) {
			model.addAttribute("changePassword", changePasswordDTO);
			return "redirect:/perfil";
		}
		try {
			service.changePassword(changePasswordDTO.getUser().getId(),
					changePasswordDTO.getOldPassword(),
					changePasswordDTO.getPassword());
		} catch (PasswordChangeException p) {
			String message = messageSource.getMessage(p.getMessageCode(), null,
					locale);
			bindingResult.addError(new ObjectError("user", message));
			model.addAttribute("changePassword", changePasswordDTO);
			flash.error("invalid.password");
			return "redirect:/perfil";
		}
		flash.success("password.change.success");
		return "redirect:/perfil";
	}


	@RequestMapping(value = "/set", method = RequestMethod.GET)
	public String set(@RequestParam("token") String token, Model model) {
		User user = dao.findOneByLostPassToken(token);
		if (user == null)
			return "user/reset-error";

		RegisterUserDTO dto = RegisterUserDTO.create(user);
		dto.setFullName("");
		model.addAttribute("bean", dto);
		return "user/set";
	}

	@RequestMapping(value = "/set", method = RequestMethod.POST)
	public String setPost(@RequestParam("token") String token,
			@Valid @ModelAttribute("bean") RegisterUserDTO spd,
			BindingResult br, Model model) {
		User user = dao.findOneByLostPassToken(token);

		if (user == null)
			return "redirect:/";

		if (!StringUtils.isBlank(spd.getEmail())) {
			User similarUser = dao.findOneByEmail(spd.getEmail());
			if (similarUser != null) {
				ObjectError error = new FieldError("bean", "email",
						spd.getFullName(), false,
						new String[] { "user.repeatedUsername" },
						new Object[] { spd.getEmail() }, "Usuário já existente");

				br.addError(error);
				model.addAttribute("bean", spd);
				return "user/set";
			}
		}

		if (br.hasErrors()) {
			model.addAttribute("bean", spd);
			return "user/set";
		}

		try {
			service.setPassword(token, spd.getPassword(), spd.getFullName());
			return "redirect:/";
		} catch (Exception e) {
			model.addAttribute("bean", spd);
			br.addError(new ObjectError("bean",
					new String[] { "user.set.error" }, null,
					"Erro ao mudar a senha"));
			return "user/set";
		}
	}


}
