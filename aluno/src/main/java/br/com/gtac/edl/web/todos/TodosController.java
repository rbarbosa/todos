package br.com.gtac.edl.web.todos;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.gtac.edl.core.service.todo.TodoListService;
import br.com.gtac.edl.domain.todo.TodoEntry;
import br.com.gtac.edl.domain.todo.TodoList;

@Controller
public class TodosController {

	private static final String ISO_DATE_FORMAT = "yyyy-MM-dd";
	@Inject
	private TodoListService service;

	@RequestMapping(value = "/todos", method = RequestMethod.GET)
	public String todos() {
		return "todos";
	}

	@RequestMapping(value = "/todos/rest/list", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String create(@RequestBody String json, HttpServletRequest request,
			HttpServletResponse response) {
		try {

			TodoList list = parseList(json);
			TodoList saved = service.create(list.getName());
			response.setStatus(HttpStatus.CREATED.value());
			return saved.getId().toString();
		} catch (Exception e) {
			response.setStatus(500);
			return "error:" + e.getMessage();
		}
	}

	private TodoList parseList(String json) {
		try {
			return getMapper().readValue(json, TodoList.class);
		} catch (IOException e) {
			e.printStackTrace();
			throw new IllegalStateException("Illegal state", e);
		}
	}

	@RequestMapping(value = "/todos/rest/list", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String list(HttpServletRequest request, HttpServletResponse response) {
		try {
			List<TodoList> all = service.findAllFromUser();
			response.setStatus(HttpStatus.ACCEPTED.value());
			return serializeList(all);
		} catch (Exception e) {
			response.setStatus(500);
			return "error:" + e.getMessage();
		}
	}

	@RequestMapping(value = "/todos/rest/list/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String listAll(@PathVariable Long id, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			TodoList saved = service.findOneList(id);
			response.setStatus(HttpStatus.ACCEPTED.value());
			return serializeList(saved);
		} catch (Exception e) {
			response.setStatus(500);
			return "error:" + e.getMessage();
		}
	}

	private String serializeEntry(TodoEntry saved) {
		try {
			return getMapper().writeValueAsString(saved);
		} catch (IOException e) {
			throw new IllegalStateException("Illegal state");
		}
	}

	private String serializeList(Object saved) {
		try {

			return getMapper().writeValueAsString(saved);
		} catch (IOException e) {
			throw new IllegalStateException("Illegal state");
		}
	}

	@RequestMapping(value = "/todos/rest/entry/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String entry(@PathVariable Long id, HttpServletRequest request,
			HttpServletResponse response) throws IllegalAccessException {
		try {
			TodoEntry saved = service.findOneEntry(id);
			response.setStatus(HttpStatus.ACCEPTED.value());
			return serializeEntry(saved);
		} catch (Exception e) {
			response.setStatus(500);
			return "error:" + e.getMessage();
		}
	}

	@RequestMapping(value = "/todos/rest/entry", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String createEntry(@RequestBody String json,
			HttpServletRequest request, HttpServletResponse response)
			throws IllegalAccessException {
		try {
			TodoEntry entry = parseEntry(json);
			TodoEntry saved = service.createEntry(entry, entry.getParent()
					.getId());
			response.setStatus(HttpStatus.CREATED.value());
			return saved.getId().toString();
		} catch (Exception e) {
			response.setStatus(500);
			return "error:" + e.getMessage();
		}
	}

	@RequestMapping(value = "/todos/rest/entry/{id}", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String updateEntry(@RequestBody String json, @PathVariable Long id,
			HttpServletRequest request, HttpServletResponse response)
			throws IllegalAccessException {
		try {
			TodoEntry entry = parseEntry(json);
			entry.setId(id);
			TodoEntry saved = service.updateEntry(entry);
			response.setStatus(HttpStatus.ACCEPTED.value());
			return saved.getId().toString();
		} catch (Exception e) {
			response.setStatus(500);
			return "error:" + e.getMessage();
		}
	}

	@RequestMapping(value = "/todos/rest/entry/remove/{id}", method = RequestMethod.POST)
	@ResponseBody
	public String removeEntry(@PathVariable Long id,
			HttpServletRequest request, HttpServletResponse response)
			throws IllegalAccessException {
		try {
			service.removeEntry(id);
			response.setStatus(HttpStatus.ACCEPTED.value());
			return "ok";
		} catch (Exception e) {
			response.setStatus(500);
			return "error:" + e.getMessage();
		}
	}

	private TodoEntry parseEntry(String json) {
		try {
			return getMapper().readValue(json, TodoEntry.class);

		} catch (IOException e) {
			throw new IllegalStateException("Cant parse request");
		}
	}

	private ObjectMapper getMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setDateFormat(new SimpleDateFormat(ISO_DATE_FORMAT));
		mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper;
	}

}
