<#import "/masterEmail.ftl" as layout>

<#setting locale="pt_BR">
<#macro reserveDesc reserve stations reserve_index>
  <p class="text">
			<table>
				<tr>
					<td style="width: 200px;">
						Auto Viação:
					</td>
						<#if reserve.busCompanyObject??>
						<td>
							<strong>${reserve.busCompanyObject.name}</strong>

							
						</td>
							<#if reserve.imageUrl??>
							<td>
									<img src="${reserve.imageUrl}" style="max-height:50px;" alt="${reserve.busCompanyObject.name}" title="${reserve.busCompanyObject.name}" />
							</td>
							</#if>
						<#else>
							<td>
								<strong>${reserve.company}</strong>
							</td>
						</#if>
				</tr>
				<tr>
					<td>
						Origem:
					</td>
					<td>
						<strong>${stations[reserve_index].origin}</strong>
					</td>
				</tr>
				<tr>
					<td>
						Destino:
					</td>
					<td>
						<strong>${stations[reserve_index].destination}</strong>
					</td>
				</tr>
				<tr>
					<td>
						Saída:
					</td>
					<td>
						<strong>${reserve.travelDate?string("dd/MM/yyyy HH:mm")}</strong>
					</td>
				</tr>
				<tr>
					<td>
						Chegada:
					</td>
					<td>
						<strong>${reserve.arrivalDate?string("dd/MM/yyyy HH:mm")}</strong>
					</td>
				</tr>
				<tr>
					<td>
						Poltrona:
					</td>
					<td>
						<strong>${reserve.seat}</strong>
					</td>
				</tr>
				<tr>
					<td>
						Bilhete:
					</td>
					<td>
						<strong>${reserve.boleto}</strong>
					</td>
				</tr>
				<tr>
					<td>
						Nome do Passageiro:
					</td>
					<td>
						<strong>${reserve.name}</strong>
					</td>
				</tr>
				<tr>
					<td>
						Documento:
					</td>
					<td>
						<strong>${reserve.document}</strong>
					</td>
				</tr>
				
			</table>
				<hr></hr>
			</p>
</#macro>

<@layout.masterTemplate title="Status do Pagamento">
		<h2 class="title">Olá ${user.info.fullName},</h2>
		<h3 style="color: gray;">Obrigado por comprar no Guichê Virtual.</h3>
		<p class="text">
			Sua compra foi realizada com sucesso na data <strong>${operation.paymentStatusUpdateDate?string("dd/MM/yyyy HH:mm:ss")}</strong> em nosso sistema.<br>
			<h5>Não é obrigatória a impressão e apresentação deste Voucher para retirada da sua passagem no Guichê da Auto Viação.</h5>
			
			Orientações para Retirada da passagem e Embarque :
			<br>&bull;  Apresente-se diretamente ao Guichê da Auto Viação com pelo menos uma (01) hora de antecedência ao horário de embarque, para retirada do bilhete rodoviário, depois compareça ao setor de embarque no horário determinado em sua passagem, e boa viagem!
			<br>&bull;  Para retirada de sua passagem é obrigatória a apresentação de documento de identidade original com fotografia . Cópias, mesmo as autenticadas, não serão aceitas.   
			<br><br>
			<center><div style="background-color:#31849B; color: white; font-size: 21px;"> Voucher de compra de passagem rodoviária </div></center><br>
			AGÊNCIA J3 (99997)<br>
			<h2>DETALHES DO PEDIDO</h2> 					
			<table>
				<tr>
					<td style="width: 200px;">
						Número do pedido:
					</td>
					<td>
						<strong>${operation.id}</strong>
					</td>
				</tr>
				<tr>
					<td style="width: 200px;">
						Data da compra:
					</td>
					<td>
						<strong>${operation.paymentStatusUpdateDate?string("dd/MM/yyyy HH:mm:ss")}</strong>
					</td>
				</tr>
			</table>
			<h2>DETALHES DA PASSAGEM</h2>
			<#list  reserves as reserve>
				<@reserveDesc reserve stations reserve_index />
			</#list>
			<h2>Valores</h2>
			<table border="1" cellspacing="0" style="width:100%">
				<tr>
					<td align="center">
						Trecho
					</td>
					<td align="center">
						Tarifa
					</td>
					<td align="center">
						Impostos
					</td>
					<td align="center">
						Pedágio
					</td>
					<td align="center">
						Seguro
					</td>
					<td align="center">
						Outros
					</td>
					<td align="center">
						Valor da Passagem
					</td>
				</tr>
				<#list  reserves as reserve>
				<tr>
				<#if reserve.goReserve??>
					<td align="center">
						<#if reserve.goReserve>		
							<strong>Ida</strong>
						<#else>
							<strong>Volta</strong>
						</#if>
					</td>
					<#else>
					<td align="center">
						
					</td>
				</#if>
					<td align="center">
						<strong>${reserve.price.base?string.currency}</strong>
					</td>
					<td align="center">
						<strong>${reserve.price.taxes?string.currency}</strong>
					</td>
					<td align="center">
						<strong>${reserve.price.toll?string.currency}</strong>
					</td>
					<td align="center">
						<strong>Incluso<!--${reserve.price.insurance?string.currency}--></strong>
					</td>
					<td align="center">
						<strong>${reserve.price.others?string.currency}</strong>
					</td>
					<td align="center">
						<strong>${reserve.price.totalPriceWith?string.currency}</strong>
					</td>
				</tr>
				</#list>
			</table>
			
			</p>	
				<h2>Detalhes do Pagamento</h2>
			<p class="text">
			<table>
				<tr>
					<td style="width: 200px;">
						Responsável pelo pagamento:
					</td>
					<td>
						<strong>${user.info.fullName}</strong>
					</td>
				</tr>
				<tr>
					<td>
						Total dos bilhetes:
					</td>
					<td>
						<strong>${operation.pricesSum?string.currency}</strong>
					</td>
				</tr>
				<tr>
					<td>
						Taxa de conveniência
					</td>
					<td>
						<strong>${operation.taxPrice?string.currency}</strong>
					</td>
				</tr>
				<tr>
				<tr>
					<td>
						Total do pedido:
					</td>
					<td>
						<strong>${(operation.taxPrice+operation.pricesSum)?string.currency}</strong>
					</td>
				</tr>
				<tr>
					<td>
						Tipo de pagamento:
					</td>
					<td>
						<strong>Cartão de crédito</strong>
					</td>
				</tr>
				<tr>
					<td>
						Status:
					</td>
					<td>
						<strong>Pago</strong>
					</td>
				</tr>
			</table>
			</p>
			<hr></hr>
			<h3>Informações gerais</h3>
				<p class="text" style="font-size:12px;">
				<b>Impressão do Bilhete Rodoviário:</b>
				Para sua maior tranquilidade e conforto, recomendamos apresentar-se no guichê da autoviação para impressão do seu bilhete com pelo menos uma (01) hora de antecedência ao horário de embarque, munido de documento de identidade original com fotografia. O bilhete de passagem é nominal e intransferível. O bilhete não é endossável.
				<br><br>
				<b>Transferência ou Cancelamento da Passagem Rodoviária: </b>
				Fica assegurado ao usuário solicitar a transferência da data ou horário de embarque, ou ainda, o cancelamento da passagem, desde que a solicitação seja feita diretamente no guichê da Empresa Transportadora, no local de origem da viagem , com antecedência mínima de 03 três horas do horário estabelecido para o embarque. Nos casos de pagamento com cartão de crédito, a devolução dos valores pagos referente ao valor da passagem será efetuada de acordo com o repasse da Administradora à Operadora. 
				 §1º: Nos termos do art.8º, §2º da Resolução n.º 978 da ANTT, fica a transportadora autorizada a reter até 5% (cinco por cento) do valor pago pela passagem rodoviária, a título de multa compensatória, no caso de o passageiro realizar o cancelamento ou a transferência, dentro do prazo previsto no caput desta cláusula. 
				§2º: A transferência de horário ficará condicionada à disponibilidade de passagens na data e horário desejados pelo usuário, ficando ainda assegurado ao Usuário a opção pela passagem com data e horário "em aberto", assim permanecendo pelo prazo, máximo, de 12 (doze) meses, contados da data do bilhete original, ficando sujeito a reajuste de preço se não utilizada dentro desse prazo.
				* Taxa de Conveniência : O Cliente reconhece e autoriza a cobrança da taxa de conveniência no ato da compra do bilhete rodoviário através deste canal de venda. Trata-se da fonte de renda  na qual Guichê Virtual garante distribuição de passagens rodoviárias, bem como a utilização de cartões, boleto, etc. <b>Em nenhuma hipótese, o valor cobrado de taxa de conveniência será reembolsado.</b>
				<br><br>
				<b>Transporte de Bagagens:</b>
				A vista do disposto no artigo 70 e seguintes do Decreto 2521/98, o preço da passagem abrange, a título de franquia, o transporte obrigatório e gratuito de bagagem no bagageiro e no volume de porta-embrulhos observados os seguintes limites máximos de peso e dimensão:
				a) bagageiro, trinta quilos de peso total e volume máximo de trezentos centímetros cúbicos, limitada a maior dimensão de qualquer volume a um metro e no porta-embrulhos, cinco quilos de peso total, com dimensões que se adaptem ao local, desde que o conforto, a segurança e a higiene dos passageiros não sejam prejudicados;
				b) excedida a franquia prevista, o passageiro pagará até meio por cento do preço da passagem correspondente ao serviço convencional, pelo transporte de cada quilograma de excesso; e 
				c) é vedado o transporte de produtos considerados perigosos, indicados na legislação específica, bem como aqueles que, por sua forma ou natureza, comprometam a segurança do veículo, de seus ocupantes ou de terceiros, nos termos do art.72 do Decreto 2521/98.
				<br>
				</p>
				<hr></hr>
				<p>Para o caso de informaçoes ou dúvidas, entre em contato através do telefone <b>(12) 3206 1332</b> e do site <a href="http://www.guichevirtual.com.br" target="_blank">www.guichevirtual.com.br</a><br><br>
				O Guichê Virtual deseja uma BOA VIAGEM!!!
				</p>
</@layout.masterTemplate>
