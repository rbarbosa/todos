package br.com.gtac.edl.domain.register;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import br.com.gtac.edl.domain.learning.validation.EmailConstraint;
import br.com.gtac.edl.domain.learning.validation.UsernameConstraint;

@Data
public class CreateUserFromInviteDTO {
	
	@NotNull(message = "{validation.notNull}")
	@UsernameConstraint(min = 4, max = 16, sizeMessage = "{DoubleConstraint.message}")
	private String username;

	@NotNull(message = "{validation.notNull}")
	@Size(min = 4, max = 32, message = "{message.size}")
	private String password;
	
	@EmailConstraint
	private String email;
	
	private Long inviteId;
	private boolean terms;

}
