package br.com.gtac.edl.domain.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Digao
 *
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ConcreteSearchClass {

	/** 
	 * @return codigo da mensagem a ser utilizado
	 */
	Class<?> value();
}
