package br.com.gtac.edl.domain.learning.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

import br.com.gtac.edl.domain.register.User;


public class UserPasswordValidator implements
		ConstraintValidator<UserPasswordConstraint, User> {

	@Override
	public void initialize(UserPasswordConstraint constraintAnnotation) {
		
	}

	@Override
	public boolean isValid(User value, ConstraintValidatorContext context) {

		if (value.getId() == null) {
			if(StringUtils.isEmpty(value.getPassword()) || value.getPassword().length()<4){
				context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate()).
					addNode("password").addConstraintViolation();
				return false;
			}
		}else{
			if(!StringUtils.isEmpty(value.getPassword()) && value.getPassword().length()<4){
				context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate()).
					addNode("password").addConstraintViolation();
				return false;
			}
		}
		return true;
	}

}
