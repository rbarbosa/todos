package br.com.gtac.edl.domain.learning.validation;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.PropertyUtils;

public class UniquePropertyValidator implements
		ConstraintValidator<UniquePropertyConstraint, Collection<?>> {

	private String field;

	@Override
	public void initialize(UniquePropertyConstraint constraintAnnotation) {
		field = constraintAnnotation.field();
	}

	@Override
	public boolean isValid(Collection<?> value,
			ConstraintValidatorContext context) {
		Set<String> compareValues = new HashSet<String>();

		for (Object obj : value) {
			compareValues.add(getValue(obj));
		}
		
		if(value.size() != compareValues.size())
			return false;

		return true;
	}

	private String getValue(Object obj) {
		try {
			return (String) PropertyUtils.getProperty(obj, field);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

}
