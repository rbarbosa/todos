package br.com.gtac.edl.domain.register;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class ChangePasswordDTO {
	@NotNull
	private User user;
	private String oldPassword;
	@Size(min = 4, max = 32, message = "{password.change.invalid}")
	private String password;

	public static ChangePasswordDTO create(User user) {
		ChangePasswordDTO dto = new ChangePasswordDTO();
		dto.setUser(user);
		dto.setOldPassword("");
		dto.setPassword("");

		return dto;
	}
}
