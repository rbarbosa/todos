package br.com.gtac.edl.domain.register;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import br.com.gtac.edl.domain.learning.validation.EmailConstraint;

@Data
public class RegisterUserDTO {

	private User user;
	
	@NotNull(message = "{validation.notNull}")
	@Size(min = 4, max = 32, message = "{message.size}")
	private String password;

	@NotNull(message = "{validation.notNull}")
	@Size(min = 4, max = 32, message = "{message.size}")
	private String confirmPass;

	@EmailConstraint(message = "{validation.email}")
	private String email;
	
	@EmailConstraint(message = "{validation.email}")
	private String emailVerify;
	
	@NotNull(message = "{validation.notNull}")
	private String fullName;
	
	private boolean newsletter=true;

	public static RegisterUserDTO create(User user) {

		RegisterUserDTO dto = new RegisterUserDTO();
		dto.setUser(user);
		dto.setFullName(user.getInfo().getFullName());
		dto.setEmail(user.getEmail());

		return dto;
	}
	
	public static RegisterUserDTO create(String username, String email, String password) {

		RegisterUserDTO dto = new RegisterUserDTO();

		dto.setEmail(email);
		dto.setFullName(username);
		dto.setPassword(password);

		return dto;
	}

}
