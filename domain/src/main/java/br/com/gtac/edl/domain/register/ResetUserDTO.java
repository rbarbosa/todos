package br.com.gtac.edl.domain.register;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class ResetUserDTO {
	
	private User user;
	
	@NotNull(message = "{validation.notNull}")
	@Size(min = 4, max = 32, message = "{message.size}")
	private String password;

	public static ResetUserDTO create(User user) {
		ResetUserDTO dto = new ResetUserDTO();
		dto.setUser(user);

		return dto;
	}

}
