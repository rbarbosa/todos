package br.com.gtac.edl.domain.register;

public enum AcademicSituation {
	COMPLETE("situation.complete"), INPROGRESS("situation.inprogress"), ABANDONED(
			"situation.abandoned");

	private final String labelCode;

	AcademicSituation(String labelCode) {
		this.labelCode = labelCode;
	}

	@Override
	public String toString() {
		if (this.equals(AcademicSituation.COMPLETE))
			return "Completo";
		if (this.equals(AcademicSituation.INPROGRESS))
			return "Em Andamento";
		if (this.equals(AcademicSituation.ABANDONED))
			return "Abandonado";
		else
			return "";
	};

	public String getLabelCode() {
		return labelCode;
	}

}
