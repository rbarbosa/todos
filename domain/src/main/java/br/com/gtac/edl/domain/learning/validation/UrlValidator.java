package br.com.gtac.edl.domain.learning.validation;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class UrlValidator implements ConstraintValidator<UrlConstraint, String> {

	@Override
	public void initialize(UrlConstraint constraintAnnotation) {

	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		try {
			URL url = new URL(value);
			URLConnection conn = url.openConnection();
			conn.connect();

			return true;
		} catch (MalformedURLException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
	}
}
