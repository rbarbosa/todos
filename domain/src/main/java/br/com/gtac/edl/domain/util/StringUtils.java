package br.com.gtac.edl.domain.util;

public class StringUtils {
	public static String urlPrettyfier(String value){
		String v = new String(value);
		v = org.apache.commons.lang3.StringUtils.stripAccents(v)
				.replaceAll("\\s", "-")
				.replaceAll("[^-\\w]", "")
				.toLowerCase();
		
		return v;
	}
	
	
	/**
	 * Creates a rule for generating URL strings with ids,
	 * in order to avoid duplicate names by putting zeroes in front of the id string.
	 * 
	 * @param id
	 * @param value
	 * @return
	 */
	public static String urlPrettyfierWithId(Long id, String value) {
		return id + "-" + urlPrettyfier(value);
	}
}
