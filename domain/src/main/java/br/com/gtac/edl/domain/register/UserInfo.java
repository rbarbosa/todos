package br.com.gtac.edl.domain.register;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.AssociationOverride;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.ToString;

@Entity
@Data
@Table(name = "user_info")
@ToString(of={"id","fullName"})
public class UserInfo implements Serializable {
	private static final long serialVersionUID = -9149765673839650480L;

	public UserInfo() {

	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id_user")
	private Long id;
	
	@OneToOne(mappedBy="info")
	private User user;

	@Column(name = "address")
	private String address;

	@Column(name = "cep")
	@Size(min = 0, max = 10)
	private String cep;

	@Column(name = "district")
	private String district;

	@Column(name = "full_name")
	private String fullName;

	@Column(name = "birth_date")
	private Date birthDate;

	@Column(name = "facebook")
	private String facebook;

	@Column(name = "cpf")
	private String cpf;
	
	
	public void setCpf(String cpf) {
		this.cpf = cpf.replaceAll("\\D", "");
	}
	
	public String getFirstName(){
	    if(fullName!=null)
		return fullName.split("\\s")[0];
	    return null;
	}



}