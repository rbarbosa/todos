package br.com.gtac.edl.domain.register;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.hibernate.annotations.SQLDelete;

/**
 * The persistent class for the edl_user database table.
 * 
 */
@Entity
@Table(name = "user")
@Data
@EqualsAndHashCode(of="id")
@SQLDelete(sql = "UPDATE user SET deleted=UNIX_TIMESTAMP() WHERE id_user=?")
public class UserLoginEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_user")
	private Long id;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name = "first_access")
	private Date firstAccess = new Date();

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name = "last_access")
	private Date lastAccess;

	@Column(name = "last_ip")
	private String lastIp;

}