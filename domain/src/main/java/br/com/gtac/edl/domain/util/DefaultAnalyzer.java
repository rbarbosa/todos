package br.com.gtac.edl.domain.util;

import java.io.IOException;
import java.io.Reader;

import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.ReusableAnalyzerBase;
import org.apache.lucene.analysis.StopFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.br.BrazilianAnalyzer;
import org.apache.lucene.analysis.br.BrazilianStemFilter;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.util.Version;

public class DefaultAnalyzer extends ReusableAnalyzerBase {

	private final Version matchVersion;

	public DefaultAnalyzer(Version matchVersion) {
		this.matchVersion = matchVersion;
	}

	@Override
	public TokenStreamComponents createComponents(String fieldName,
			Reader reader) {
		Tokenizer src = new StandardTokenizer(matchVersion, reader);
		TokenStream sink = new StandardFilter(matchVersion, src);
		sink = new LowerCaseFilter(matchVersion, sink);
		sink = new StopFilter(matchVersion, sink,
				BrazilianAnalyzer.getDefaultStopSet());
		sink = new BrazilianStemFilter(sink);

		return new TokenStreamComponents(src, sink) {
			@Override
			protected boolean reset(final Reader reader) throws IOException {
				return super.reset(reader);
			}
		};
	}
}
