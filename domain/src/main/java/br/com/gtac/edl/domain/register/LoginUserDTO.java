package br.com.gtac.edl.domain.register;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import br.com.gtac.edl.domain.learning.validation.EmailConstraint;

@Data
public class LoginUserDTO {

	private User user;
	
	@NotNull(message = "{validation.notNull}")
	@Size(min = 4, max = 32, message = "{message.size}")
	private String j_password;

	@EmailConstraint(message = "{validation.email}")
	private String j_username;
	
	private boolean rememberme=true;

}
