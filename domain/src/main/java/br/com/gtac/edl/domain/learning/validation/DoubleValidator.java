package br.com.gtac.edl.domain.learning.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DoubleValidator implements
		ConstraintValidator<DoubleConstraint, Number> {

	private double min;
	private double max;

	@Override
	public void initialize(DoubleConstraint constraintAnnotation) {
		min = constraintAnnotation.min();
		max = constraintAnnotation.max();
	}

	@Override
	public boolean isValid(Number value, ConstraintValidatorContext context) {
		if (value == null) {
			return false;
		}
		double val = value.doubleValue();
		if(val>=min && val<=max){
			return true;
		}
		return false;
	}

}