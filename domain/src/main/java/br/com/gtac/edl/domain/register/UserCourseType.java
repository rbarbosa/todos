package br.com.gtac.edl.domain.register;

public enum UserCourseType {
	FUNDAMENTALEDUCATION("coursetype.fundamental"), HIGHSCHOOL(
			"coursetype.highschool"), GRADUATE("coursetype.graduate"), GRADUATESTUDIES(
			"coursetype.graduatestudies");

	private final String labelCode;

	UserCourseType(String labelCode) {
		this.labelCode = labelCode;
	}

	@Override
	public String toString() {

		if (this.equals(UserCourseType.FUNDAMENTALEDUCATION))
			return "Ensino Fundamental";
		if (this.equals(UserCourseType.HIGHSCHOOL))
			return "Ensino Médio";
		if (this.equals(UserCourseType.GRADUATE))
			return "Graduação";
		if (this.equals(UserCourseType.GRADUATESTUDIES))
			return "Pós-Graduação";
		else
			return "";

	};

	public String getLabelCode() {
		return labelCode;
	}
}
