package br.com.gtac.edl.domain.register;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Entity
@Table(name = "userconnection", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "userId", "providerId",
				"providerUserId" }),
		@UniqueConstraint(columnNames = { "userId", "providerId", "rank" }) })
@Data
public class UserConnection {
	@Id
	@Column(name = "id_userconnection")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "providerid")
	private String providerId;
	@Column(name = "provideruserid")
	private String providerUserId;
	
	@Column(name="rank")
	private String rank;
	
	private String displayname;
	@Column(name="profileurl")
	private String profileUrl;
	
	private String secret;

	@ManyToOne
	@JoinColumn(name = "userid")
	private User user;
}
