
package br.com.gtac.edl.domain.register;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.Email;

import br.com.gtac.edl.domain.learning.validation.EmailConstraint;
import br.com.gtac.edl.domain.learning.validation.UserPasswordConstraint;

/**
 * The persistent class for the edl_user database table.
 * 
 */
@Entity
@Table(name="user")
@Data
@EqualsAndHashCode(of="id")
@UserPasswordConstraint
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class User implements Serializable {	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_user")
	private Long id;

	private Boolean completed = true;

	@Column(name = "deleted")
	private long deleted;

	@Email
	@EmailConstraint(message = "Digite um email válido.")
	private String email;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name = "first_access", updatable = false)
	private Date firstAccess = new Date();

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name = "last_access", updatable = false)
	private Date lastAccess;

	@Column(name = "last_ip", updatable = false)
	private String lastIp;

	@Column(name="user_password")
	@Size()
	private String password;
	
	@Lob
	private String roles;

	@Column(name = "lost_pass_token")
	private String lostPassToken;
	
	@Column(name = "last_update_date")
	private Date lastUpdateDate;
	
	@OneToOne(cascade={CascadeType.ALL})
	@JoinColumn(name = "id_user_info")
	@Valid
	private UserInfo info;

	public void setEmail(String email){
		this.email = email!=null ? email.toLowerCase() : null;
	}

	public void setAdmin(boolean admin) {
		if (admin) {
			this.setRoles(Roles.getRolesString(Roles.USER, Roles.ADMIN));
		} else {
			this.setRoles(Roles.getRolesString(Roles.USER));
		}
	}

	public boolean isAdmin() {
		return this.hasRole(Roles.ADMIN);
	}

    public User() {
    }
    
    public boolean hasRole(Roles role){
    	return getRoleSet().contains(role);
    }
    
    
    public Set<Roles> getRoleSet(){
    	return Roles.getRolesSet(roles);
    }
    
    public boolean isActive(){
    	return deleted==0;
    }
    public void setActive(boolean b){
    	if(deleted==0){
    		deleted= b? 0:new Date().getTime()/1000;
    	}else if(b){
    		deleted = 0;
    	}
    }
    
	@PreUpdate
	public void preUpdate() {
		lastUpdateDate = new Date();
	}
	@PrePersist
	public void prePersist() {
		lastUpdateDate = new Date();
	}

}