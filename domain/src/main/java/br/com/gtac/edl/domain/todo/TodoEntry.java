package br.com.gtac.edl.domain.todo;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.hibernate.validator.constraints.Length;

@Entity
@Data
@Table(name="todo_entry")
public class TodoEntry {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="id_todo_list")
	@JsonBackReference
	private TodoList parent;
	
	@Length(max = 140, min = 3, message = "Title should have length between 3 and 140")
	private String title;
	@Lob
	private String description;
	private Integer priority;
	private Calendar dueDate;
	private boolean completed;
}
