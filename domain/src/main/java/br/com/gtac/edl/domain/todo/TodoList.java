package br.com.gtac.edl.domain.todo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Data
@Table(name="todo_list")
public class TodoList {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(nullable = false)
	private String name;
	@Column(nullable = false)
	@JsonIgnore
	private Long userId;
	private Calendar lastSeen = Calendar.getInstance();
	
	@OneToMany(cascade = CascadeType.REFRESH, mappedBy = "parent", fetch = FetchType.EAGER)
	private List<TodoEntry> entries = new ArrayList<>();

}
