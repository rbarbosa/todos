package br.com.gtac.edl.domain.util;

import org.apache.commons.validator.routines.EmailValidator;


public class EmailUtil {
	public static boolean isValidEmailAddress(String email) {
		return EmailValidator.getInstance().isValid(email);
	}
}
