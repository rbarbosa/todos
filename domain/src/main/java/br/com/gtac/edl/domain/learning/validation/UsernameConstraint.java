package br.com.gtac.edl.domain.learning.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UsernameValidator.class)
public @interface UsernameConstraint {

	String message() default "{username.message}";
	
	String sizeMessage() default "{DoubleConstraint.message}";
	
	int min();
	
	int max();

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
