package br.com.gtac.edl.domain.learning.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.gtac.edl.domain.util.EmailUtil;


public class EmailValidator implements
		ConstraintValidator<EmailConstraint, String> {

	@Override
	public void initialize(EmailConstraint constraintAnnotation) {

	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return EmailUtil.isValidEmailAddress(value);
	}

}
