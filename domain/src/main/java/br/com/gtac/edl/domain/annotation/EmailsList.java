package br.com.gtac.edl.domain.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EmailsListValidator.class)
public @interface EmailsList {

	String message() default "{EmailsList.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
