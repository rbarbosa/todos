package br.com.gtac.edl.domain.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

import br.com.gtac.edl.domain.util.EmailUtil;


public class EmailsListValidator implements
		ConstraintValidator<EmailsList, String> {

	@Override
	public void initialize(EmailsList constraintAnnotation) {
		
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if(StringUtils.isEmpty(value)==false){
			String[] emails = value.split(",");
			for(String email:emails){
				if(EmailUtil.isValidEmailAddress(email)==false){
					return false;
				}
			}
			return true;
		}
		return true;
	}

}
