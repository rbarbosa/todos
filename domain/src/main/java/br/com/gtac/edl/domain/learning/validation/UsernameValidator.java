package br.com.gtac.edl.domain.learning.validation;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;

public class UsernameValidator implements
		ConstraintValidator<UsernameConstraint, String> {
	private UsernameConstraint annotation;

	@Override
	public void initialize(UsernameConstraint constraintAnnotation) {
		annotation = constraintAnnotation;
	}

	private static final Pattern validPattern = Pattern
			.compile("^\\w(?:\\w*(?:[.-]\\w+)?)*$");

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {

		if (!StringUtils.isBlank(value)) {
			boolean validation = validPattern.matcher(value).matches();
			if (validation) {
				if (value.length() == 32) {
					return true;
				}
				
				context.disableDefaultConstraintViolation();
				
				context.buildConstraintViolationWithTemplate(annotation.sizeMessage())
						.addConstraintViolation();
				
				return value.length() >= annotation.min() && value.length() <= annotation.max();
			} else {
				return false;
			}
		} else if (value == null) {
			return true;
		}
		return false;
	}

}
