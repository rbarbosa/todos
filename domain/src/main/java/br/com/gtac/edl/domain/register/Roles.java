package br.com.gtac.edl.domain.register;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.ImmutableMap;

public enum Roles {
	USER("ROLE_USER"),ADMIN("ROLE_ADMIN"),ANONYMOUS("ROLE_ANONYMOUS"), TEST("ROLE_TEST");
	
	private final String roleName;
	
	
	private static final ImmutableMap<String, Roles> NAMES_MAP = 
			ImmutableMap.of(USER.getRoleName(),USER , ADMIN.getRoleName(),ADMIN,ANONYMOUS.getRoleName(),ANONYMOUS,TEST.getRoleName(),TEST);
	
	private Roles(String name){
		roleName=name;
	}
	
	public String getRoleName(){
		return roleName;
	}
	
	@Override
	public String toString() {
		return roleName;
	}
	public static String getRolesString(Roles ...roles ){
		return StringUtils.join(roles, ',');
	}
	public static Roles valueByString(String role){
		return NAMES_MAP.get(role);
	}
	public static Set<Roles> getRolesSet(String roles){
		Set<Roles> res = new HashSet<Roles>();
		if(roles!=null && roles.isEmpty()==false){
    		String[] split = StringUtils.split(roles, ',');
    		for(String role:split){
    			res.add(Roles.valueByString(role));
    		}
    	}
		return res;
	}
	
	
	
	

}
