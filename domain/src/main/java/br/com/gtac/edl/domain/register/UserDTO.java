package br.com.gtac.edl.domain.register;

import lombok.Data;
import lombok.Delegate;

@Data
public class UserDTO {
	@Delegate
	private User user;


	public static UserDTO createDTO(User user) {

		UserDTO userDto = new UserDTO();

		userDto.setUser(user);

		return userDto;
	}

}

