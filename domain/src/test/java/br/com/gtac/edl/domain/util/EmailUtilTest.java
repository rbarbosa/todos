package br.com.gtac.edl.domain.util;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class EmailUtilTest {

	
	
	@Test
	public void testValidEmails() {
		List<String> validCases = Arrays.asList("rodrigo@gmail.com","rodrigo.barbosa@gmail.com","test-mario@gtac.com");
		for(String s:validCases){
			assertTrue("Failed:"+s,EmailUtil.isValidEmailAddress(s));
		}
		
	}
	@Test
	public void testInvalidEmails() {
		List<String> validCases = Arrays.asList("rodrigo@gmail","rodrigo.barbosa.gmail.com",
				"test-mario-gtac.com");
		for(String s:validCases){
			assertFalse("Failed:"+s,EmailUtil.isValidEmailAddress(s));
		}
		
	}

}
