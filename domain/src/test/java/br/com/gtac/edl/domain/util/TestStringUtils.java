package br.com.gtac.edl.domain.util;

import junit.framework.Assert;

import org.junit.Test;

public class TestStringUtils {

	@Test
	public void urlPrettyfierTest(){
		String[] originals = new String[]{
				"1.1, Teste",
				"láláláççç",
				";.,~^^[] teste"
		};
		String[] expecteds = new String[]{
				"11-teste",
				"lalalaccc",
				"-teste"
		};
		
		for(int i = 0; i < originals.length; i++){
			String actual = StringUtils.urlPrettyfier(originals[i]);
			String expected = expecteds[i];
			
			Assert.assertEquals(expected, actual);
		}
	}
}
